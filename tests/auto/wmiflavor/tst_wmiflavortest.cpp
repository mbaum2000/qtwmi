#include <QString>
#include <QtTest>

#include "wmiflavor.h"

class WmiFlavorTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testDefaultConstructor();
    void testPublicConstructor();
    void testPublicConstructor_data();
    void testCopyContructor();
    void testCopyContructor_data();
    void testPrivateConstructor();
    void testPrivateConstructor_data();
    void testPrivateConstructorIgnoreOriginMask();
    void testComparisonOperators();
    void testComparisonOperators_data();
    void testAssignmentOperators();
    void testAssignmentOperators_data();
    void testAccessors();
    void testAccessors_data();
    void testQVariant();
    void testQVariant_data();
    void testQDebug();
    void testQDebug_data();
    void testQVariantQDebug();
    void testQVariantQDebug_data();

private:
    void test_data();
};


void WmiFlavorTest::testDefaultConstructor() {
    WmiFlavor flavor;
    QCOMPARE(flavor.amended(), false);
    QCOMPARE(flavor.toInstance(), true);
    QCOMPARE(flavor.toSubClass(), true);
    QCOMPARE(flavor.disableOverride(), false);
}

void WmiFlavorTest::testPublicConstructor() {
    QFETCH(bool, amended);
    QFETCH(bool, toInstance);
    QFETCH(bool, toSubClass);
    QFETCH(bool, disableOverride);

    WmiFlavor flavor(amended, toInstance, toSubClass, disableOverride);
    QCOMPARE(flavor.amended(), amended);
    QCOMPARE(flavor.toInstance(), toInstance);
    QCOMPARE(flavor.toSubClass(), toSubClass);
    QCOMPARE(flavor.disableOverride(), disableOverride);
}

void WmiFlavorTest::testPublicConstructor_data() {
    test_data();
}

void WmiFlavorTest::testCopyContructor() {
    QFETCH(bool, amended);
    QFETCH(bool, toInstance);
    QFETCH(bool, toSubClass);
    QFETCH(bool, disableOverride);

    WmiFlavor flavor1(amended, toInstance, toSubClass, disableOverride);
    WmiFlavor flavor2(flavor1);

    QCOMPARE(flavor2.amended(), amended);
    QCOMPARE(flavor2.toInstance(), toInstance);
    QCOMPARE(flavor2.toSubClass(), toSubClass);
    QCOMPARE(flavor2.disableOverride(), disableOverride);
}

void WmiFlavorTest::testCopyContructor_data() {
    test_data();
}

void WmiFlavorTest::testPrivateConstructor() {
    QFETCH(bool, amended);
    QFETCH(bool, toInstance);
    QFETCH(bool, toSubClass);
    QFETCH(bool, disableOverride);
    QFETCH(long, comFlavor);

    WmiFlavor flavor(comFlavor);
    QCOMPARE(flavor.amended(), amended);
    QCOMPARE(flavor.toInstance(), toInstance);
    QCOMPARE(flavor.toSubClass(), toSubClass);
    QCOMPARE(flavor.disableOverride(), disableOverride);
}

void WmiFlavorTest::testPrivateConstructor_data() {
    test_data();
}

void WmiFlavorTest::testPrivateConstructorIgnoreOriginMask() {
    WmiFlavor flavor1((long)0x83);
    WmiFlavor flavor2((long)0xE3);

    QVERIFY(flavor1 == flavor2);
}

void WmiFlavorTest::testComparisonOperators() {
    QFETCH(bool, amended);
    QFETCH(bool, toInstance);
    QFETCH(bool, toSubClass);
    QFETCH(bool, disableOverride);

    WmiFlavor flavor1(amended, toInstance, toSubClass, disableOverride);
    WmiFlavor flavor2(flavor1);
    WmiFlavor flavor3(!amended, toInstance, toSubClass, disableOverride);
    WmiFlavor flavor4(amended, !toInstance, toSubClass, disableOverride);
    WmiFlavor flavor5(amended, toInstance, !toSubClass, disableOverride);
    WmiFlavor flavor6(amended, toInstance, toSubClass, !disableOverride);

    QVERIFY(flavor1 == flavor1);

    QVERIFY(flavor1 == flavor2);
    QVERIFY(flavor2 == flavor1);

    QVERIFY(flavor1 != flavor3);
    QVERIFY(flavor3 != flavor1);

    QVERIFY(flavor1 != flavor4);
    QVERIFY(flavor4 != flavor1);

    QVERIFY(flavor1 != flavor5);
    QVERIFY(flavor5 != flavor1);

    QVERIFY(flavor1 != flavor6);
    QVERIFY(flavor6 != flavor1);
}

void WmiFlavorTest::testComparisonOperators_data() {
    test_data();
}

void WmiFlavorTest::testAssignmentOperators() {
    QFETCH(bool, amended);
    QFETCH(bool, toInstance);
    QFETCH(bool, toSubClass);
    QFETCH(bool, disableOverride);

    WmiFlavor flavor1(amended, toInstance, toSubClass, disableOverride);
    WmiFlavor flavor2 = flavor1;

    QCOMPARE(flavor2.amended(), amended);
    QCOMPARE(flavor2.toInstance(), toInstance);
    QCOMPARE(flavor2.toSubClass(), toSubClass);
    QCOMPARE(flavor2.disableOverride(), disableOverride);
}

void WmiFlavorTest::testAssignmentOperators_data() {
    test_data();
}

void WmiFlavorTest::testAccessors() {
    QFETCH(bool, amended);
    QFETCH(bool, toInstance);
    QFETCH(bool, toSubClass);
    QFETCH(bool, disableOverride);

    WmiFlavor flavor;
    flavor.setAmended(amended);
    flavor.setToInstance(toInstance);
    flavor.setToSubClass(toSubClass);
    flavor.setDisableOverride(disableOverride);

    QCOMPARE(flavor.amended(), amended);
    QCOMPARE(flavor.toInstance(), toInstance);
    QCOMPARE(flavor.toSubClass(), toSubClass);
    QCOMPARE(flavor.disableOverride(), disableOverride);
}

void WmiFlavorTest::testAccessors_data() {
    test_data();
}

void WmiFlavorTest::testQVariant() {
    QFETCH(bool, amended);
    QFETCH(bool, toInstance);
    QFETCH(bool, toSubClass);
    QFETCH(bool, disableOverride);

    WmiFlavor flavor(amended, toInstance, toSubClass, disableOverride);
    QVariant variant = QVariant::fromValue<WmiFlavor>(flavor);

    WmiFlavor flavorCopy = variant.value<WmiFlavor>();
    QVariant variantCopy = flavorCopy;

    QVERIFY(flavor == variant);
    QVERIFY(!(flavor != variant));

    QVERIFY(variant == flavor);
    QVERIFY(!(variant != flavor));

    QCOMPARE(flavor, flavorCopy);
    QCOMPARE(variant, variantCopy);

    QVERIFY(variant == variantCopy);
    QVERIFY(!(variant != variantCopy));
}

void WmiFlavorTest::testQVariant_data() {
    test_data();
}

void WmiFlavorTest::testQDebug() {
    QFETCH(bool, amended);
    QFETCH(bool, toInstance);
    QFETCH(bool, toSubClass);
    QFETCH(bool, disableOverride);
    QFETCH(QString, qDebugText);

    WmiFlavor flavor(amended, toInstance, toSubClass, disableOverride);

    QString debugOut;
    QDebug debug(&debugOut);
    debug << flavor;
    QCOMPARE(debugOut, qDebugText);
}

void WmiFlavorTest::testQDebug_data() {
    test_data();
}

void WmiFlavorTest::testQVariantQDebug() {
    QFETCH(bool, amended);
    QFETCH(bool, toInstance);
    QFETCH(bool, toSubClass);
    QFETCH(bool, disableOverride);
    QFETCH(QString, qDebugText);

    WmiFlavor flavor(amended, toInstance, toSubClass, disableOverride);

    QString debugVariantOut;
    QString expectedDebugVariantOut = QString("QVariant(WmiFlavor, %1) ").arg(qDebugText);
    QDebug debugVariant(&debugVariantOut);
    debugVariant << QVariant::fromValue<WmiFlavor>(flavor);
    QCOMPARE(debugVariantOut, expectedDebugVariantOut);
}

void WmiFlavorTest::testQVariantQDebug_data() {
    test_data();
}

void WmiFlavorTest::test_data() {
    QTest::addColumn<bool>("amended");
    QTest::addColumn<bool>("toInstance");
    QTest::addColumn<bool>("toSubClass");
    QTest::addColumn<bool>("disableOverride");
    QTest::addColumn<long>("comFlavor");
    QTest::addColumn<QString>("qDebugText");

    QTest::newRow("WmiFlavor(true, true, true, true)")
            << true
            << true
            << true
            << true
            << (long)0x93
            << QString("WmiFlavor(Amended, ToInstance, ToSubClass, DisableOverride) ");

    QTest::newRow("WmiFlavor(false, false, false, false)")
            << false
            << false
            << false
            << false
            << (long)0x0
            << QString("WmiFlavor(Restricted) ");

    QTest::newRow("WmiFlavor(false, true, true, false)")
            << false
            << true
            << true
            << false
            << (long)0x3
            << QString("WmiFlavor(ToInstance, ToSubClass) ");

    QTest::newRow("WmiFlavor(true, true, false, false)")
            << true
            << true
            << false
            << false
            << (long)0x81
            << QString("WmiFlavor(Amended, ToInstance, NotToSubClass) ");

    QTest::newRow("WmiFlavor(true, false, true, false)")
            << true
            << false
            << true
            << false
            << (long)0x82
            << QString("WmiFlavor(Amended, NotToInstance, ToSubClass) ");

    QTest::newRow("WmiFlavor(true, true, true, false) /*With ORIGIN_MASK*/")
            << true
            << true
            << true
            << false
            << (long)0xE3
            << QString("WmiFlavor(Amended, ToInstance, ToSubClass) ");
}

QTEST_APPLESS_MAIN(WmiFlavorTest)

#include "tst_wmiflavortest.moc"
