TEMPLATE = subdirs

SUBDIRS += \
    wmiconnection \
    wmiqualifier \
    wmiflavor \
    wmiparameter \
    wmiproperty \
    wmiobject \
    wmimethod
