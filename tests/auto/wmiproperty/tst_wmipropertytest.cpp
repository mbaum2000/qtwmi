#include <QString>
#include <QtTest>

#include "wmitest.h"

#include "wmiproperty.h"
#include "wmiconnection.h"

class WmiPropertyTest : public WmiTest {
    Q_OBJECT
private Q_SLOTS:
    void testDefaultConstructor();
    void testCopyContructor();
    void testCopyContructor_data();
    void testComparisonOperators();
    void testComparisonOperators_data();
    void testAssignmentOperators();
    void testAssignmentOperators_data();
    void testPropertyIntrospection();
    void testPropertyIntrospection_data();
    void testPropertyQualifiers();
    void testPropertyQualifiers_data();
    void testQDebug();
    void testQDebug_data();

private:
    void test_data(bool includeInvalid = true);
};


void WmiPropertyTest::testDefaultConstructor() {
    WmiProperty property;
    QVERIFY(property.isNull());
    QVERIFY(!property.isValid());
    QCOMPARE(property.object(), WmiObject());
    QCOMPARE(property.name(), QString());
    QCOMPARE(property.cimType(), WmiProperty::Empty);
    QCOMPARE(property.cimTypeName(), QString());
    QCOMPARE(property.origin(), QString());
    QCOMPARE(property.qualifiers(), QList<WmiQualifier>());
    QCOMPARE(property.isSystem(), false);
    QCOMPARE(property.isPropogated(), false);
}

void WmiPropertyTest::testCopyContructor() {
    QFETCH(QString, objectName);
    QFETCH(QString, propertyName);

    WmiConnection connection(wmiTestNamespace());

    WmiProperty property1 = connection.getObject(objectName)
                                  .propertyInfo(propertyName);
    WmiProperty property2(property1);

    QVERIFY(property1 == property2);
    QVERIFY(!(property1 != property2));
    QVERIFY(property1.object() == property2.object());
    QVERIFY(property1.name() == property2.name());
    QVERIFY(property1.cimType() == property2.cimType());
    QVERIFY(property1.cimTypeName() == property2.cimTypeName());
    QVERIFY(property1.origin() == property2.origin());
    QVERIFY(property1.qualifiers() == property2.qualifiers());
    QVERIFY(property1.isSystem() == property2.isSystem());
    QVERIFY(property1.isPropogated() == property2.isPropogated());
}

void WmiPropertyTest::testCopyContructor_data() {
    test_data();
}

void WmiPropertyTest::testComparisonOperators() {
    QFETCH(QString, objectName);
    QFETCH(QString, propertyName);

    WmiConnection connection(wmiTestNamespace());

    WmiProperty property0;
    WmiProperty property1 = connection.getObject(objectName)
                                  .propertyInfo(propertyName);
    WmiProperty property2 = connection.getObject(QLatin1String("Test_WmiTestInterface.Id=3"))
                                  .propertyInfo(propertyName);
    WmiProperty property3 = connection.getObject(objectName)
                                  .propertyInfo(propertyName);

    QVERIFY(property0 != property1);
    QVERIFY(property1 != property2);
    QVERIFY(property1 == property3);

    QVERIFY(!(property0 == property1));
    QVERIFY(!(property1 == property2));
    QVERIFY(!(property1 != property3));
}

void WmiPropertyTest::testComparisonOperators_data() {
    test_data(false);
}

void WmiPropertyTest::testAssignmentOperators() {
    QFETCH(QString, objectName);
    QFETCH(QString, propertyName);

    WmiConnection connection(wmiTestNamespace());

    WmiProperty property1 = connection.getObject(objectName)
                                  .propertyInfo(propertyName);
    WmiProperty property2 = property1;

    QVERIFY(property1 == property2);
    QVERIFY(!(property1 != property2));
    QVERIFY(property1.object() == property2.object());
    QVERIFY(property1.name() == property2.name());
    QVERIFY(property1.cimType() == property2.cimType());
    QVERIFY(property1.cimTypeName() == property2.cimTypeName());
    QVERIFY(property1.origin() == property2.origin());
    QVERIFY(property1.qualifiers() == property2.qualifiers());
    QVERIFY(property1.isSystem() == property2.isSystem());
    QVERIFY(property1.isPropogated() == property2.isPropogated());
}

void WmiPropertyTest::testAssignmentOperators_data() {
    test_data();
}

void WmiPropertyTest::testPropertyIntrospection() {
    QFETCH(QString, objectName);
    QFETCH(QString, propertyName);
    QFETCH(bool, isValid);
    QFETCH(WmiProperty::CimType, cimType);
    QFETCH(QString, cimTypeName);
    QFETCH(QString, origin);
    QFETCH(QList<WmiQualifier>, qualifiers);
    QFETCH(bool, isSystem);
    QFETCH(bool, isPropogated);

    WmiConnection connection(wmiTestNamespace());

    WmiObject object = connection.getObject(objectName);
    WmiProperty property = object.propertyInfo(propertyName);

    if(isValid) {
        QVERIFY(property.isValid());
        QCOMPARE(property.name(), propertyName);
    } else {
        QVERIFY(!property.isValid());
        QCOMPARE(property.name(), QString());
    }
    QCOMPARE(property.cimType(), cimType);
    QCOMPARE(property.cimTypeName(), cimTypeName);
    QCOMPARE(property.origin(), origin);
    QCOMPARE(property.qualifiers(), qualifiers);
    QCOMPARE(property.isSystem(), isSystem);
    QCOMPARE(property.isPropogated(), isPropogated);
}

void WmiPropertyTest::testPropertyIntrospection_data() {
    test_data();
}

void WmiPropertyTest::testPropertyQualifiers() {
    QFETCH(QString, objectName);
    QFETCH(QString, propertyName);
    QFETCH(WmiQualifier, qualifier);
    QFETCH(QString, qualifierName);

    WmiConnection connection(wmiTestNamespace());

    WmiObject object = connection.getObject(objectName);
    WmiProperty property = object.propertyInfo(propertyName);

    WmiQualifier propertyQualifier = property.qualifier(qualifierName);

    QCOMPARE(propertyQualifier.name(), qualifier.name());
    QCOMPARE(propertyQualifier.value(), qualifier.value());
    QCOMPARE(propertyQualifier.flavor(), qualifier.flavor());

    QCOMPARE(propertyQualifier, qualifier);

    if(!qualifier.name().isEmpty()) {
        QVERIFY(property.qualifiers().contains(qualifier));
    } else {
        QVERIFY(!property.qualifiers().contains(qualifier));
    }
}

void WmiPropertyTest::testPropertyQualifiers_data() {
    QTest::addColumn<QString>("objectName");
    QTest::addColumn<QString>("propertyName");
    QTest::addColumn<QString>("qualifierName");
    QTest::addColumn<WmiQualifier>("qualifier");

    QTest::newRow("[Test_WmiTestInterface].Id[Description]")
            << QString("Test_WmiTestInterface")
            << QString("Id")
            << QString("Description")
            << WmiQualifier(QLatin1String("Description"),
                            QVariant("The Unique ID"),
                            WmiFlavor(false, true, true, false));

    QTest::newRow("[Test_WmiTestInterface].StringProperty[Description]")
            << QString("Test_WmiTestInterface")
            << QString("StringProperty")
            << QString("Description")
            << WmiQualifier(QLatin1String("Description"),
                            QVariant("A String Property"),
                            WmiFlavor(false, true, true, false));

    QTest::newRow("[Test_WmiTestInterface].Id[Key]")
            << QString("Test_WmiTestInterface")
            << QString("Id")
            << QString("Key")
            << WmiQualifier(QLatin1String("key"),
                            QVariant(true),
                            WmiFlavor(false, true, true, true));

    QTest::newRow("[Test_WmiTestInterface].StringProperty[Key]")
            << QString("Test_WmiTestInterface")
            << QString("StringProperty")
            << QString("Key")
            << WmiQualifier(QString(),
                            QVariant(),
                            WmiFlavor());

    QTest::newRow("[Test_WmiTestInterface].StringProperty[write]")
            << QString("Test_WmiTestInterface")
            << QString("StringProperty")
            << QString("write")
            << WmiQualifier(QLatin1String("write"),
                            QVariant(true),
                            WmiFlavor(false, false, false, false));

    QTest::newRow("[Test_WmiTestInterface.Id=1].Id[Description]")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("Id")
            << QString("Description")
            << WmiQualifier(QLatin1String("Description"),
                            QVariant("The Unique ID"),
                            WmiFlavor(false, true, true, false));

    QTest::newRow("[Test_WmiTestInterface.Id=1].StringProperty[Description]")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("StringProperty")
            << QString("Description")
            << WmiQualifier(QLatin1String("Description"),
                            QVariant("A String Property"),
                            WmiFlavor(false, true, true, false));

    QTest::newRow("[Test_WmiTestInterface.Id=1].Id[Key]")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("Id")
            << QString("Key")
            << WmiQualifier(QLatin1String("key"),
                            QVariant(true),
                            WmiFlavor(false, true, true, true));

    QTest::newRow("[Test_WmiTestInterface.Id=1].StringProperty[Key]")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("StringProperty")
            << QString("Key")
            << WmiQualifier(QString(),
                            QVariant(),
                            WmiFlavor());

    QTest::newRow("[Test_WmiTestInterface.Id=1].StringProperty[write]")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("StringProperty")
            << QString("write")
            << WmiQualifier(QString(),
                            QVariant(),
                            WmiFlavor());
}

void WmiPropertyTest::testQDebug() {
    QFETCH(QString, objectName);
    QFETCH(QString, propertyName);
    QFETCH(QString, qDebugText);

    WmiConnection connection(wmiTestNamespace());
    WmiObject object = connection.getObject(objectName);
    WmiProperty property = object.propertyInfo(propertyName);

    QString debugOut;
    QDebug debug(&debugOut);
    debug << property;
    QCOMPARE(debugOut, qDebugText);
}

void WmiPropertyTest::testQDebug_data() {
    test_data();
}

void WmiPropertyTest::test_data(bool includeInvalid) {
    QTest::addColumn<QString>("objectName");
    QTest::addColumn<QString>("propertyName");
    QTest::addColumn<bool>("isValid");
    QTest::addColumn<WmiProperty::CimType>("cimType");
    QTest::addColumn<QString>("cimTypeName");
    QTest::addColumn<QString>("origin");
    QTest::addColumn<QList<WmiQualifier>>("qualifiers");
    QTest::addColumn<bool>("isSystem");
    QTest::addColumn<bool>("isPropogated");
    QTest::addColumn<QString>("qDebugText");

    QTest::newRow("[Test_WmiTestInterface].StringProperty")
            << QString("Test_WmiTestInterface")
            << QString("StringProperty")
            << true
            << WmiProperty::String
            << QString("String")
            << QString("Test_WmiTestInterface")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("string"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("Description"), QLatin1String("A String Property"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("write"), true, WmiFlavor(false, false, false, false))})
            << false
            << false
            << QString("WmiProperty([Test_WmiTestInterface].StringProperty) ");

    QTest::newRow("[Test_WmiTestInterface].NumberProperty")
            << QString("Test_WmiTestInterface")
            << QString("NumberProperty")
            << true
            << WmiProperty::SInt32
            << QString("SInt32")
            << QString("Test_WmiTestInterface")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("sint32"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("Description"), QLatin1String("A Numeric Property"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("write"), true, WmiFlavor(false, false, false, false))})
            << false
            << false
            << QString("WmiProperty([Test_WmiTestInterface].NumberProperty) ");

    QTest::newRow("[Test_WmiTestInterface].BooleanProperty")
            << QString("Test_WmiTestInterface")
            << QString("BooleanProperty")
            << true
            << WmiProperty::Boolean
            << QString("Boolean")
            << QString("Test_WmiTestInterface")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("boolean"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("Description"), QLatin1String("A Boolean Property"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("write"), true, WmiFlavor(false, false, false, false))})
            << false
            << false
            << QString("WmiProperty([Test_WmiTestInterface].BooleanProperty) ");

    QTest::newRow("[Test_WmiTestInterface].__NAMESPACE")
            << QString("Test_WmiTestInterface")
            << QString("__NAMESPACE")
            << true
            << WmiProperty::String
            << QString("String")
            << QString("___SYSTEM")
            << QList<WmiQualifier>({})
            << true
            << false
            << QString("WmiProperty([Test_WmiTestInterface].__NAMESPACE) ");

    QTest::newRow("[Test_WmiTestInterface.Id=1].StringProperty")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("StringProperty")
            << true
            << WmiProperty::String
            << QString("String")
            << QString("Test_WmiTestInterface")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("string"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("Description"), QLatin1String("A String Property"), WmiFlavor(false, true, true, false))})
            << false
            << false
            << QString("WmiProperty([Test_WmiTestInterface.Id=1].StringProperty) ");

    QTest::newRow("[Test_WmiTestInterface.Id=1].NumberProperty")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("NumberProperty")
            << true
            << WmiProperty::SInt32
            << QString("SInt32")
            << QString("Test_WmiTestInterface")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("sint32"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("Description"), QLatin1String("A Numeric Property"), WmiFlavor(false, true, true, false))})
            << false
            << false
            << QString("WmiProperty([Test_WmiTestInterface.Id=1].NumberProperty) ");

    QTest::newRow("[Test_WmiTestInterface.Id=1].BooleanProperty")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("BooleanProperty")
            << true
            << WmiProperty::Boolean
            << QString("Boolean")
            << QString("Test_WmiTestInterface")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("boolean"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("Description"), QLatin1String("A Boolean Property"), WmiFlavor(false, true, true, false))})
            << false
            << false
            << QString("WmiProperty([Test_WmiTestInterface.Id=1].BooleanProperty) ");

    QTest::newRow("[Test_WmiTestInterface.Id=1].__NAMESPACE")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("__NAMESPACE")
            << true
            << WmiProperty::String
            << QString("String")
            << QString("___SYSTEM")
            << QList<WmiQualifier>({})
            << true
            << false
            << QString("WmiProperty([Test_WmiTestInterface.Id=1].__NAMESPACE) ");

    if(includeInvalid) {
        QTest::newRow("[Test_WmiTestInterface].NonExistantProperty")
                << QString("Test_WmiTestInterface")
                << QString("NonExistantProperty")
                << false
                << WmiProperty::Empty
                << QString()
                << QString()
                << QList<WmiQualifier>({})
                << false
                << false
                << QString("WmiProperty() ");

        QTest::newRow("[Test_WmiTestInterface.Id=1].NonExistantProperty")
                << QString("Test_WmiTestInterface.Id=1")
                << QString("NonExistantProperty")
                << false
                << WmiProperty::Empty
                << QString()
                << QString()
                << QList<WmiQualifier>({})
                << false
                << false
                << QString("WmiProperty() ");
    }
}

QTEST_MAIN(WmiPropertyTest)

#include "tst_wmipropertytest.moc"
