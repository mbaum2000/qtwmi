#include <QString>
#include <QtTest>

#include "wmitest.h"

#include "wmiconnection.h"
#include "wmiobjectrequest.h"
#include "wmimethod.h"
#include "wmimethodcall.h"
#include "wmiproperty.h"
#include "wmiqualifier.h"
#include "wmiflavor.h"
#include "wmierror.h"

class WmiConnectionTest : public WmiTest {
    Q_OBJECT
private Q_SLOTS:

    void testConnection();
    void testConnection_data();
//    void testListNamespaces(); // TODO
//    void testListClasses(); // TODO
    void testExecQuery();
    void testExecQuery_data();
    void testGetObject();
    void testGetObject_data();
};


void WmiConnectionTest::testConnection() {
    QFETCH(QString, wmiNamespace);
    QFETCH(bool, isValid);
    QFETCH(WmiError::Error, error);

    WmiConnection connection(wmiNamespace);

    QCOMPARE(connection.isValid(), isValid);
    QCOMPARE(connection.error().error(), error);
}

void WmiConnectionTest::testConnection_data() {
    QTest::addColumn<QString>("wmiNamespace");
    QTest::addColumn<bool>("isValid");
    QTest::addColumn<WmiError::Error>("error");

    QTest::newRow("ROOT")
            << QString("ROOT")
            << true
            << WmiError::NoError;

    QTest::newRow("ROOT\\Cimv2")
            << QString("ROOT\\Cimv2")
            << true
            << WmiError::NoError;

    QTest::newRow("\\\\localhost\\ROOT\\Cimv2")
            << QString("\\\\localhost\\ROOT\\Cimv2")
            << true
            << WmiError::NoError;

//    QTest::newRow("\\\\nonexistanthost\\ROOT\\Cimv2")
//            << QString("\\\\nonexistanthost\\ROOT\\Cimv2")
//            << false
//            << WmiError::ServerUnavailable;

//    QTest::newRow("\\\\remotehost\\ROOT\\Cimv2")
//            << QString("\\\\remotehost\\ROOT\\Cimv2")
//            << false
//            << WmiError::AccessDenied;

    QTest::newRow("blarg")
            << QString("blarg")
            << false
            << WmiError::InvalidNamespace;

    QTest::newRow("ROOT\\blarg")
            << QString("ROOT\\blarg")
            << false
            << WmiError::InvalidNamespace;
}

void WmiConnectionTest::testExecQuery() {
    QFETCH(QString, query);
    QFETCH(QVariantList, expectedResults);

    WmiConnection connection(wmiTestNamespace());

    WmiObjectRequest *request = connection.execQuery(query);
    QVERIFY(request != Q_NULLPTR);
    request->waitForFinished();

    QCOMPARE(request->objects().count(), expectedResults.count());
    for(int i = 0; i < request->objects().count(); i++) {
        WmiObject result = request->objects().at(i);
        QVariantMap expected = expectedResults.at(i).toMap();

        QCOMPARE(result.isInstance(), true);
        QCOMPARE(result.className(), expected.value("class").toString());
        QCOMPARE(result.property("__RELPATH"), expected.value("__RELPATH"));
        QCOMPARE(result.property("Id"), expected.value("Id"));
        QCOMPARE(result.property("StringProperty"), expected.value("StringProperty"));
        QCOMPARE(result.property("NumberProperty"), expected.value("NumberProperty"));
        QCOMPARE(result.property("BooleanProperty"), expected.value("BooleanProperty"));
    }
}

void WmiConnectionTest::testExecQuery_data() {
    QTest::addColumn<QString>("query");
    QTest::addColumn<QVariantList>("expectedResults");

    QTest::newRow("SELECT * FROM Test_WmiTestInterface")
            << QString("SELECT * FROM Test_WmiTestInterface")
            << (QVariantList({
                                 QVariantMap({{"class", "Test_WmiTestInterface"},
                                              {"__RELPATH", "Test_WmiTestInterface.Id=1"},
                                              {"Id", "1"},
                                              {"StringProperty", "foo"},
                                              {"NumberProperty", 42},
                                              {"BooleanProperty", true}}),
                                 QVariantMap({{"class", "Test_WmiTestInterface"},
                                              {"__RELPATH", "Test_WmiTestInterface.Id=2"},
                                              {"Id", "2"},
                                              {"StringProperty", "bar"},
                                              {"NumberProperty", 256},
                                              {"BooleanProperty", false}}),
                                 QVariantMap({{"class", "Test_WmiTestInterface"},
                                              {"__RELPATH", "Test_WmiTestInterface.Id=3"},
                                              {"Id", "3"},
                                              {"StringProperty", "baz"},
                                              {"NumberProperty", 2048},
                                              {"BooleanProperty", true}})
                             }));

    QTest::newRow("SELECT * FROM Test_WmiTestInterface WHERE StringProperty LIKE 'ba_'")
            << QString("SELECT * FROM Test_WmiTestInterface WHERE StringProperty LIKE 'ba_'")
            << (QVariantList({
                                 QVariantMap({{"class", "Test_WmiTestInterface"},
                                              {"__RELPATH", "Test_WmiTestInterface.Id=2"},
                                              {"Id", "2"},
                                              {"StringProperty", "bar"},
                                              {"NumberProperty", 256},
                                              {"BooleanProperty", false}}),
                                 QVariantMap({{"class", "Test_WmiTestInterface"},
                                              {"__RELPATH", "Test_WmiTestInterface.Id=3"},
                                              {"Id", "3"},
                                              {"StringProperty", "baz"},
                                              {"NumberProperty", 2048},
                                              {"BooleanProperty", true}})
                             }));

    QTest::newRow("SELECT * FROM Test_WmiTestInterface WHERE NumberProperty < 1024")
            << QString("SELECT * FROM Test_WmiTestInterface WHERE NumberProperty < 1024")
            << (QVariantList({
                                 QVariantMap({{"class", "Test_WmiTestInterface"},
                                              {"__RELPATH", "Test_WmiTestInterface.Id=1"},
                                              {"Id", "1"},
                                              {"StringProperty", "foo"},
                                              {"NumberProperty", 42},
                                              {"BooleanProperty", true}}),
                                 QVariantMap({{"class", "Test_WmiTestInterface"},
                                              {"__RELPATH", "Test_WmiTestInterface.Id=2"},
                                              {"Id", "2"},
                                              {"StringProperty", "bar"},
                                              {"NumberProperty", 256},
                                              {"BooleanProperty", false}})
                             }));

    QTest::newRow("SELECT * FROM Test_WmiTestInterface WHERE BooleanProperty = true")
            << QString("SELECT * FROM Test_WmiTestInterface WHERE BooleanProperty = true")
            << (QVariantList({
                                 QVariantMap({{"class", "Test_WmiTestInterface"},
                                              {"__RELPATH", "Test_WmiTestInterface.Id=1"},
                                              {"Id", "1"},
                                              {"StringProperty", "foo"},
                                              {"NumberProperty", 42},
                                              {"BooleanProperty", true}}),
                                 QVariantMap({{"class", "Test_WmiTestInterface"},
                                              {"__RELPATH", "Test_WmiTestInterface.Id=3"},
                                              {"Id", "3"},
                                              {"StringProperty", "baz"},
                                              {"NumberProperty", 2048},
                                              {"BooleanProperty", true}})
                             }));
}

void WmiConnectionTest::testGetObject() {
    QFETCH(QString, objectName);
    QFETCH(QString, className);
    QFETCH(bool, isValid);
    QFETCH(bool, isInstance);
    QFETCH(WmiError::Error, error);

    WmiConnection connection(wmiTestNamespace());

    WmiObject object = connection.getObject(objectName);
    QCOMPARE(object.isValid(), isValid);
    if(object.isValid()) {
        QCOMPARE(object.className(), className);
        QCOMPARE(object.isInstance(), isInstance);
    }
    QCOMPARE(connection.error().error(), error);
}

void WmiConnectionTest::testGetObject_data() {
    QTest::addColumn<QString>("objectName");
    QTest::addColumn<QString>("className");
    QTest::addColumn<bool>("isValid");
    QTest::addColumn<bool>("isInstance");
    QTest::addColumn<WmiError::Error>("error");

    QTest::newRow("Test_WmiTestInterface")
            << QString("Test_WmiTestInterface")
            << QString("Test_WmiTestInterface")
            << true
            << false
            << WmiError::NoError;

    QTest::newRow("Test_WmiTestInterface.Id=1")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("Test_WmiTestInterface")
            << true
            << true
            << WmiError::NoError;

    QTest::newRow("Test_WmiChildObject")
            << QString("Test_WmiChildObject")
            << QString("Test_WmiChildObject")
            << true
            << false
            << WmiError::NoError;

    QTest::newRow("Test_NonExistantObject")
            << QString("Test_NonExistantObject")
            << QString()
            << false
            << false
            << WmiError::NotFound;

    QTest::newRow("Test_InvalidObject!")
            << QString("Test_InvalidObject!")
            << QString()
            << false
            << false
            << WmiError::InvalidObjectPath;
}

QTEST_MAIN(WmiConnectionTest)

#include "tst_wmiconnectiontest.moc"
