#include <QString>
#include <QtTest>

#include "wmitest.h"

#include "wmiparameter.h"
#include "wmiconnection.h"
#include "wmimethod.h"

class WmiParameterTest : public WmiTest {
    Q_OBJECT
private Q_SLOTS:
    void testDefaultConstructor();
    void testCopyContructor();
    void testCopyContructor_data();
    void testComparisonOperators();
    void testComparisonOperators_data();
    void testAssignmentOperators();
    void testAssignmentOperators_data();
    void testParameterIntrospection();
    void testParameterIntrospection_data();
    void testParameterQualifiers();
    void testParameterQualifiers_data();
    void testQDebug();
    void testQDebug_data();

private:
    void test_data(bool includeInvalid = true);
};


void WmiParameterTest::testDefaultConstructor() {
    WmiParameter parameter;
    QVERIFY(parameter.isNull());
    QVERIFY(!parameter.isValid());
    QCOMPARE(parameter.method(), WmiMethod());
    QCOMPARE(parameter.name(), QString());
    QCOMPARE(parameter.cimType(), WmiParameter::Empty);
    QCOMPARE(parameter.cimTypeName(), QString());
    QCOMPARE(parameter.qualifiers(), QList<WmiQualifier>());
    QCOMPARE(parameter.isInput(), false);
    QCOMPARE(parameter.isOutput(), false);
}

void WmiParameterTest::testCopyContructor() {
    QFETCH(QString, objectName);
    QFETCH(QString, methodName);
    QFETCH(QString, parameterName);

    WmiConnection connection(wmiTestNamespace());

    WmiParameter parameter1 = connection.getObject(objectName)
                                       .method(methodName)
                                       .parameterInfo(parameterName);
    WmiParameter parameter2(parameter1);

    QVERIFY(parameter1 == parameter2);
    QVERIFY(!(parameter1 != parameter2));
    QVERIFY(parameter1.method() == parameter2.method());
    QVERIFY(parameter1.name() == parameter2.name());
    QVERIFY(parameter1.cimType() == parameter2.cimType());
    QVERIFY(parameter1.cimTypeName() == parameter2.cimTypeName());
    QVERIFY(parameter1.qualifiers() == parameter2.qualifiers());
    QVERIFY(parameter1.isInput() == parameter2.isInput());
    QVERIFY(parameter1.isOutput() == parameter2.isOutput());
}

void WmiParameterTest::testCopyContructor_data() {
    test_data();
}

void WmiParameterTest::testComparisonOperators() {
    QFETCH(QString, objectName);
    QFETCH(QString, methodName);
    QFETCH(QString, parameterName);

    WmiConnection connection(wmiTestNamespace());

    WmiParameter parameter0;
    WmiParameter parameter1 = connection.getObject(objectName)
                                       .method(methodName)
                                       .parameterInfo(parameterName);
    WmiParameter parameter2 = connection.getObject(QLatin1String("Test_WmiTestInterface.Id=3"))
                                       .method(QLatin1String("AddChild"))
                                       .parameterInfo(QLatin1String("Name"));
    WmiParameter parameter3 = connection.getObject(objectName)
                                       .method(methodName)
                                       .parameterInfo(parameterName);

    QVERIFY(parameter0 != parameter1);
    QVERIFY(parameter1 != parameter2);
    QVERIFY(parameter1 == parameter3);

    QVERIFY(!(parameter0 == parameter1));
    QVERIFY(!(parameter1 == parameter2));
    QVERIFY(!(parameter1 != parameter3));
}

void WmiParameterTest::testComparisonOperators_data() {
    test_data(false);
}

void WmiParameterTest::testAssignmentOperators() {
    QFETCH(QString, objectName);
    QFETCH(QString, methodName);
    QFETCH(QString, parameterName);

    WmiConnection connection(wmiTestNamespace());

    WmiParameter parameter1 = connection.getObject(objectName)
                                       .method(methodName)
                                       .parameterInfo(parameterName);
    WmiParameter parameter2 = parameter1;

    QVERIFY(parameter1 == parameter2);
    QVERIFY(!(parameter1 != parameter2));
    QVERIFY(parameter1.method() == parameter2.method());
    QVERIFY(parameter1.name() == parameter2.name());
    QVERIFY(parameter1.cimType() == parameter2.cimType());
    QVERIFY(parameter1.cimTypeName() == parameter2.cimTypeName());
    QVERIFY(parameter1.qualifiers() == parameter2.qualifiers());
    QVERIFY(parameter1.isInput() == parameter2.isInput());
    QVERIFY(parameter1.isOutput() == parameter2.isOutput());
}

void WmiParameterTest::testAssignmentOperators_data() {
    test_data();
}

void WmiParameterTest::testParameterIntrospection() {
    QFETCH(QString, objectName);
    QFETCH(QString, methodName);
    QFETCH(QString, parameterName);
    QFETCH(bool, isValid);
    QFETCH(WmiParameter::CimType, cimType);
    QFETCH(QString, cimTypeName);
    QFETCH(QList<WmiQualifier>, qualifiers);
    QFETCH(bool, isInput);
    QFETCH(bool, isOutput);

    WmiConnection connection(wmiTestNamespace());

    WmiObject object = connection.getObject(objectName);
    WmiMethod method = object.method(methodName);
    WmiParameter parameter = method.parameterInfo(parameterName);

    if(isValid) {
        QVERIFY(parameter.isValid());
        QCOMPARE(parameter.name(), parameterName);
    } else {
        QVERIFY(!parameter.isValid());
        QCOMPARE(parameter.name(), QString());
    }
    QCOMPARE(parameter.cimType(), cimType);
    QCOMPARE(parameter.cimTypeName(), cimTypeName);
    QCOMPARE(parameter.qualifiers(), qualifiers);
    QCOMPARE(parameter.isInput(), isInput);
    QCOMPARE(parameter.isOutput(), isOutput);
}

void WmiParameterTest::testParameterIntrospection_data() {
    test_data();
}

void WmiParameterTest::testParameterQualifiers() {
    QFETCH(QString, objectName);
    QFETCH(QString, methodName);
    QFETCH(QString, parameterName);
    QFETCH(QString, qualifierName);
    QFETCH(QVariant, value);
    QFETCH(WmiFlavor, flavor);

    WmiConnection connection(wmiTestNamespace());

    WmiObject object = connection.getObject(objectName);
    WmiMethod method = object.method(methodName);
    WmiParameter parameter = method.parameterInfo(parameterName);
    WmiQualifier qualifier = parameter.qualifier(qualifierName);

    QCOMPARE(qualifier.value(),
             value);

    QCOMPARE(qualifier.flavor(),
             flavor);

    QVERIFY(!object.qualifiers().isEmpty());
}

void WmiParameterTest::testParameterQualifiers_data() {
    QTest::addColumn<QString>("objectName");
    QTest::addColumn<QString>("methodName");
    QTest::addColumn<QString>("parameterName");
    QTest::addColumn<QString>("qualifierName");
    QTest::addColumn<QVariant>("value");
    QTest::addColumn<WmiFlavor>("flavor");

    QTest::newRow("[Test_WmiTestInterface].EchoString(inString)[Description]")
            << QString("Test_WmiTestInterface")
            << QString("EchoString")
            << QString("inString")
            << QString("Description")
            << QVariant("The Input String")
            << WmiFlavor(false, true, true, false);

    QTest::newRow("[Test_WmiTestInterface].EchoString(outString)[Description]")
            << QString("Test_WmiTestInterface")
            << QString("EchoString")
            << QString("outString")
            << QString("Description")
            << QVariant("The Output String")
            << WmiFlavor(false, true, true, false);

    QTest::newRow("[Test_WmiTestInterface].EchoString(inString)[in]")
            << QString("Test_WmiTestInterface")
            << QString("EchoString")
            << QString("inString")
            << QString("in")
            << QVariant(true)
            << WmiFlavor(false, false, false, false);

    QTest::newRow("[Test_WmiTestInterface].EchoString(outString)[out]")
            << QString("Test_WmiTestInterface")
            << QString("EchoString")
            << QString("outString")
            << QString("out")
            << QVariant(true)
            << WmiFlavor(false, false, false, false);

    QTest::newRow("[Test_WmiTestInterface.Id=1].EchoString(inString)[Description]")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("EchoString")
            << QString("inString")
            << QString("Description")
            << QVariant("The Input String")
            << WmiFlavor(false, true, true, false);

    QTest::newRow("[Test_WmiTestInterface.Id=1].EchoString(outString)[Description]")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("EchoString")
            << QString("outString")
            << QString("Description")
            << QVariant("The Output String")
            << WmiFlavor(false, true, true, false);

    QTest::newRow("[Test_WmiTestInterface.Id=1].EchoString(inString)[in]")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("EchoString")
            << QString("inString")
            << QString("in")
            << QVariant(true)
            << WmiFlavor(false, false, false, false);

    QTest::newRow("[Test_WmiTestInterface.Id=1].EchoString(outString)[out]")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("EchoString")
            << QString("outString")
            << QString("out")
            << QVariant(true)
            << WmiFlavor(false, false, false, false);
}

void WmiParameterTest::testQDebug() {
    QFETCH(QString, objectName);
    QFETCH(QString, methodName);
    QFETCH(QString, parameterName);
    QFETCH(QString, qDebugText);

    WmiConnection connection(wmiTestNamespace());
    WmiObject object = connection.getObject(objectName);
    WmiMethod method = object.method(methodName);
    WmiParameter parameter = method.parameterInfo(parameterName);

    QString debugOut;
    QDebug debug(&debugOut);
    debug << parameter;
    QCOMPARE(debugOut, qDebugText);
}

void WmiParameterTest::testQDebug_data() {
    test_data();
}

void WmiParameterTest::test_data(bool includeInvalid) {
    QTest::addColumn<QString>("objectName");
    QTest::addColumn<QString>("methodName");
    QTest::addColumn<QString>("parameterName");
    QTest::addColumn<bool>("isValid");
    QTest::addColumn<WmiParameter::CimType>("cimType");
    QTest::addColumn<QString>("cimTypeName");
    QTest::addColumn<QList<WmiQualifier>>("qualifiers");
    QTest::addColumn<bool>("isInput");
    QTest::addColumn<bool>("isOutput");
    QTest::addColumn<QString>("qDebugText");

    QTest::newRow("[Test_WmiTestInterface].GetProperties(StringProperty)")
            << QString("Test_WmiTestInterface")
            << QString("GetProperties")
            << QString("StringProperty")
            << true
            << WmiParameter::String
            << QString("String")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("string"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("ID"), 0, WmiFlavor(false, false, false, false)),
                                    WmiQualifier(QLatin1String("out"), true, WmiFlavor(false, false, false, false))})
            << false
            << true
            << QString("WmiParameter([Test_WmiTestInterface].GetProperties(StringProperty)) ");

    QTest::newRow("[Test_WmiTestInterface].GetProperties(NumberProperty)")
            << QString("Test_WmiTestInterface")
            << QString("GetProperties")
            << QString("NumberProperty")
            << true
            << WmiParameter::SInt32
            << QString("SInt32")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("sint32"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("ID"), 1, WmiFlavor(false, false, false, false)),
                                    WmiQualifier(QLatin1String("out"), true, WmiFlavor(false, false, false, false))})
            << false
            << true
            << QString("WmiParameter([Test_WmiTestInterface].GetProperties(NumberProperty)) ");

    QTest::newRow("[Test_WmiTestInterface].GetProperties(BooleanProperty)")
            << QString("Test_WmiTestInterface")
            << QString("GetProperties")
            << QString("BooleanProperty")
            << true
            << WmiParameter::Boolean
            << QString("Boolean")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("boolean"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("ID"), 2, WmiFlavor(false, false, false, false)),
                                    WmiQualifier(QLatin1String("out"), true, WmiFlavor(false, false, false, false))})
            << false
            << true
            << QString("WmiParameter([Test_WmiTestInterface].GetProperties(BooleanProperty)) ");

    QTest::newRow("[Test_WmiTestInterface].EchoString(inString)")
            << QString("Test_WmiTestInterface")
            << QString("EchoString")
            << QString("inString")
            << true
            << WmiParameter::String
            << QString("String")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("string"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("Description"), QLatin1String("The Input String"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("ID"), 0, WmiFlavor(false, false, false, false)),
                                    WmiQualifier(QLatin1String("in"), true, WmiFlavor(false, false, false, false))})
            << true
            << false
            << QString("WmiParameter([Test_WmiTestInterface].EchoString(inString)) ");

    QTest::newRow("[Test_WmiTestInterface].EchoString(outString)")
            << QString("Test_WmiTestInterface")
            << QString("EchoString")
            << QString("outString")
            << true
            << WmiParameter::String
            << QString("String")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("string"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("Description"), QLatin1String("The Output String"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("ID"), 1, WmiFlavor(false, false, false, false)),
                                    WmiQualifier(QLatin1String("out"), true, WmiFlavor(false, false, false, false))})
            << false
            << true
            << QString("WmiParameter([Test_WmiTestInterface].EchoString(outString)) ");

    QTest::newRow("[Test_WmiTestInterface.Id=1].GetProperties(StringProperty)")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("GetProperties")
            << QString("StringProperty")
            << true
            << WmiParameter::String
            << QString("String")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("string"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("ID"), 0, WmiFlavor(false, false, false, false)),
                                    WmiQualifier(QLatin1String("out"), true, WmiFlavor(false, false, false, false))})
            << false
            << true
            << QString("WmiParameter([Test_WmiTestInterface.Id=1].GetProperties(StringProperty)) ");

    QTest::newRow("[Test_WmiTestInterface.Id=1].GetProperties(NumberProperty)")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("GetProperties")
            << QString("NumberProperty")
            << true
            << WmiParameter::SInt32
            << QString("SInt32")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("sint32"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("ID"), 1, WmiFlavor(false, false, false, false)),
                                    WmiQualifier(QLatin1String("out"), true, WmiFlavor(false, false, false, false))})
            << false
            << true
            << QString("WmiParameter([Test_WmiTestInterface.Id=1].GetProperties(NumberProperty)) ");

    QTest::newRow("[Test_WmiTestInterface.Id=1].GetProperties(BooleanProperty)")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("GetProperties")
            << QString("BooleanProperty")
            << true
            << WmiParameter::Boolean
            << QString("Boolean")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("boolean"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("ID"), 2, WmiFlavor(false, false, false, false)),
                                    WmiQualifier(QLatin1String("out"), true, WmiFlavor(false, false, false, false))})
            << false
            << true
            << QString("WmiParameter([Test_WmiTestInterface.Id=1].GetProperties(BooleanProperty)) ");

    QTest::newRow("[Test_WmiTestInterface.Id=1].EchoString(inString)")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("EchoString")
            << QString("inString")
            << true
            << WmiParameter::String
            << QString("String")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("string"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("Description"), QLatin1String("The Input String"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("ID"), 0, WmiFlavor(false, false, false, false)),
                                    WmiQualifier(QLatin1String("in"), true, WmiFlavor(false, false, false, false))})
            << true
            << false
            << QString("WmiParameter([Test_WmiTestInterface.Id=1].EchoString(inString)) ");

    QTest::newRow("[Test_WmiTestInterface.Id=1].EchoString(outString)")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("EchoString")
            << QString("outString")
            << true
            << WmiParameter::String
            << QString("String")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("CIMTYPE"), QLatin1String("string"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("Description"), QLatin1String("The Output String"), WmiFlavor(false, true, true, false)),
                                    WmiQualifier(QLatin1String("ID"), 1, WmiFlavor(false, false, false, false)),
                                    WmiQualifier(QLatin1String("out"), true, WmiFlavor(false, false, false, false))})
            << false
            << true
            << QString("WmiParameter([Test_WmiTestInterface.Id=1].EchoString(outString)) ");

    if(includeInvalid) {
        QTest::newRow("[Test_WmiTestInterface].GetProperties(NonExistantParameter)")
                << QString("Test_WmiTestInterface")
                << QString("GetProperties")
                << QString("NonExistantParameter")
                << false
                << WmiParameter::Empty
                << QString()
                << QList<WmiQualifier>({})
                << false
                << false
                << QString("WmiParameter() ");

        QTest::newRow("[Test_WmiTestInterface.Id=1].GetProperties(NonExistantParameter)")
                << QString("Test_WmiTestInterface.Id=1")
                << QString("GetProperties")
                << QString("NonExistantParameter")
                << false
                << WmiParameter::Empty
                << QString()
                << QList<WmiQualifier>({})
                << false
                << false
                << QString("WmiParameter() ");
    }
}

QTEST_MAIN(WmiParameterTest)

#include "tst_wmiparametertest.moc"
