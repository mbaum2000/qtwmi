#include <QString>
#include <QtTest>

#include "wmitest.h"

#include "wmimethod.h"
#include "wmiconnection.h"
#include "wmimethodcall.h"
#include "wmierror.h"

class WmiMethodTest : public WmiTest {
    Q_OBJECT
private Q_SLOTS:
    void testDefaultConstructor();
    void testCopyContructor();
    void testCopyContructor_data();
    void testComparisonOperators();
    void testComparisonOperators_data();
    void testAssignmentOperators();
    void testAssignmentOperators_data();
    void testMethodIntrospection();
    void testMethodIntrospection_data();
    void testExecMethod();
    void testExecMethod_data();
    void testExecWait();
    void testExecWait_data();
    void testMethodQualifiers();
    void testMethodQualifiers_data();
    void testQDebug();
    void testQDebug_data();

private:
    void test_data(bool includeInvalid = true);
};


void WmiMethodTest::testDefaultConstructor() {
    WmiMethod method;
    QVERIFY(method.isNull());
    QVERIFY(!method.isValid());
    QCOMPARE(method.name(), QString());
    QCOMPARE(method.inputParameters(), QStringList());
    QCOMPARE(method.outputParameters(), QStringList());
    QCOMPARE(method.origin(), QString());
    QCOMPARE(method.qualifiers(), QList<WmiQualifier>());
    QCOMPARE(method.cimType(), WmiProperty::Empty);
    QCOMPARE(method.cimTypeName(), QString());
}

void WmiMethodTest::testCopyContructor() {
    QFETCH(QString, objectName);
    QFETCH(QString, methodName);

    WmiConnection connection(wmiTestNamespace());

    WmiMethod method1 = connection.getObject(objectName)
                                  .method(methodName);
    WmiMethod method2(method1);

    QVERIFY(method1 == method2);
    QVERIFY(!(method1 != method2));
    QVERIFY(method1.object() == method2.object());
    QVERIFY(method1.name() == method2.name());
    QVERIFY(method1.inputParameters() == method2.inputParameters());
    QVERIFY(method1.outputParameters() == method2.outputParameters());
    QVERIFY(method1.origin() == method2.origin());
    QVERIFY(method1.qualifiers() == method2.qualifiers());
    QVERIFY(method1.cimType() == method2.cimType());
    QVERIFY(method1.cimTypeName() == method2.cimTypeName());
}

void WmiMethodTest::testCopyContructor_data() {
    test_data();
}

void WmiMethodTest::testComparisonOperators() {
    QFETCH(QString, objectName);
    QFETCH(QString, methodName);

    WmiConnection connection(wmiTestNamespace());

    WmiMethod method0;
    WmiMethod method1 = connection.getObject(objectName)
                                  .method(methodName);
    WmiMethod method2 = connection.getObject(QLatin1String("Test_WmiTestInterface.Id=3"))
                                  .method(methodName);
    WmiMethod method3 = connection.getObject(objectName)
                                  .method(methodName);

    QVERIFY(method0 != method1);
    QVERIFY(method1 != method2);
    QVERIFY(method1 == method3);

    QVERIFY(!(method0 == method1));
    QVERIFY(!(method1 == method2));
    QVERIFY(!(method1 != method3));
}

void WmiMethodTest::testComparisonOperators_data() {
    test_data(false);
}

void WmiMethodTest::testAssignmentOperators() {
    QFETCH(QString, objectName);
    QFETCH(QString, methodName);

    WmiConnection connection(wmiTestNamespace());

    WmiMethod method1 = connection.getObject(objectName)
                                  .method(methodName);
    WmiMethod method2 = method1;

    QVERIFY(method1 == method2);
    QVERIFY(!(method1 != method2));
    QVERIFY(method1.object() == method2.object());
    QVERIFY(method1.name() == method2.name());
    QVERIFY(method1.inputParameters() == method2.inputParameters());
    QVERIFY(method1.outputParameters() == method2.outputParameters());
    QVERIFY(method1.origin() == method2.origin());
    QVERIFY(method1.qualifiers() == method2.qualifiers());
    QVERIFY(method1.cimType() == method2.cimType());
    QVERIFY(method1.cimTypeName() == method2.cimTypeName());
}

void WmiMethodTest::testAssignmentOperators_data() {
    test_data();
}

void WmiMethodTest::testMethodIntrospection() {
    QFETCH(QString, objectName);
    QFETCH(QString, methodName);
    QFETCH(bool, isValid);
    QFETCH(QStringList, inputParameters);
    QFETCH(QStringList, outputParameters);
    QFETCH(QString, origin);
    QFETCH(QList<WmiQualifier>, qualifiers);
    QFETCH(WmiProperty::CimType, returnCimType);
    QFETCH(QString, returnCimTypeName);

    WmiConnection connection(wmiTestNamespace());

    WmiObject object = connection.getObject(objectName);
    WmiMethod method = object.method(methodName);

    if(isValid) {
        QVERIFY(method.isValid());
        QCOMPARE(method.name(), methodName);
    } else {
        QVERIFY(!method.isValid());
        QCOMPARE(method.name(), QString());
    }
    QCOMPARE(method.inputParameters(), inputParameters);
    QCOMPARE(method.outputParameters(), outputParameters);
    QCOMPARE(method.origin(), origin);
    QCOMPARE(method.qualifiers(), qualifiers);
    QCOMPARE(method.cimType(), returnCimType);
    QCOMPARE(method.cimTypeName(), returnCimTypeName);
}

void WmiMethodTest::testMethodIntrospection_data() {
    test_data();
}

void WmiMethodTest::testExecMethod() {
    QFETCH(QString, objectName);
    QFETCH(QString, methodName);
    QFETCH(bool, isValid);
    QFETCH(QVariant, returnValue);
    QFETCH(QVariantMap, inputParametersMap);
    QFETCH(QVariantMap, outputParametersMap);
    QFETCH(WmiError::Error, error);

    WmiConnection connection(wmiTestNamespace());
    WmiObject object = connection.getObject(objectName);
    WmiMethod method = object.method(methodName);
    WmiMethodCall *call = method.exec(QVariantList(), inputParametersMap);

    if(isValid) {
        QVERIFY(call != Q_NULLPTR);

        call->waitForFinished();

        QCOMPARE(call->returnValue(), returnValue);
        QCOMPARE(call->inputParameterValues(), inputParametersMap);
        QCOMPARE(call->outputParameterValues(), outputParametersMap);
    } else {
        QVERIFY(call == Q_NULLPTR);
    }
    QCOMPARE(method.error().error(), error);
}

void WmiMethodTest::testExecMethod_data() {
    test_data();
}

void WmiMethodTest::testExecWait() {
    QFETCH(QString, objectName);
    QFETCH(QString, methodName);
    QFETCH(QVariant, returnValue);
    QFETCH(QVariantMap, inputParametersMap);
    QFETCH(QVariantMap, outputParametersMap);
    QFETCH(WmiError::Error, error);

    WmiConnection connection(wmiTestNamespace());
    WmiObject object = connection.getObject(objectName);
    WmiMethod method = object.method(methodName);
    QVariantMap outputParameterValues;
    QVariant value = method.execWait(QVariantList(), inputParametersMap, outputParameterValues);

    QCOMPARE(value, returnValue);
    QCOMPARE(outputParameterValues, outputParametersMap);
    QCOMPARE(method.error().error(), error);
}

void WmiMethodTest::testExecWait_data() {
    test_data();
}

void WmiMethodTest::testMethodQualifiers() {
    QFETCH(QString, objectName);
    QFETCH(QString, methodName);
    QFETCH(WmiQualifier, qualifier);
    QFETCH(QString, qualifierName);

    WmiConnection connection(wmiTestNamespace());

    WmiObject object = connection.getObject(objectName);
    WmiMethod method = object.method(methodName);

    WmiQualifier methodQualifier = method.qualifier(qualifierName);

    QCOMPARE(methodQualifier.name(), qualifier.name());
    QCOMPARE(methodQualifier.value(), qualifier.value());
    QCOMPARE(methodQualifier.flavor(), qualifier.flavor());

    QCOMPARE(methodQualifier, qualifier);

    if(!qualifier.name().isEmpty()) {
        QVERIFY(method.qualifiers().contains(qualifier));
    } else {
        QVERIFY(!method.qualifiers().contains(qualifier));
    }
}

void WmiMethodTest::testMethodQualifiers_data() {
    QTest::addColumn<QString>("objectName");
    QTest::addColumn<QString>("methodName");
    QTest::addColumn<QString>("qualifierName");
    QTest::addColumn<WmiQualifier>("qualifier");

    // These tests are commented out because for some reason [ManagementQualifier] is not honored
    // when [ManagementTask] is used.  This is an issue in the provider, not QtWMI, so this is
    // being commented out until I can fix it in the C# provider and get it into the generated MOF.
//    QTest::newRow("[Test_WmiTestInterface].EchoString[Description]")
//            << QString("Test_WmiTestInterface")
//            << QString("EchoString")
//            << QString("Description")
//            << WmiQualifier(QLatin1String("Description"),
//                            QVariant("Takes the value of inString and returns it in outString"),
//                            WmiFlavor(false, true, true, false));

//    QTest::newRow("[Test_WmiTestInterface].GetProperties[Description]")
//            << QString("Test_WmiTestInterface")
//            << QString("GetProperties")
//            << QString("Description")
//            << WmiQualifier(QLatin1String("Description"),
//                            QVariant("Returns the values of the String, Number, and Boolean properties as output parameters"),
//                            WmiFlavor(false, true, true, false));

    QTest::newRow("[Test_WmiTestInterface].EchoString[static]")
            << QString("Test_WmiTestInterface")
            << QString("EchoString")
            << QString("static")
            << WmiQualifier(QLatin1String("static"),
                            true,
                            WmiFlavor(false, false, false, false));

    QTest::newRow("[Test_WmiTestInterface].GetProperties[static]")
            << QString("Test_WmiTestInterface")
            << QString("GetProperties")
            << QString("static")
            << WmiQualifier(QString(),
                            QVariant(),
                            WmiFlavor());

//    QTest::newRow("[Test_WmiTestInterface.Id=1].EchoString[Description]")
//            << QString("Test_WmiTestInterface.Id=1")
//            << QString("EchoString")
//            << QString("Description")
//            << WmiQualifier(QLatin1String("Description"),
//                            QVariant("Takes the value of inString and returns it in outString"),
//                            WmiFlavor(false, true, true, false));

//    QTest::newRow("[Test_WmiTestInterface.Id=1].GetProperties[Description]")
//            << QString("Test_WmiTestInterface.Id=1")
//            << QString("GetProperties")
//            << QString("Description")
//            << WmiQualifier(QLatin1String("Description"),
//                            QVariant("Returns the values of the String, Number, and Boolean properties as output parameters"),
//                            WmiFlavor(false, true, true, false));

    QTest::newRow("[Test_WmiTestInterface.Id=1].EchoString[static]")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("EchoString")
            << QString("static")
            << WmiQualifier(QLatin1String("static"),
                            true,
                            WmiFlavor(false, false, false, false));

    QTest::newRow("[Test_WmiTestInterface.Id=1].GetProperties[static]")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("GetProperties")
            << QString("static")
            << WmiQualifier(QString(),
                            QVariant(),
                            WmiFlavor());
}

void WmiMethodTest::testQDebug() {
    QFETCH(QString, objectName);
    QFETCH(QString, methodName);
    QFETCH(QString, qDebugText);

    WmiConnection connection(wmiTestNamespace());
    WmiObject object = connection.getObject(objectName);
    WmiMethod method = object.method(methodName);

    QString debugOut;
    QDebug debug(&debugOut);
    debug << method;
    QCOMPARE(debugOut, qDebugText);
}

void WmiMethodTest::testQDebug_data() {
    test_data();
}

void WmiMethodTest::test_data(bool includeInvalid) {
    QTest::addColumn<QString>("objectName");
    QTest::addColumn<QString>("methodName");
    QTest::addColumn<bool>("isValid");
    QTest::addColumn<QStringList>("inputParameters");
    QTest::addColumn<QStringList>("outputParameters");
    QTest::addColumn<QString>("origin");
    QTest::addColumn<QList<WmiQualifier>>("qualifiers");
    QTest::addColumn<WmiProperty::CimType>("returnCimType");
    QTest::addColumn<QString>("returnCimTypeName");

    QTest::addColumn<QVariant>("returnValue");
    QTest::addColumn<QVariantMap>("inputParametersMap");
    QTest::addColumn<QVariantMap>("outputParametersMap");
    QTest::addColumn<WmiError::Error>("error");

    QTest::addColumn<QString>("qDebugText");

    QTest::newRow("[Test_WmiTestInterface.Id=1].GetProperties")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("GetProperties")
            << true
            << QStringList({})
            << QStringList({QLatin1String("BooleanProperty"), QLatin1String("NumberProperty"), QLatin1String("StringProperty")})
            << QString("Test_WmiTestInterface")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("implemented"), true, WmiFlavor(false, false, false, false))})
            << WmiProperty::Boolean
            << QString("Boolean")
            << QVariant(true)
            << QVariantMap({})
            << QVariantMap({{"StringProperty", "foo"},
                            {"NumberProperty", 42},
                            {"BooleanProperty", true}})
            << WmiError::NoError
            << QString("WmiMethod([\"Test_WmiTestInterface.Id=1\"].\"GetProperties\") ");

    QTest::newRow("[Test_WmiTestInterface.Id=2].GetProperties")
            << QString("Test_WmiTestInterface.Id=2")
            << QString("GetProperties")
            << true
            << QStringList({})
            << QStringList({QLatin1String("BooleanProperty"), QLatin1String("NumberProperty"), QLatin1String("StringProperty")})
            << QString("Test_WmiTestInterface")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("implemented"), true, WmiFlavor(false, false, false, false))})
            << WmiProperty::Boolean
            << QString("Boolean")
            << QVariant(true)
            << QVariantMap({})
            << QVariantMap({{"StringProperty", "bar"},
                            {"NumberProperty", 256},
                            {"BooleanProperty", false}})
            << WmiError::NoError
            << QString("WmiMethod([\"Test_WmiTestInterface.Id=2\"].\"GetProperties\") ");

    QTest::newRow("[Test_WmiTestInterface].EchoString")
            << QString("Test_WmiTestInterface")
            << QString("EchoString")
            << true
            << QStringList({QLatin1String("inString")})
            << QStringList({QLatin1String("outString")})
            << QString("Test_WmiTestInterface")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("implemented"), true, WmiFlavor(false, false, false, false)),
                                    WmiQualifier(QLatin1String("static"), true, WmiFlavor(false, false, false, false))})
            << WmiProperty::Boolean
            << QString("Boolean")
            << QVariant(true)
            << QVariantMap({{"inString", "Lorem Ipsum"}})
            << QVariantMap({{"outString", "Lorem Ipsum"}})
            << WmiError::NoError
            << QString("WmiMethod([\"Test_WmiTestInterface\"].\"EchoString\") ");

    QTest::newRow("[Test_WmiTestInterface.Id=1].EchoString")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("EchoString")
            << true
            << QStringList({QLatin1String("inString")})
            << QStringList({QLatin1String("outString")})
            << QString("Test_WmiTestInterface")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("implemented"), true, WmiFlavor(false, false, false, false)),
                                    WmiQualifier(QLatin1String("static"), true, WmiFlavor(false, false, false, false))})
            << WmiProperty::Boolean
            << QString("Boolean")
            << QVariant(true)
            << QVariantMap({{"inString", "Lorem Ipsum"}})
            << QVariantMap({{"outString", "Lorem Ipsum"}})
            << WmiError::NoError
            << QString("WmiMethod([\"Test_WmiTestInterface.Id=1\"].\"EchoString\") ");

    QTest::newRow("[Test_WmiTestInterface.Id=1].EchoString /*Parameter TypeMismatch*/")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("EchoString")
            << true
            << QStringList({QLatin1String("inString")})
            << QStringList({QLatin1String("outString")})
            << QString("Test_WmiTestInterface")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("implemented"), true, WmiFlavor(false, false, false, false)),
                                    WmiQualifier(QLatin1String("static"), true, WmiFlavor(false, false, false, false))})
            << WmiProperty::Boolean
            << QString("Boolean")
            << QVariant(true)
            << QVariantMap({{"inString", QVariantList({42, 2197})}})
            << QVariantMap({{"outString", QVariant()}})
            << WmiError::TypeMismatch
            << QString("WmiMethod([\"Test_WmiTestInterface.Id=1\"].\"EchoString\") ");

    QTest::newRow("[Test_WmiTestInterface.Id=1].EchoString /*Parameter NotFound*/")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("EchoString")
            << true
            << QStringList({QLatin1String("inString")})
            << QStringList({QLatin1String("outString")})
            << QString("Test_WmiTestInterface")
            << QList<WmiQualifier>({WmiQualifier(QLatin1String("implemented"), true, WmiFlavor(false, false, false, false)),
                                    WmiQualifier(QLatin1String("static"), true, WmiFlavor(false, false, false, false))})
            << WmiProperty::Boolean
            << QString("Boolean")
            << QVariant(true)
            << QVariantMap({{"inputString", "Lorem Ipsum"}})
            << QVariantMap({{"outString", QVariant()}})
            << WmiError::NotFound
            << QString("WmiMethod([\"Test_WmiTestInterface.Id=1\"].\"EchoString\") ");

    if(includeInvalid) {
        QTest::newRow("[Test_WmiTestInterface.Id=1].Foo")
                << QString("Test_WmiTestInterface.Id=1")
                << QString("Foo")
                << false
                << QStringList({})
                << QStringList({})
                << QString()
                << QList<WmiQualifier>({})
                << WmiProperty::Empty
                << QString()
                << QVariant()
                << QVariantMap({})
                << QVariantMap({})
                << WmiError::InvalidMethod
                << QString("WmiMethod() ");
    }
}

QTEST_MAIN(WmiMethodTest)

#include "tst_wmimethodtest.moc"
