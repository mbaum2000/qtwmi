include($${top_srcdir}/src/libqtwmi.pri)

QT += testlib
QT -= gui

TARGET = tst_wmiqualifiertest
CONFIG += console
CONFIG += testcase
CONFIG -= app_bundle

TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += tst_wmiqualifiertest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
