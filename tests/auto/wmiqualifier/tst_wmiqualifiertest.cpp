#include <QString>
#include <QtTest>

#include "wmiqualifier.h"

class WmiQualifierTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testDefaultConstructor();
    void testPublicConstructor();
    void testPublicConstructor_data();
    void testCopyContructor();
    void testCopyContructor_data();
    void testComparisonOperators();
    void testComparisonOperators_data();
    void testAssignmentOperators();
    void testAssignmentOperators_data();
    void testAccessors();
    void testAccessors_data();
    void testQVariant();
    void testQVariant_data();
    void testQDebug();
    void testQDebug_data();
    void testQVariantQDebug();
    void testQVariantQDebug_data();

private:
    void test_data();
};


void WmiQualifierTest::testDefaultConstructor() {
    WmiQualifier qualifier;
    QCOMPARE(qualifier.name(), QString());
    QCOMPARE(qualifier.value(), QVariant());
    QCOMPARE(qualifier.flavor(), WmiFlavor());
}

void WmiQualifierTest::testPublicConstructor() {
    QFETCH(QString, name);
    QFETCH(QVariant, value);
    QFETCH(WmiFlavor, flavor);

    WmiQualifier qualifier(name, value, flavor);
    QCOMPARE(qualifier.name(), name);
    QCOMPARE(qualifier.value(), value);
    QCOMPARE(qualifier.flavor(), flavor);
}

void WmiQualifierTest::testPublicConstructor_data() {
    test_data();
}

void WmiQualifierTest::testCopyContructor() {
    QFETCH(QString, name);
    QFETCH(QVariant, value);
    QFETCH(WmiFlavor, flavor);

    WmiQualifier qualifier1(name, value, flavor);
    WmiQualifier qualifier2(qualifier1);

    QCOMPARE(qualifier2.name(), name);
    QCOMPARE(qualifier2.value(), value);
    QCOMPARE(qualifier2.flavor(), flavor);
}

void WmiQualifierTest::testCopyContructor_data() {
    test_data();
}

void WmiQualifierTest::testComparisonOperators() {
    QFETCH(QString, name);
    QFETCH(QVariant, value);
    QFETCH(WmiFlavor, flavor);

    WmiQualifier qualifier0;
    WmiQualifier qualifier1(name, value, flavor);
    WmiQualifier qualifier2(QLatin1String("Foo"), QVariant(true), WmiFlavor());
    WmiQualifier qualifier3(name, value, flavor);

    QVERIFY(qualifier0 != qualifier1);
    QVERIFY(qualifier1 != qualifier2);
    QVERIFY(qualifier1 == qualifier3);

    QVERIFY(!(qualifier0 == qualifier1));
    QVERIFY(!(qualifier1 == qualifier2));
    QVERIFY(!(qualifier1 != qualifier3));
}

void WmiQualifierTest::testComparisonOperators_data() {
    test_data();
}

void WmiQualifierTest::testAssignmentOperators() {
    QFETCH(QString, name);
    QFETCH(QVariant, value);
    QFETCH(WmiFlavor, flavor);

    WmiQualifier qualifier1(name, value, flavor);
    WmiQualifier qualifier2 = qualifier1;

    QCOMPARE(qualifier2.name(), name);
    QCOMPARE(qualifier2.value(), value);
    QCOMPARE(qualifier2.flavor(), flavor);
}

void WmiQualifierTest::testAssignmentOperators_data() {
    test_data();
}

void WmiQualifierTest::testAccessors() {
    QFETCH(QString, name);
    QFETCH(QVariant, value);
    QFETCH(WmiFlavor, flavor);

    WmiQualifier qualifier;
    qualifier.setName(name);
    qualifier.setValue(value);
    qualifier.setFlavor(flavor);

    QCOMPARE(qualifier.name(), name);
    QCOMPARE(qualifier.value(), value);
    QCOMPARE(qualifier.flavor(), flavor);
}

void WmiQualifierTest::testAccessors_data() {
    test_data();
}

void WmiQualifierTest::testQVariant() {
    QFETCH(QString, name);
    QFETCH(QVariant, value);
    QFETCH(WmiFlavor, flavor);

    WmiQualifier qualifier(name, value, flavor);
    QVariant variant = QVariant::fromValue<WmiQualifier>(qualifier);

    WmiQualifier qualifierCopy = variant.value<WmiQualifier>();
    QVariant variantCopy = qualifierCopy;

    QVERIFY(qualifier == variant);
    QVERIFY(!(qualifier != variant));

    QVERIFY(variant == qualifier);
    QVERIFY(!(variant != qualifier));

    QCOMPARE(qualifier, qualifierCopy);
    QCOMPARE(variant, variantCopy);

    QVERIFY(variant == variantCopy);
    QVERIFY(!(variant != variantCopy));
}

void WmiQualifierTest::testQVariant_data() {
    test_data();
}

void WmiQualifierTest::testQDebug() {
    QFETCH(QString, name);
    QFETCH(QVariant, value);
    QFETCH(WmiFlavor, flavor);
    QFETCH(QString, qDebugText);

    WmiQualifier qualifier(name, value, flavor);

    QString debugOut;
    QDebug debug(&debugOut);
    debug << qualifier;
    QCOMPARE(debugOut, qDebugText);
}

void WmiQualifierTest::testQDebug_data() {
    test_data();
}

void WmiQualifierTest::testQVariantQDebug() {
    QFETCH(QString, name);
    QFETCH(QVariant, value);
    QFETCH(WmiFlavor, flavor);
    QFETCH(QString, qDebugText);

    WmiQualifier qualifier(name, value, flavor);

    QString debugVariantOut;
    QString expectedDebugVariantOut = QString("QVariant(WmiQualifier, %1) ").arg(qDebugText);
    QDebug debugVariant(&debugVariantOut);
    debugVariant << QVariant::fromValue<WmiQualifier>(qualifier);
    QCOMPARE(debugVariantOut, expectedDebugVariantOut);
}

void WmiQualifierTest::testQVariantQDebug_data() {
    test_data();
}

void WmiQualifierTest::test_data() {
    QTest::addColumn<QString>("name");
    QTest::addColumn<QVariant>("value");
    QTest::addColumn<WmiFlavor>("flavor");
    QTest::addColumn<QString>("qDebugText");

    QTest::newRow("WmiQualifier(Key(true) : ToInstance, ToSubClass, DisableOverride)")
            << QString("Key")
            << QVariant(true)
            << WmiFlavor(false, true, true, true)
            << QString("WmiQualifier(Key(true) : ToInstance, ToSubClass, DisableOverride) ");

    QTest::newRow("WmiQualifier(Description(\"The Description\") : ToInstance, ToSubClass)")
            << QString("Description")
            << QVariant("The Description")
            << WmiFlavor(false, true, true, false)
            << QString("WmiQualifier(Description(\"The Description\") : ToInstance, ToSubClass) ");
}

QTEST_APPLESS_MAIN(WmiQualifierTest)

#include "tst_wmiqualifiertest.moc"
