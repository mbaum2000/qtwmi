#include <QString>
#include <QtTest>

#include "wmitest.h"

#include "wmiobject.h"
#include "wmiobjectrequest.h"
#include "wmiconnection.h"
#include "wmierror.h"

class WmiObjectTest : public WmiTest {
    Q_OBJECT
private Q_SLOTS:
    void testDefaultConstructor();
    void testCopyContructor();
    void testCopyContructor_data();
    void testComparisonOperators();
    void testComparisonOperators_data();
    void testAssignmentOperators();
    void testAssignmentOperators_data();
    void testAccessors();
    void testAccessors_data();
    void testListProperties();
    void testListProperties_data();
    void testGetProperty();
    void testSetProperty();
    void testListMethods();
    void testListMethods_data();
    void testSpawnInstance();
    void testSpawnInstance_data();
    void testGetClassDefinition();
    void testGetClassDefinition_data();
    void testObjectQualifiers();
    void testObjectQualifiers_data();
    void testQDebug();
    void testQDebug_data();

private:
    void test_data(bool includeInvalid = true);
};


void WmiObjectTest::testDefaultConstructor() {
    WmiObject object;

    QVERIFY(object.isNull());
    QVERIFY(!object.isValid());
    QCOMPARE(object.className(), QString());
    QCOMPARE(object.objectPath(), QString());
    QCOMPARE(object.connection(), Q_NULLPTR);
    QCOMPARE(object.isInstance(), false);
    QCOMPARE(object.properties(), QStringList());
    QCOMPARE(object.allProperties(), QStringList());
    QCOMPARE(object.methods(), QStringList());
    QCOMPARE(object.qualifiers(), QList<WmiQualifier>());
}

void WmiObjectTest::testCopyContructor() {
    QFETCH(QString, objectName);

    WmiConnection connection(wmiTestNamespace());

    WmiObject object1 = connection.getObject(objectName);
    WmiObject object2(object1);

    QVERIFY(object1 == object2);
    QVERIFY(!(object1 != object2));
    QVERIFY(object1.className() == object2.className());
    QVERIFY(object1.objectPath() == object2.objectPath());
    QVERIFY(object1.connection() == object2.connection());
    QVERIFY(object1.isInstance() == object2.isInstance());
    QVERIFY(object1.properties() == object2.properties());
    QVERIFY(object1.allProperties() == object2.allProperties());
    QVERIFY(object1.methods() == object2.methods());
    QVERIFY(object1.qualifiers() == object2.qualifiers());
}

void WmiObjectTest::testCopyContructor_data() {
    test_data();
}

void WmiObjectTest::testComparisonOperators() {
    QFETCH(QString, objectName);

    WmiConnection connection(wmiTestNamespace());

    WmiObject object0;
    WmiObject object1 = connection.getObject(objectName);
    WmiObject object2 = connection.getObject(QLatin1String("Test_WmiTestInterface.Id=2000"));
    WmiObject object3 = connection.getObject(objectName);

    QVERIFY(object0 != object1);
    QVERIFY(object1 != object2);
    QVERIFY(object1 == object3);

    QVERIFY(!(object0 == object1));
    QVERIFY(!(object1 == object2));
    QVERIFY(!(object1 != object3));
}

void WmiObjectTest::testComparisonOperators_data() {
    test_data(false);
}

void WmiObjectTest::testAssignmentOperators() {
    QFETCH(QString, objectName);

    WmiConnection connection(wmiTestNamespace());

    WmiObject object1 = connection.getObject(objectName);
    WmiObject object2 = object1;

    QVERIFY(object1 == object2);
    QVERIFY(!(object1 != object2));
    QVERIFY(object1.className() == object2.className());
    QVERIFY(object1.objectPath() == object2.objectPath());
    QVERIFY(object1.connection() == object2.connection());
    QVERIFY(object1.isInstance() == object2.isInstance());
    QVERIFY(object1.properties() == object2.properties());
    QVERIFY(object1.allProperties() == object2.allProperties());
    QVERIFY(object1.methods() == object2.methods());
    QVERIFY(object1.qualifiers() == object2.qualifiers());
}

void WmiObjectTest::testAssignmentOperators_data() {
    test_data();
}

void WmiObjectTest::testAccessors() {
    QFETCH(QString, objectName);
    QFETCH(QString, className);
    QFETCH(bool, isValid);
    QFETCH(bool, isInstance);

    WmiConnection connection(wmiTestNamespace());

    WmiObject object = connection.getObject(objectName);

    QCOMPARE(object.isValid(), isValid);
    QCOMPARE(object.className(), className);
    QCOMPARE(object.objectPath(), objectName);
    QCOMPARE(object.isInstance(), isInstance);
}

void WmiObjectTest::testAccessors_data() {
    test_data(false);
}

void WmiObjectTest::testListProperties() {
    QFETCH(QString, objectName);
    QFETCH(bool, isValid);
    QFETCH(QStringList, properties);

    QStringList allProperties({"__PATH",
                               "__NAMESPACE",
                               "__SERVER",
                               "__DERIVATION",
                               "__PROPERTY_COUNT",
                               "__RELPATH",
                               "__DYNASTY",
                               "__SUPERCLASS",
                               "__CLASS",
                               "__GENUS"});
    allProperties.append(properties);

    WmiConnection connection(wmiTestNamespace());

    WmiObject object = connection.getObject(objectName);

    if(isValid) {
        QCOMPARE(object.error().error(), WmiError::NoError);
        QCOMPARE(object.properties(), properties);
        QCOMPARE(object.error().error(), WmiError::NoError);
        QCOMPARE(object.allProperties(), allProperties);
        QCOMPARE(object.error().error(), WmiError::NoError);
    } else {
        QCOMPARE(object.error().error(), WmiError::NotFound);
        QCOMPARE(object.properties(), QStringList());
        QCOMPARE(object.error().error(), WmiError::InvalidObject);
        QCOMPARE(object.allProperties(), QStringList());
        QCOMPARE(object.error().error(), WmiError::InvalidObject);
    }
}

void WmiObjectTest::testListProperties_data() {
    test_data();
}

// TODO: Split up into dataset
void WmiObjectTest::testGetProperty() {
    WmiConnection connection(wmiTestNamespace());
    WmiObject object = connection.getObject(QLatin1String("Test_WmiTestInterface.Id=1"));

    QCOMPARE(object.property("Id"), QVariant(1));
    QCOMPARE(object.error().error(), WmiError::NoError);

    QCOMPARE(object.property("StringProperty"), QVariant("foo"));
    QCOMPARE(object.error().error(), WmiError::NoError);

    QCOMPARE(object.property("NumberProperty"), QVariant(42));
    QCOMPARE(object.error().error(), WmiError::NoError);

    QCOMPARE(object.property("BooleanProperty"), QVariant(true));
    QCOMPARE(object.error().error(), WmiError::NoError);

    QCOMPARE(object.property("NonExistantProperty"), QVariant());
    QCOMPARE(object.error().error(), WmiError::NotFound);
}

void WmiObjectTest::testSetProperty() {
    WmiConnection connection(wmiTestNamespace());

    WmiObject o1 = connection.getObject(QLatin1String("Test_WmiTestInterface.Id=1"));
    WmiObject o2 = connection.getObject(QLatin1String("Test_WmiTestInterface.Id=1"));

    o1.setProperty("StringProperty", "blarg");
    o1.setProperty("NumberProperty", 5);
    o1.setProperty("BooleanProperty", false);
    o1.apply();

    // First copy is updated
    QCOMPARE(o1.property("StringProperty"), QVariant("blarg"));
    QCOMPARE(o1.property("NumberProperty"), QVariant(5));
    QCOMPARE(o1.property("BooleanProperty"), QVariant(false));

    // But second copy is not...
    QCOMPARE(o2.property("StringProperty"), QVariant("foo"));
    QCOMPARE(o2.property("NumberProperty"), QVariant(42));
    QCOMPARE(o2.property("BooleanProperty"), QVariant(true));

    o2.refresh();

    // ...until it is refreshed
    QCOMPARE(o2.property("StringProperty"), QVariant("blarg"));
    QCOMPARE(o2.property("NumberProperty"), QVariant(5));
    QCOMPARE(o2.property("BooleanProperty"), QVariant(false));

    WmiObject o3 = connection.getObject(QLatin1String("Test_WmiTestInterface.Id=1"));

    // New queries will have updated data
    QCOMPARE(o3.property("StringProperty"), QVariant("blarg"));
    QCOMPARE(o3.property("NumberProperty"), QVariant(5));
    QCOMPARE(o3.property("BooleanProperty"), QVariant(false));
}

void WmiObjectTest::testListMethods() {
    QFETCH(QString, objectName);
    QFETCH(bool, isValid);
    QFETCH(QStringList, methods);

    WmiConnection connection(wmiTestNamespace());

    WmiObject object = connection.getObject(objectName);    
    if(isValid) {
        QCOMPARE(object.error().error(), WmiError::NoError);

        QCOMPARE(object.methods(), methods);
        QCOMPARE(object.error().error(), WmiError::NoError);
    } else {
        QCOMPARE(object.error().error(), WmiError::NotFound);

        QCOMPARE(object.methods(), methods);
        QCOMPARE(object.error().error(), WmiError::InvalidObject);
    }
}

void WmiObjectTest::testListMethods_data() {
    test_data();
}

void WmiObjectTest::testSpawnInstance() {
    QFETCH(QString, objectPath);

    WmiConnection connection(wmiTestNamespace());
    WmiObject base = connection.getObject(objectPath);
    WmiObject object = base.spawnInstance();

    if(objectPath.isNull()) {
        QVERIFY(object.isNull());
    } else {
        QVERIFY(object.isValid());
        QVERIFY(object.isInstance());
        QCOMPARE(object.className(), base.className());
        QVERIFY(base != object);

        object.setProperty("Id", 4);
        object.setProperty("StringProperty", "blarg");
        object.setProperty("NumberProperty", 512);
        object.setProperty("BooleanProperty", true);
        object.apply();

        WmiObjectRequest *request = connection.execQuery("SELECT * FROM Test_WmiTestInterface WHERE Id = 4");
        request->waitForFinished();

        QVERIFY(request->objects().count() == 1);

        WmiObject result = request->objects().at(0);
        QCOMPARE(result.property("Id"), QVariant(4));
        QCOMPARE(result.property("StringProperty"), QVariant("blarg"));
        QCOMPARE(result.property("NumberProperty"), QVariant(512));
        QCOMPARE(result.property("BooleanProperty"), QVariant(true));
    }
}

void WmiObjectTest::testSpawnInstance_data() {
    QTest::addColumn<QString>("objectPath");

    QTest::newRow("Null")
            << QString();

    QTest::newRow("Test_WmiTestInterface")
            << QString("Test_WmiTestInterface");

    QTest::newRow("Test_WmiTestInterface.Id=1")
            << QString("Test_WmiTestInterface.Id=1");
}

void WmiObjectTest::testGetClassDefinition() {
    QFETCH(QString, objectPath);

    WmiConnection connection(wmiTestNamespace());
    WmiObject object = connection.getObject(objectPath);
    WmiObject cls = object.classObject();

    if(objectPath.isNull()) {
        QVERIFY(cls.isNull());
    } else {
        QVERIFY(cls.isValid());
        QVERIFY(!cls.isInstance());
        QCOMPARE(cls.className(), object.className());
        if(!object.isInstance()) {
            QVERIFY(cls == object);
        }
    }
}

void WmiObjectTest::testGetClassDefinition_data() {
    testSpawnInstance_data();
}

void WmiObjectTest::testObjectQualifiers() {
    QFETCH(QString, objectName);
    QFETCH(WmiQualifier, qualifier);
    QFETCH(QString, qualifierName);

    WmiConnection connection(wmiTestNamespace());

    WmiObject object = connection.getObject(objectName);
    QCOMPARE(object.error().error(), WmiError::NoError);

    WmiQualifier objectQualifier = object.qualifier(qualifierName);

    QCOMPARE(objectQualifier.name(), qualifier.name());
    QCOMPARE(objectQualifier.value(), qualifier.value());
    QCOMPARE(objectQualifier.flavor(), qualifier.flavor());

    QCOMPARE(objectQualifier, qualifier);

    if(!qualifier.name().isEmpty()) {
        QVERIFY(object.qualifiers().contains(qualifier));
        QCOMPARE(object.error().error(), WmiError::NoError);
    } else {
        QVERIFY(!object.qualifiers().contains(qualifier));
        QCOMPARE(object.error().error(), WmiError::NotFound);
    }
}

void WmiObjectTest::testObjectQualifiers_data() {
    QTest::addColumn<QString>("objectName");
    QTest::addColumn<QString>("qualifierName");
    QTest::addColumn<WmiQualifier>("qualifier");

    QTest::newRow("Test_WmiTestInterface[Description]")
            << QString("Test_WmiTestInterface")
            << QString("Description")
            << WmiQualifier(QLatin1String("Description"),
                            QVariant(QLatin1String("A WMI Test Interface")),
                            WmiFlavor(false, true, true, false));

    QTest::newRow("Test_WmiTestInterface.Id=1[Description]")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("Description")
            << WmiQualifier(QLatin1String("Description"),
                            QVariant(QLatin1String("A WMI Test Interface")),
                            WmiFlavor(false, true, true, false));

    QTest::newRow("Test_WmiTestInterface[NonExistant]")
            << QString("Test_WmiTestInterface")
            << QString("NonExistant")
            << WmiQualifier(QString(),
                            QVariant(),
                            WmiFlavor());
}

void WmiObjectTest::testQDebug() {
    QFETCH(QString, objectName);
    QFETCH(QString, qDebugText);

    WmiConnection connection(wmiTestNamespace());
    WmiObject object = connection.getObject(objectName);

    QString debugOut;
    QDebug debug(&debugOut);
    debug << object;
    QCOMPARE(debugOut, qDebugText);
}

void WmiObjectTest::testQDebug_data() {
    test_data();
}

void WmiObjectTest::test_data(bool includeInvalid) {
    QTest::addColumn<QString>("objectName");
    QTest::addColumn<QString>("className");
    QTest::addColumn<bool>("isValid");
    QTest::addColumn<QStringList>("properties");
    QTest::addColumn<QStringList>("methods");
    QTest::addColumn<bool>("isInstance");
    QTest::addColumn<QString>("qDebugText");

    QTest::newRow("Test_WmiTestInterface")
            << QString("Test_WmiTestInterface")
            << QString("Test_WmiTestInterface")
            << true
            << QStringList({QLatin1String("BooleanProperty"), QLatin1String("Id"), QLatin1String("NumberProperty"), QLatin1String("StringProperty")})
            << QStringList({QLatin1String("EchoString"), QLatin1String("GetProperties"), QLatin1String("AddChild")})
            << false
            << QString("WmiObject(Class, \"Test_WmiTestInterface\") ");

    QTest::newRow("Test_WmiTestInterface.Id=1")
            << QString("Test_WmiTestInterface.Id=1")
            << QString("Test_WmiTestInterface")
            << true
            << QStringList({QLatin1String("BooleanProperty"), QLatin1String("Id"), QLatin1String("NumberProperty"), QLatin1String("StringProperty")})
            << QStringList({QLatin1String("EchoString"), QLatin1String("GetProperties"), QLatin1String("AddChild")})
            << true
            << QString("WmiObject(Instance, \"Test_WmiTestInterface.Id=1\") ");

    if(includeInvalid) {
        QTest::newRow("Test_WmiTestInterface.Id=1000")
                << QString("Test_WmiTestInterface.Id=1000")
                << QString("Test_WmiTestInterface")
                << false
                << QStringList({})
                << QStringList({})
                << true
                << QString("WmiObject(Null) ");
    }
}

QTEST_MAIN(WmiObjectTest)

#include "tst_wmiobjecttest.moc"
