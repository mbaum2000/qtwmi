#include "wmitest.h"
#include "wmiconnection.h"
#include "wmiobject.h"

#include <QtTest>

WmiTest::WmiTest()
    : QObject(),
      m_com(new ComConnection) { }

QString WmiTest::wmiTestNamespace() {
    return QLatin1String("ROOT\\QtWmiTest");
}

void WmiTest::initTestCase() {
    WmiConnection connection(wmiTestNamespace());

    WmiObject setup = connection.getObject("Test_WmiTestInterfaceSetup.Id=0");
    QVERIFY2(setup.isValid(), "Error! WMI Test Provider is NOT Running");
}

void WmiTest::cleanupTestCase() {
    WmiConnection connection(wmiTestNamespace());

    WmiObject setup = connection.getObject("Test_WmiTestInterfaceSetup.Id=0");
    if(setup.isValid()) {
        setup.execMethod("Reset");
    }
}

void WmiTest::init() {
    WmiConnection connection(wmiTestNamespace());

    WmiObject setup = connection.getObject("Test_WmiTestInterfaceSetup.Id=0");
    bool setupSuccess = setup.isValid();
    if(setupSuccess) {
        setupSuccess &= setup.execMethod("Reset").toBool();
        setupSuccess &= setup.execMethod("AddClass",
                                         QVariantMap({{"Id", 1},
                                                      {"StringProperty", "foo"},
                                                      {"NumberProperty", 42},
                                                      {"BooleanProperty", true}})).toBool();
        setupSuccess &= setup.execMethod("AddClass",
                                         QVariantMap({{"Id", 2},
                                                      {"StringProperty", "bar"},
                                                      {"NumberProperty", 256},
                                                      {"BooleanProperty", false}})).toBool();
        setupSuccess &= setup.execMethod("AddClass",
                                         QVariantMap({{"Id", 3},
                                                      {"StringProperty", "baz"},
                                                      {"NumberProperty", 2048},
                                                      {"BooleanProperty", true}})).toBool();
    }
    QVERIFY2(setupSuccess, "Error! Unable to setup clean test data");
}
