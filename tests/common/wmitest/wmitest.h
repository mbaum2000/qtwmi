#ifndef WMITEST_H
#define WMITEST_H

#include <QObject>
#include <QScopedPointer>
#include "comconnection.h"

class WmiTest : public QObject {
    Q_OBJECT
public:
    WmiTest();

    static QString wmiTestNamespace();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void init();

private:
    QScopedPointer<ComConnection> m_com;
};


#endif // WMITEST_H
