#include "wminamespacetab.h"
#include "wmiquerywindow.h"
#include "wmiconnection.h"
#include "wmiobject.h"
#include "wmiobjectrequest.h"

#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QMdiSubWindow>
#include <QDateTime>
#include <QDebug>

#include <QProgressBar>

WmiNamespaceTab::WmiNamespaceTab(WmiConnection *connection, QWidget *parent)
    : QWidget(parent),
      m_connection(connection),
      m_isClosing(false) {
    setupUi(this);
    if(!m_connection) {
        return;
    }

    connect(this, SIGNAL(startLoading()), parent, SLOT(startLoading()));
    connect(this, SIGNAL(stopLoading()), parent, SLOT(stopLoading()));

    m_model = new QStandardItemModel(this);
    m_proxy = new QSortFilterProxyModel(this);
    m_proxy->setSourceModel(m_model);
    classList->setModel(m_proxy);
}

WmiNamespaceTab::~WmiNamespaceTab() { }

void WmiNamespaceTab::load() {
    Q_EMIT startLoading();
    loadModelItems();
    Q_EMIT stopLoading();
}

void WmiNamespaceTab::closeTab() {
    m_isClosing = true;
    this->deleteLater();
}

void WmiNamespaceTab::on_query_clicked() {
    if(!m_connection && !m_model && !m_proxy) {
        return;
    }
    WmiQueryWindow *wmiQuery = new WmiQueryWindow(m_connection, this);
    QMdiSubWindow *subWin = mdiArea->addSubWindow(wmiQuery);
    QStandardItem *item = m_model->itemFromIndex(m_proxy->mapToSource(classList->currentIndex()));
    if(item && item->data().toInt() == Class) {
        wmiQuery->loadQuery(item->text());
    } else {
        wmiQuery->loadQuery();
    }
    subWin->setWindowIcon(wmiQuery->windowIcon());
}

void WmiNamespaceTab::on_filter_textChanged(const QString &text) {
    if(!m_proxy) {
        return;
    }
    m_proxy->setFilterWildcard(text);
}

void WmiNamespaceTab::on_classList_activated(const QModelIndex &index) {
    if(!m_connection && !m_model) {
        return;
    }
    QStandardItem *item = m_model->itemFromIndex(m_proxy->mapToSource(index));
    if(item) {
        switch(item->data().toInt()) {
        case Namespace:
            emit namespaceRequested(QString("%1\\%2").arg(m_connection->wmiNamespace()).arg(item->text()));
            break;
        case Class:
            {
                WmiQueryWindow *wmiQuery = new WmiQueryWindow(m_connection, this);
                QMdiSubWindow *subWin = mdiArea->addSubWindow(wmiQuery);
                wmiQuery->loadClass(item->text());
                subWin->setWindowIcon(wmiQuery->windowIcon());
            }
            break;
        default:
            break;
        }
    }
}

void WmiNamespaceTab::loadModelItems() {
    WmiObjectRequest *nsRequest = m_connection->execQuery("SELECT * FROM __NAMESPACE");
    nsRequest->waitForFinished();
    for(const WmiObject &nspace : nsRequest->objects()) {
        QStandardItem *item = new QStandardItem(QIcon(":/icons/namespace.svg"), nspace.property("Name").toString());
        item->setEditable(false);
        item->setData(Namespace);
        m_model->appendRow(item);
        qApp->processEvents();
        if(m_isClosing) {
            return;
        }
    }
    WmiObjectRequest *clsRequest = m_connection->execQuery("SELECT * FROM meta_class");
    clsRequest->waitForFinished();
    for(const WmiObject &cls : clsRequest->objects()) {
        QStandardItem *item = new QStandardItem(QIcon(QString(":/icons/%1%2.svg")
                                                      .arg(cls.className().startsWith("__") ? "system-" : "")
                                                      .arg(cls.isInstance() ? "instance" : "class")),
                                                cls.className());
        item->setEditable(false);
        item->setData(Class);
        m_model->appendRow(item);
        qApp->processEvents();
        if(m_isClosing) {
            return;
        }
    }
}

void WmiNamespaceTab::changeEvent(QEvent *e) {
    QWidget::changeEvent(e);
    switch(e->type()) {
    case QEvent::LanguageChange:
        retranslateUi(this);
        break;
    default:
        break;
    }
}
