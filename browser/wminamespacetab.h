#ifndef WMINAMESPACETAB_H
#define WMINAMESPACETAB_H

#include "ui_wminamespacetab.h"
#include "wmiobject.h"

#include <QPointer>

class WmiConnection;
class QStandardItemModel;
class QSortFilterProxyModel;

class WmiNamespaceTab : public QWidget, private Ui::WmiNamespaceTab {
    Q_OBJECT
public:
    explicit WmiNamespaceTab(WmiConnection *connection, QWidget *parent = 0);
    ~WmiNamespaceTab();

    enum ObjectType {
        Namespace,
        Class
    };

public slots:
    void load();
    void closeTab();

private slots:
    void on_query_clicked();
    void on_filter_textChanged(const QString &text);
    void on_classList_activated(const QModelIndex &index);

signals:
    void namespaceRequested(const QString &wmiNamespace);
    void startLoading();
    void stopLoading();

protected:
    void loadModelItems();
    void changeEvent(QEvent *e);

private:
    QPointer<WmiConnection> m_connection;
    QPointer<QStandardItemModel> m_model;
    QPointer<QSortFilterProxyModel> m_proxy;
    bool m_isClosing;
};

#endif // WMINAMESPACETAB_H
