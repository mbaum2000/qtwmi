#include "wmibrowser.h"
#include "wminamespacetab.h"
#include "wmiconnection.h"
#include "wmiobject.h"

#include <QInputDialog>
#include <QMessageBox>
#include <QProgressBar>
#include <QDebug>

WmiBrowser::WmiBrowser(QWidget *parent)
    : QMainWindow(parent),
      m_busyCount(0),
      m_progressBar(new QProgressBar(this)) {
    setupUi(this);
    statusBar()->addWidget(m_progressBar);
    m_progressBar->setRange(0, 0);
    m_progressBar->hide();
    loadNamespace("ROOT");
}

void WmiBrowser::loadNamespace(const QString &wmiNamespace) {
    for(int i = 0; i < connections->count(); i++) {
        if(connections->tabText(i) == wmiNamespace) {
            connections->setCurrentIndex(i);
            return;
        }
    }
    WmiConnection *connection = new WmiConnection(wmiNamespace, this);
    if(connection->isValid()) {
        WmiNamespaceTab *nsBrowser = new WmiNamespaceTab(connection, this);
        connection->setParent(nsBrowser);
        connect(nsBrowser, SIGNAL(namespaceRequested(QString)), this, SLOT(loadNamespace(QString)));
        connections->addTab(nsBrowser, wmiNamespace);
        connections->setCurrentWidget(nsBrowser);
        nsBrowser->load();
    } else {
        delete connection;
        QMessageBox::warning(this, tr("Error..."), tr("Unable to connect to namespace \"%1\"").arg(wmiNamespace));
    }
}

void WmiBrowser::startLoading() {
    m_busyCount++;
    m_progressBar->show();
}

void WmiBrowser::stopLoading() {
    m_busyCount--;
    if(m_busyCount == 0) {
        m_progressBar->hide();
    }
}

void WmiBrowser::on_actionConnect_triggered() {
    QString wmiNamespace = QInputDialog::getText(this, tr("Connect to WMI"), tr("WMI Namespace:"), QLineEdit::Normal, QString("ROOT"));
    loadNamespace(wmiNamespace);
}

void WmiBrowser::on_connections_tabCloseRequested(int index) {
    WmiNamespaceTab *widget = qobject_cast<WmiNamespaceTab*>(connections->widget(index));
    if(widget) {
        connections->removeTab(index);
        widget->closeTab();
    }
}

void WmiBrowser::changeEvent(QEvent *e) {
    QMainWindow::changeEvent(e);
    switch(e->type()) {
    case QEvent::LanguageChange:
        retranslateUi(this);
        break;
    default:
        break;
    }
}
