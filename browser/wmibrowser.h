#ifndef WMIBROWSER_H
#define WMIBROWSER_H

#include "ui_wmibrowser.h"

class QProgressBar;

class WmiBrowser : public QMainWindow, private Ui::WmiBrowser {
    Q_OBJECT
public:
    explicit WmiBrowser(QWidget *parent = 0);

public slots:
    void loadNamespace(const QString &wmiNamespace);
    void startLoading();
    void stopLoading();

private slots:
    void on_actionConnect_triggered();
    void on_connections_tabCloseRequested(int index);

protected:
    void changeEvent(QEvent *e);

private:
    int m_busyCount;
    QProgressBar *m_progressBar;
};

#endif // WMIBROWSER_H
