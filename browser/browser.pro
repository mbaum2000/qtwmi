include($${top_srcdir}/src/libqtwmi.pri)

QT += core gui

CONFIG(debug, debug|release) {
    CONFIG += console
}

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = browser
TEMPLATE = app

SOURCES += \
    main.cpp \
    wmibrowser.cpp \
    wmiquerywindow.cpp \
    wminamespacetab.cpp

HEADERS += \
    wmibrowser.h \
    wmiquerywindow.h \
    wminamespacetab.h

FORMS += \
    wmibrowser.ui \
    wmiquerywindow.ui \
    wminamespacetab.ui

RESOURCES += \
    ../icons/icons.qrc

QMAKE_CXXFLAGS_RELEASE -= -Zc:strictStrings
QMAKE_CFLAGS_RELEASE -= -Zc:strictStrings
QMAKE_CFLAGS -= -Zc:strictStrings
QMAKE_CXXFLAGS -= -Zc:strictStrings
