#ifndef WMIQUERYWINDOW_H
#define WMIQUERYWINDOW_H

#include "ui_wmiquerywindow.h"

class WmiConnection;
class WmiObject;
class QStandardItemModel;

class WmiQueryWindow : public QDialog, private Ui::WmiQueryWindow {
    Q_OBJECT
public:
    explicit WmiQueryWindow(WmiConnection *connection, QWidget *browser = 0);

public slots:
    void loadClass(const QString &className);
    void loadQuery(const QString &className = QString());

private slots:
    void on_runQuery_clicked();
    void addObjects(const QList<WmiObject> &objects);

signals:
    void startLoading(bool dummy = true);
    void stopLoading(bool dummy = true);

protected:
    void changeEvent(QEvent *e);

private:
    WmiConnection *m_connection;
    QStandardItemModel *m_model;
};

#endif // WMIQUERYWINDOW_H
