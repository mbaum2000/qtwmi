#include "wmibrowser.h"
#include "wmiconnection.h"
#include "comconnection.h"

#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    WmiBrowser w;
    w.show();

    return a.exec();
}
