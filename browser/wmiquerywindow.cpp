#include "wmiquerywindow.h"
#include "wmiconnection.h"
#include "wmiobject.h"
#include "wmiobjectrequest.h"

#include <QStandardItemModel>
#include <QMessageBox>
#include <QDebug>

WmiQueryWindow::WmiQueryWindow(WmiConnection *connection, QWidget *parent)
    : QDialog(parent),
      m_connection(connection) {
    setupUi(this);

    connect(this, SIGNAL(startLoading(bool)), runQuery, SLOT(setDisabled(bool)));
    connect(this, SIGNAL(startLoading()), parent, SIGNAL(startLoading()));

    connect(this, SIGNAL(stopLoading()), parent, SIGNAL(stopLoading()));
    connect(this, SIGNAL(stopLoading(bool)), runQuery, SLOT(setEnabled(bool)));

    m_model = new QStandardItemModel(this);
    results->setModel(m_model);
}

void WmiQueryWindow::loadClass(const QString &className) {
    queryRegion->hide();
    WmiObject object = m_connection->getObject(className);
    if(object.isValid()) {
        addObjects(QList<WmiObject>({object}));
        setWindowTitle(QString("Class: %1").arg(className));
        setWindowIcon(QIcon(QString(":/icons/%1class.svg")
                            .arg(className.startsWith("__") ? "system-" : "")));
        show();
    } else {
        QMessageBox::warning(this, tr("Error..."), tr("Unable to get class \"%1\"").arg(className));
    }
}

void WmiQueryWindow::loadQuery(const QString &className) {
    query->setPlainText(QString("SELECT * FROM %1").arg(className));
    setWindowIcon(QIcon(":/icons/query.svg"));
    show();
}

void WmiQueryWindow::on_runQuery_clicked() {
    if(!m_connection) {
        return;
    }
    QString queryString = query->toPlainText();
    setWindowTitle(tr("Query: %1").arg(queryString));

    Q_EMIT startLoading();

    WmiObjectRequest *result = m_connection->execQuery(queryString);
    result->waitForFinished();
    addObjects(result->objects());

    Q_EMIT stopLoading();
}

void WmiQueryWindow::addObjects(const QList<WmiObject> &objects) {
    m_model->clear();
    m_model->setHorizontalHeaderLabels(QStringList({tr("Name"), tr("Type"), tr("Value")}));
    for(WmiObject obj : objects) {
        QStandardItem *objItem = new QStandardItem(QIcon(QString(":/icons/%1%2.svg")
                                                         .arg(obj.className().startsWith("__") ? "system-" : "")
                                                         .arg(obj.isInstance() ? "instance" : "class")),
                                                   obj.objectPath());
        objItem->setEditable(false);
        for(const QString &property : obj.allProperties()) {
            QVariant value = obj.property(property);
            QStandardItem *name = new QStandardItem(QIcon(QString(":/icons/%1property.svg")
                                                          .arg(property.startsWith("__") ? "system-" : "")),
                                                    property);
            name->setEditable(false);
            QStandardItem *type = new QStandardItem(value.typeName());
            type->setEditable(false);
            QStandardItem *repr = new QStandardItem(value.toString());
            repr->setEditable(false);
            QList<QStandardItem*> items({name, type, repr});
            objItem->appendRow(items);
        }
        for(const QString &method : obj.methods()) {
            QStandardItem *name = new QStandardItem(QIcon(QString(":/icons/method.svg")),
                                                    method);
            name->setEditable(false);
            QStandardItem *type = new QStandardItem("Method");
            type->setEditable(false);
            QStandardItem *repr = new QStandardItem("");
            repr->setEditable(false);
            QList<QStandardItem*> items({name, type, repr});
            objItem->appendRow(items);
        }
        m_model->appendRow(objItem);
        results->resizeColumnToContents(0);
        qApp->processEvents();
    }
    if(m_model->rowCount() == 1) {
        results->expandAll();
        results->resizeColumnToContents(0);
    }
}

void WmiQueryWindow::changeEvent(QEvent *e) {
    QDialog::changeEvent(e);
    switch(e->type()) {
    case QEvent::LanguageChange:
        retranslateUi(this);
        break;
    default:
        break;
    }
}
