TEMPLATE = subdirs

SUBDIRS += \
    src \
    browser \
    util \
    tests

browser.depends += src
tests.depends += src

OTHER_FILES += \
    .qmake.conf \
    msbuild.prf

