# QtWMI

QtWMI is a wrapper around the Windows WMI Interface (Windows Management Instrumentation), written in C++ with Qt.

At this time, it is very much a work-in-progress, and the API can (and likely will) change at any time without notice.

## Compiling

```
#!batch
git clone git@bitbucket.org:mbaum2000/qtwmi.git

mkdir build-qtwmi
cd build-qtwmi
qmake ..\qtwmi
make
rem ...or nmake, mingw32-make, jom
```

## Releases

QtWMI is currently in alpha.  In the future, releases will be available to download here:
https://bitbucket.org/mbaum2000/qtwmi/downloads

## Issues

Bug reports and feature requests can be made on the Issue Tracker here:
https://bitbucket.org/mbaum2000/qtwmi/issues

## License

QtWMI is Licensed under the GNU Lesser General Public License, version 3:
https://www.gnu.org/licenses/lgpl-3.0.en.html

