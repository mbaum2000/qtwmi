TEMPLATE = app

TARGET = WmiTestInterface

CONFIG += msbuild

MSBUILD_PROJECTS = WmiProvider.csproj
MSBUILD_SOURCES = WmiTestInterface.cs Properties/AssemblyInfo.cs

equals(TEMPLATE, app) {
    DEFINES += DECOUPLED
}

OTHER_FILES = test.key
