﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Management.Instrumentation;
using System.Configuration.Install;
using System.Reflection;

#if DECOUPLED
[assembly: WmiConfiguration(@"ROOT\QtWmiTest", HostingModel = ManagementHostingModel.Decoupled)]
#else
[assembly: WmiConfiguration(@"ROOT\QtWmiTest", HostingModel = ManagementHostingModel.NetworkService)]
#endif

// @TODO: Add Inheritance
// @TODO: Add EmbeddedObjects
// @TODO: Why do Decoupled Providers crash when calling methods?
// @TODO: Find a way to not require Administrator privileges to install
namespace WmiTestInterface {
    [System.ComponentModel.RunInstaller(true)]
    public class MyInstall : DefaultManagementInstaller { }

    [ManagementEntity(Name = "Test_WmiTestInterfaceSetup")]
    public class WmiTestInterfaceSetup {
        static WmiTestInterfaceSetup instance;
        public Dictionary<int, WmiTestInterface> testInstances;

        public WmiTestInterfaceSetup() {
            testInstances = new Dictionary<int, WmiTestInterface>();
        }

        public static WmiTestInterfaceSetup Instance {
            get {
                if (instance == null) {
                    instance = new WmiTestInterfaceSetup();
                }
                return instance;
            }
        }

        [ManagementKey]
        public int Id {
            get { return 0; }
        }

        [ManagementEnumerator]
        static public IEnumerable EnumerateInstances() {
            yield return Instance;
        }

        [ManagementBind]
        static public WmiTestInterfaceSetup GetInstance([ManagementName("Id")] int id) {
            if(id != 0) {
                return null;
            }
            return Instance;
        }

        [ManagementTask]
        public bool AddClass(int Id, string StringProperty, int NumberProperty, bool BooleanProperty) {
            if(testInstances.Keys.Contains(Id)) {
                return false;
            }
            var iface = new WmiTestInterface(Id, StringProperty, NumberProperty, BooleanProperty);
            testInstances.Add(Id, iface);
            return true;
        }

        [ManagementTask]
        public bool Reset() {
            testInstances.Clear();
            return true;
        }
    }

    [ManagementEntity(Name = "Test_WmiTestInterface")]
    [ManagementQualifier("Description", Value = "A WMI Test Interface")]
    public class WmiTestInterface {
        [ManagementKey]
        [ManagementQualifier("Description", Value = "The Unique ID")]
        public readonly int Id;

        [ManagementConfiguration]
        [ManagementQualifier("Description", Value = "A String Property")]
        public string StringProperty;

        [ManagementConfiguration]
        [ManagementQualifier("Description", Value = "A Numeric Property")]
        public int NumberProperty;

        [ManagementConfiguration]
        [ManagementQualifier("Description", Value = "A Boolean Property")]
        public bool BooleanProperty;

        public Dictionary<string, WmiChildObject> children;

        public WmiTestInterface(int id = -1, string strKey = "", int numKey = 0, bool boolKey = false) {
            Id = id;
            StringProperty = strKey;
            NumberProperty = numKey;
            BooleanProperty = boolKey;
            children = new Dictionary<string, WmiChildObject>();
        }

        [ManagementEnumerator]
        static public IEnumerable EnumerateInstances() {
            var instances = WmiTestInterfaceSetup.Instance.testInstances;
            foreach(WmiTestInterface instance in instances.Values) {
                yield return instance;
            }
        }

        [ManagementBind]
        static public WmiTestInterface GetInstance([ManagementName("Id")] int id) {
            var instances = WmiTestInterfaceSetup.Instance.testInstances;
            if(instances.ContainsKey(id)) {
                return instances[id];
            }
            return null;
        }

        [ManagementCreate]
        static public WmiTestInterface CreateInstance([ManagementName("Id")] int id,
                                                      [ManagementName("StringProperty")] string stringProp,
                                                      [ManagementName("NumberProperty")] int numProp,
                                                      [ManagementName("BooleanProperty")] bool boolProp) {
            var instances = WmiTestInterfaceSetup.Instance.testInstances;
            if(instances.ContainsKey(id)) {
                return null;
            }
            var iface = new WmiTestInterface(id, stringProp, numProp, boolProp);
            WmiTestInterfaceSetup.Instance.testInstances.Add(id, iface);
            return iface;
        }

        [ManagementTask]
        [ManagementQualifier("Description", Value = "Takes the value of inString and returns it in outString")]
        static public bool EchoString([ManagementQualifier("Description", Value = "The Input String")]
                                      string inString,
                                      [ManagementQualifier("Description", Value = "The Output String")]
                                      out string outString) {
            outString = inString;
            return true;
        }

        [ManagementTask]
        [ManagementQualifier("Description", Value = "Returns the values of the String, Number, and Boolean properties as output parameters")]
        public bool GetProperties(out string StringProperty, out int NumberProperty, out bool BooleanProperty) {
            StringProperty = this.StringProperty;
            NumberProperty = this.NumberProperty;
            BooleanProperty = this.BooleanProperty;
            return true;
        }

        [ManagementTask]
        public bool AddChild(string Name) {
            if(!children.ContainsKey(Name)) {
                children[Name] = new WmiChildObject(this, Name);
                return true;
            }
            return false;
        }

        //[ManagementTask]
        public bool GetChild(string Name, /*[ManagementQualifier("EmbeddedInstance", Value="WmiChildObject")] string*/ ref WmiChildObject Child) {
        if(children.ContainsKey(Name)) {
                Child = children[Name];
                return true;
            }
            return false;
        }

        //[ManagementTask]
        public bool GetChildren(/*[ManagementQualifier("EmbeddedInstance", Value="WmiChildObject[]")] string*/ ref WmiChildObject[] Children) {
            Children = children.Values.ToArray();
            return true;
        }
    }

    [ManagementEntity(Name = "Test_WmiChildObject")]
    public class WmiChildObject {
        [ManagementKey]
        public string Name;

        //[ManagementProbe]
        //[ManagementQualifier("EmbeddedInstance", Value = "Test_WmiTestInterface")]
        //[ManagementReference(Type = "Test_WmiTestInterface")]
        public readonly WmiTestInterface Parent;

        public WmiChildObject(WmiTestInterface parent, string name) {
            Parent = parent;
            Name = name;
        }

        [ManagementBind]
        static public WmiChildObject GetInstance(/*[ManagementName("Parent")] WmiTestInterface parent,*/
                                                 [ManagementName("Name")] string name) {
            //var children = parent.children;
            //if(children.ContainsKey(name)) {
            //    return children[name];
            //}
            return null;
        }

        [ManagementEnumerator]
        static public IEnumerable EnumerateInstances() {
            yield break;
        }
    }

#if DECOUPLED
    class MainClass {
        public static void Main(string[] args) {
            ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
            InstrumentationManager.RegisterType(typeof(WmiTestInterfaceSetup));
            InstrumentationManager.RegisterType(typeof(WmiTestInterface));
            InstrumentationManager.RegisterType(typeof(WmiChildObject));
            var instance = WmiTestInterfaceSetup.Instance;
            Console.WriteLine("Press Enter to terminate connection");
            Console.ReadLine();
            InstrumentationManager.UnregisterType(typeof(WmiTestInterfaceSetup));
            InstrumentationManager.UnregisterType(typeof(WmiTestInterface));
            InstrumentationManager.UnregisterType(typeof(WmiChildObject));
            ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location });
        }
    }
#endif
}
