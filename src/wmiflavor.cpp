#include "wmiflavor.h"
#include "wmiflavor_p.h"

#include <QDebug>

#include <wbemidl.h>

static int registerWmiFlavor() {
    int type = qRegisterMetaType<WmiFlavor>();
    QMetaType::registerDebugStreamOperator<WmiFlavor>();
    return type;
}

Q_CONSTRUCTOR_FUNCTION(registerWmiFlavor)

WmiFlavorData::WmiFlavorData(LONG wmiFlavor)
    : m_flavor(wmiFlavor) { }

WmiFlavorData::WmiFlavorData(bool amended, bool toInstance, bool toSubClass, bool disableOverride) {
    m_flavor = (amended ? WBEM_FLAVOR_AMENDED : 0)
               | (toInstance ? WBEM_FLAVOR_FLAG_PROPAGATE_TO_INSTANCE : 0)
               | (toSubClass ? WBEM_FLAVOR_FLAG_PROPAGATE_TO_DERIVED_CLASS : 0)
               | (disableOverride ? WBEM_FLAVOR_NOT_OVERRIDABLE : 0);
}

WmiFlavorData::WmiFlavorData(const WmiFlavorData &other)
    : QSharedData(other),
      m_flavor(other.m_flavor) { }

WmiFlavorData::~WmiFlavorData() { }

WmiFlavor WmiFlavorData::create(LONG wmiFlavor) {
    return WmiFlavor(wmiFlavor);
}

QString WmiFlavorData::toString(const WmiFlavor &flavor) {
    QStringList flavors;
    if(flavor.amended()) {
        flavors << QLatin1String("Amended");
    }
    bool toInstance = flavor.toInstance();
    bool toSubClass = flavor.toSubClass();
    if(!toInstance && !toSubClass) {
        flavors << QLatin1String("Restricted");
    } else {
        if(toInstance) {
            flavors << QLatin1String("ToInstance");
        } else {
            flavors << QLatin1String("NotToInstance");
        }
        if(toSubClass) {
            flavors << QLatin1String("ToSubClass");
        } else {
            flavors << QLatin1String("NotToSubClass");
        }
    }
    if(flavor.disableOverride()) {
        flavors << QLatin1String("DisableOverride");
    }
    return flavors.join(", ");
}


WmiFlavor::WmiFlavor(bool amended, bool toInstance, bool toSubClass, bool disableOverride)
    : d(new WmiFlavorData(amended, toInstance, toSubClass, disableOverride)) { }

WmiFlavor::WmiFlavor(long wmiFlavor)
    : d(new WmiFlavorData(wmiFlavor
                          & (WBEM_FLAVOR_MASK_PROPAGATION
                             | WBEM_FLAVOR_MASK_PERMISSIONS
                             | WBEM_FLAVOR_MASK_AMENDED))) { }

WmiFlavor::WmiFlavor(const WmiFlavor &other)
    : d(other.d) { }

WmiFlavor::~WmiFlavor() { }

const WmiFlavor &WmiFlavor::operator=(const WmiFlavor &other) {
    d = other.d;
    return *this;
}

bool WmiFlavor::operator==(const WmiFlavor &other) const {
    return (d->m_flavor == other.d->m_flavor);
}

bool WmiFlavor::operator!=(const WmiFlavor &other) const {
    return !(*this == other);
}

bool WmiFlavor::amended() const {
    return (d->m_flavor & WBEM_FLAVOR_AMENDED);
}

void WmiFlavor::setAmended(bool amended) {
    if(amended && !this->amended()) {
        d->m_flavor |= WBEM_FLAVOR_AMENDED;
    } else if(!amended && this->amended()) {
        d->m_flavor &= ~WBEM_FLAVOR_AMENDED;
    }
}

bool WmiFlavor::toInstance() const {
    return (d->m_flavor & WBEM_FLAVOR_FLAG_PROPAGATE_TO_INSTANCE);
}

void WmiFlavor::setToInstance(bool toInstance) {
    if(toInstance && !this->toInstance()) {
        d->m_flavor |= WBEM_FLAVOR_FLAG_PROPAGATE_TO_INSTANCE;
    } else if(!toInstance && this->toInstance()) {
        d->m_flavor &= ~WBEM_FLAVOR_FLAG_PROPAGATE_TO_INSTANCE;
    }
}

bool WmiFlavor::toSubClass() const {
    return (d->m_flavor & WBEM_FLAVOR_FLAG_PROPAGATE_TO_DERIVED_CLASS);
}

void WmiFlavor::setToSubClass(bool toSubClass) {
    if(toSubClass && !this->toSubClass()) {
        d->m_flavor |= WBEM_FLAVOR_FLAG_PROPAGATE_TO_DERIVED_CLASS;
    } else if(!toSubClass && this->toSubClass()) {
        d->m_flavor &= ~WBEM_FLAVOR_FLAG_PROPAGATE_TO_DERIVED_CLASS;
    }
}

bool WmiFlavor::disableOverride() const {
    return (d->m_flavor & WBEM_FLAVOR_NOT_OVERRIDABLE);
}

void WmiFlavor::setDisableOverride(bool disableOverride) {
    if(disableOverride && !this->disableOverride()) {
        d->m_flavor |= WBEM_FLAVOR_NOT_OVERRIDABLE;
    } else if(!disableOverride && this->disableOverride()) {
        d->m_flavor &= ~WBEM_FLAVOR_NOT_OVERRIDABLE;
    }
}
QDebug operator<<(QDebug dbg, const WmiFlavor &flavor) {
    QDebugStateSaver saver(dbg);
    dbg.nospace() << "WmiFlavor(";
    dbg.noquote() << WmiFlavorData::toString(flavor);
    dbg.nospace() << ')';
    return dbg.space();
}
