#ifndef WMIELEMENT_P_H
#define WMIELEMENT_P_H

#include "wmierror.h"
#include "wmielement.h"
#include "wmiqualifier.h"
#include "wmiflavor.h"
#include "wmiobject.h"

class WmiElementPrivate {
public:
    WmiElementPrivate(const WmiObject &object = WmiObject(), const QString &name = QString());
    WmiElementPrivate(const WmiElementPrivate &other);
    ~WmiElementPrivate();

    WmiObject m_object;
    QString m_name;
    QMap<QString, WmiQualifier> m_qualifiers;
    WmiElement::CimType m_cimType;
    long m_flavor;
    mutable WmiError::Error m_lastError;
};

#endif // WMIELEMENT_P_H
