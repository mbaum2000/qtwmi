#ifndef WMIOBJECTSINK_H
#define WMIOBJECTSINK_H

#include "wmiconnection.h"
#include "wmiobject.h"
#include "wmierror.h"

#include <QList>
#include <QPointer>

#include <wbemidl.h>

class WmiRequest;

class WmiObjectSink : public IWbemObjectSink {
public:
    WmiObjectSink(QObject *handler, const WmiConnection *connection);
    virtual ~WmiObjectSink();

    virtual HRESULT STDMETHODCALLTYPE QueryInterface(const IID &riid, void **ppvObject);
    virtual ULONG STDMETHODCALLTYPE AddRef();
    virtual ULONG STDMETHODCALLTYPE Release();
    virtual HRESULT STDMETHODCALLTYPE Indicate(LONG lObjectCount, IWbemClassObject **apObjArray);
    virtual HRESULT STDMETHODCALLTYPE SetStatus(LONG lFlags, HRESULT hResult, BSTR strParam, IWbemClassObject *pObjParam);

    bool waitForFinished(int msecs = 30000) const;
    bool cancel();

    void setHandler(QObject *handler);
    QObject *handler() const;

    const WmiConnection *connection() const;

    QList<WmiObject> objects() const;
    bool isFinished() const;

private:
    QPointer<QObject> m_handler;
    QPointer<const WmiConnection> m_connection;
    QList<WmiObject> m_objects;
    LONG m_ref;
    bool m_finished;

public: // The need to hack this as public is horrible.  Either fix this, or ditch lastError entirely in favor of Exceptions.
    mutable WmiError::Error m_lastError;
};

#endif // WMIOBJECTSINK_H
