#ifndef WMIFLAVOR_P_H
#define WMIFLAVOR_P_H

#include "wmiflavor.h"

#include <QSharedData>

#include <wbemidl.h>

class WmiFlavorData : public QSharedData {
public:
    WmiFlavorData(LONG wmiFlavor);
    WmiFlavorData(bool amended, bool toInstance, bool toSubClass, bool disableOverride);
    WmiFlavorData(const WmiFlavorData &other);
    ~WmiFlavorData();

    static WmiFlavor create(LONG wmiFlavor);
    static QString toString(const WmiFlavor &flavor);

    LONG m_flavor;
};

#endif // WMIFLAVOR_P_H
