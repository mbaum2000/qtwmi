#include "wmiconnection.h"
#include "wmiconnection_p.h"
#include "wmiobject.h"
#include "wmiobject_p.h"
#include "comtools.h"
#include "wmiobjectrequest.h"
#include "wmiobjectrequest_p.h"

#include <qaxtypes.h>
#include <QEventLoop>
#include <QDebug>

#define _WIN32_DCOM
#include <windows.h>
#include <wbemidl.h>

WmiConnectionPrivate::WmiConnectionPrivate(WmiConnection *q, const QString &wmiNamespace)
    : q_ptr(q),
      m_namespace(wmiNamespace),
      m_locator(0),
      m_service(0),
      m_lastError(WmiError::NoError) {

    m_isValid = initialize();
}

WmiConnectionPrivate::~WmiConnectionPrivate() { }

IWbemServices *WmiConnectionPrivate::serviceForConnection(const WmiConnection *connection) {
    if(!connection) {
        return Q_NULLPTR;
    }
    return connection->d_ptr->m_service.data();
}

bool WmiConnectionPrivate::initialize() {
    m_lastError = WmiError::NoError;
    HRESULT hr;

    IWbemLocator *locator = NULL;
    IWbemServices *service = NULL;

    hr = CoCreateInstance(CLSID_WbemLocator, 0, CLSCTX_INPROC_SERVER, IID_IWbemLocator, (LPVOID *) &locator);
    if(FAILED(hr)) {
        qWarning() << "Failed to create IWbemLocator object:" << hrPrintable(hr);
        m_lastError = WmiError::errorFromHresult(hr);
        if(locator) {
            locator->Release();
        }
        return false;
    }

    hr = locator->ConnectServer(
            BString(m_namespace),
            NULL,
            NULL,
            0,
            WBEM_FLAG_CONNECT_USE_MAX_WAIT,
            0,
            0,
            &service);
    if(FAILED(hr)) {
        m_lastError = WmiError::errorFromHresult(hr);
        if(service) {
            service->Release();
        }
        if(locator) {
            locator->Release();
        }
        return false;
    }

    hr = CoSetProxyBlanket(service,
       RPC_C_AUTHN_WINNT,
       RPC_C_AUTHZ_NONE,
       NULL,
       RPC_C_AUTHN_LEVEL_CALL,
       RPC_C_IMP_LEVEL_IMPERSONATE,
       NULL,
       EOAC_NONE
    );
    if(FAILED(hr)) {
        qWarning() << "Could not set proxy blanket:" << hrPrintable(hr);
        m_lastError = WmiError::errorFromHresult(hr);
        if(service) {
            service->Release();
        }
        if(locator) {
            locator->Release();
        }
        return false;
    }

    m_service.reset(service);
    m_locator.reset(locator);
    return true;
}


WmiConnection::WmiConnection(const QString &wmiNamespace, QObject *parent)
    : QObject(parent),
      d_ptr(new WmiConnectionPrivate(this, wmiNamespace)) { }

WmiConnection::~WmiConnection() { }

QString WmiConnection::wmiNamespace() const {
    Q_D(const WmiConnection);
    return d->m_namespace;
}

WmiObjectRequest* WmiConnection::execQuery(const QString &query) const {
    Q_D(const WmiConnection);
    d->m_lastError = WmiError::NoError;
    if(!isValid()) {
        d->m_lastError = WmiError::NotConnected;
        return 0;
    }

    WmiObjectRequest *request = WmiObjectRequestPrivate::create(this);
    WmiObjectSink *sink = WmiRequestPrivate::objectSinkFromRequest(request);
    HRESULT hr = d->m_service->ExecQueryAsync(
        BSTR(L"WQL"),
        BString(query),
        WBEM_FLAG_SEND_STATUS,
        NULL,
        sink);
    if(FAILED(hr)) {
        request->deleteLater();
        d->m_lastError = WmiError::errorFromHresult(hr);
        return 0;
    }

    return request;
}

WmiObject WmiConnection::getObject(const QString &className) const {
    Q_D(const WmiConnection);
    if(!isValid()) {
        d->m_lastError = WmiError::NotConnected;
        return WmiObject();
    }
    WmiObject object = WmiObjectData::create(className, NULL, this);
    d->m_lastError = object.error().error();
    return object;
}

WmiError WmiConnection::error() const {
    Q_D(const WmiConnection);
    return WmiError(d->m_lastError);
}

bool WmiConnection::isValid() const {
    Q_D(const WmiConnection);
    return d->m_isValid;
}

QDebug operator<<(QDebug dbg, const WmiConnection *obj) {
    QDebugStateSaver saver(dbg);
    if(obj) {
        dbg.nospace() << obj->metaObject()->className() << '(';
        dbg.nospace() << (void *)obj;
        dbg.nospace() << ", namespace = " << obj->wmiNamespace();
        if(!obj->objectName().isEmpty()) {
            dbg.nospace() << ", name = " << obj->objectName();
        }
        dbg.nospace() << ')';
    } else {
        dbg << qobject_cast<const QObject*>(obj);
    }
    return dbg.space();
}
