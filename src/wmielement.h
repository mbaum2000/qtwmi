#ifndef WMIELEMENT_H
#define WMIELEMENT_H

#include "exports.h"

#include <QVariant>
#include <QSharedPointer>

class WmiElementPrivate;
class WmiObject;
class WmiQualifier;
class WmiFlavor;
class WmiError;

class QTWMI_EXPORT WmiElement {
    Q_GADGET
    Q_DECLARE_PRIVATE(WmiElement)
public:
    WmiElement();
    WmiElement(const WmiElement &other);
    virtual ~WmiElement();

    enum CimType {
        Illegal = 0xfff,
        Empty = 0x0,
        SInt8 = 0x10,
        UInt8 = 0x11,
        SInt16 = 0x02,
        UInt16 = 0x12,
        SInt32 = 0x03,
        UInt32 = 0x13,
        SInt64 = 0x14,
        UInt64 = 0x15,
        Real32 = 0x04,
        Real64 = 0x05,
        Boolean = 0x0b,
        String = 0x08,
        DateTime = 0x65,
        Reference = 0x66,
        Char16 = 0x67,
        Object = 0x0d,
        SInt8Array = 0x2010,
        UInt8Array = 0x2011,
        SInt16Array = 0x2002,
        UInt16Array = 0x2012,
        SInt32Array = 0x2003,
        UInt32Array = 0x2013,
        SInt64Array = 0x2014,
        UInt64Array = 0x2015,
        Real32Array = 0x2004,
        Real64Array = 0x2005,
        BooleanArray = 0x200b,
        StringArray = 0x2008,
        DateTimeArray = 0x2065,
        ReferenceArray = 0x2066,
        Char16Array = 0x2067,
        ObjectArray = 0x200d
    };
    Q_ENUM(CimType)

    virtual const WmiElement &operator=(const WmiElement &other);
    virtual bool operator==(const WmiElement &other) const;
    virtual bool operator!=(const WmiElement &other) const;

    virtual bool isValid() const;
    virtual bool isNull() const;

    virtual WmiObject object() const;

    virtual QString name() const;
    virtual CimType cimType() const;
    virtual QString cimTypeName() const;
    virtual QString origin() const = 0;
    virtual QList<WmiQualifier> qualifiers() const;
    virtual WmiQualifier qualifier(const QString &name) const;

    virtual WmiError error() const;

protected:
    WmiElement(WmiElementPrivate *d);
    QSharedPointer<WmiElementPrivate> d_ptr;

private:
    friend class WmiElementPrivate;
};

#endif // WMIELEMENT_H
