#ifndef WMIQUALIFIER_P_H
#define WMIQUALIFIER_P_H

#include "wmiqualifier.h"
#include "wmiflavor.h"

#include <QSharedData>

class IWbemQualifierSet;

class WmiQualifierData : public QSharedData {
public:
    WmiQualifierData(const QString &name, const QVariant &value = QVariant(), const WmiFlavor &flavor = WmiFlavor());
    WmiQualifierData(const WmiQualifierData &other);
    ~WmiQualifierData();

    static QMap<QString, WmiQualifier> qualifierSetToMap(IWbemQualifierSet *qualifiers);

    QString m_name;
    QVariant m_value;
    WmiFlavor m_flavor;
};

#endif // WMIQUALIFIER_P_H
