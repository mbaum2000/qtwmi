#ifndef WMIQUALIFIER_H
#define WMIQUALIFIER_H

#include "exports.h"
#include "wmiflavor.h"

#include <QString>
#include <QVariant>
#include <QSharedDataPointer>

class WmiQualifierData;

class QTWMI_EXPORT WmiQualifier {
public:
    WmiQualifier(const QString &name = QString(), const QVariant &value = QVariant(), const WmiFlavor &flavor = WmiFlavor());
    WmiQualifier(const WmiQualifier &other);
    ~WmiQualifier();

    const WmiQualifier &operator=(const WmiQualifier &other);
    bool operator==(const WmiQualifier &other) const;
    bool operator!=(const WmiQualifier &other) const;

    QString name() const;
    void setName(const QString &name);

    QVariant value() const;
    void setValue(const QVariant &value);

    WmiFlavor flavor() const;
    void setFlavor(const WmiFlavor &flavor);

//    bool isSystem() const; // TODO: Implement these from ORIGIN on flavor?
//    bool isPropogated() const;

    inline operator QVariant() const { return QVariant::fromValue<WmiQualifier>(*this); }

private:
    QSharedDataPointer<WmiQualifierData> d;
};

QDebug QTWMI_EXPORT operator<<(QDebug dbg, const WmiQualifier &qualifier);

Q_DECLARE_METATYPE(WmiQualifier)

#endif // WMIQUALIFIER_H
