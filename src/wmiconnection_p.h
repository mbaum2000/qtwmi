#ifndef WMICONNECTION_P_H
#define WMICONNECTION_P_H

#include "wmiobject.h"
#include "wmiobject_p.h"
#include "wmierror.h"
#include "comtools.h"

#include <QString>
#include <QVariant>
#include <QList>
#include <QScopedPointer>
#include <QDebug>

#include <windows.h>

struct IWbemLocator;
struct IWbemServices;
struct IWbemClassObject;

class WmiConnectionPrivate {
    Q_DECLARE_PUBLIC(WmiConnection)
public:
    explicit WmiConnectionPrivate(WmiConnection *q, const QString &wmiNamespace);
    ~WmiConnectionPrivate();

    static IWbemServices *serviceForConnection(const WmiConnection *connection);

    bool initialize();

    WmiConnection *q_ptr;
    QString m_namespace;
    QScopedPointer<IWbemLocator, ScopedPointerComReleaser> m_locator;
    QScopedPointer<IWbemServices, ScopedPointerComReleaser> m_service;
    bool m_isValid;
    mutable WmiError::Error m_lastError;
};

#endif // WMICONNECTION_P_H
