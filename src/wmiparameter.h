#ifndef WMIPARAMETER_H
#define WMIPARAMETER_H

#include "exports.h"
#include "wmielement.h"

#include <QVariant>

class WmiParameterPrivate;
class WmiMethod;

class QTWMI_EXPORT WmiParameter : public WmiElement {
    Q_GADGET
    Q_DECLARE_PRIVATE(WmiParameter)
public:
    WmiParameter();
    WmiParameter(const WmiParameter &other);
    ~WmiParameter();

    bool operator==(const WmiParameter &other) const;

    bool isValid() const;

    WmiObject object() const;
    WmiMethod method() const;
    QString origin() const;

    bool isInput() const;
    bool isOutput() const;

protected:
    WmiParameter(WmiParameterPrivate *d);

private:
    friend class WmiParameterPrivate;
};

QDebug QTWMI_EXPORT operator<<(QDebug dbg, const WmiParameter &param);

#endif // WMIPARAMETER_H
