#ifndef WMIOBJECT_H
#define WMIOBJECT_H

#include "exports.h"
#include "wmiqualifier.h"

#include <QString>
#include <QStringList>
#include <QVariantMap>
#include <QExplicitlySharedDataPointer>

class WmiObjectData;
class WmiConnection;
class WmiError;
class WmiMethod;
class WmiProperty;
struct IWbemClassObject;

class QTWMI_EXPORT WmiObject {
public:
    WmiObject();
    WmiObject(const WmiObject &other);
    ~WmiObject();

    const WmiObject &operator=(const WmiObject &other);
    bool operator==(const WmiObject &other) const;
    bool operator!=(const WmiObject &other) const;

    bool isValid() const;
    bool isNull() const;

    WmiError error() const;

    QVariant property(const QString &name) const;
    WmiProperty propertyInfo(const QString &name) const;
    bool setProperty(const QString &name, const QVariant &value);
    QStringList properties() const;
    QStringList allProperties() const;

    QString className() const;
    QString objectPath() const;
    const WmiConnection *connection() const;
    bool isInstance() const;
    QList<WmiQualifier> qualifiers() const;
    WmiQualifier qualifier(const QString &name) const;

    QStringList methods() const;
    WmiMethod method(const QString &name) const;
    QVariant execMethod(const QString &name, const QVariantList &inputParametersList, const QVariantMap &inputParametersMap, QVariantMap &outputParameters);
    QVariant execMethod(const QString &name, const QVariantList &inputParametersList = QVariantList(), const QVariantMap &inputParametersMap = QVariantMap());
    QVariant execMethod(const QString &name, const QVariantList &inputParameters, QVariantMap &outputParameters);
    QVariant execMethod(const QString &name, const QVariantMap &inputParameters, QVariantMap &outputParameters);
    QVariant execMethod(const QString &name, const QVariantMap &inputParameters);

    QString mof(bool noFlavors = false) const;

    WmiObject spawnInstance() const;
    WmiObject classObject() const;

    void refresh();
    bool apply();

private:
    WmiObject(const QString &objectPath, IWbemClassObject *comObject, const WmiConnection *connection);

    friend class WmiObjectData;

    QExplicitlySharedDataPointer<WmiObjectData> d;
};

QDebug QTWMI_EXPORT operator<<(QDebug dbg, const WmiObject &obj);

Q_DECLARE_METATYPE(WmiObject)
Q_DECLARE_METATYPE(QList<WmiObject>)

#endif // WMIOBJECT_H
