#include "wmirequest.h"
#include "wmirequest_p.h"
#include "wmiobjectsink.h"

WmiRequestPrivate::WmiRequestPrivate(WmiRequest *q, const WmiConnection *connection)
    : WmiObjectSink(Q_NULLPTR, connection),
      q_ptr(q) { }

WmiRequestPrivate::~WmiRequestPrivate() { }

WmiObjectSink *WmiRequestPrivate::objectSinkFromRequest(WmiRequest *request) {
    if(!request) {
        return Q_NULLPTR;
    }
    return request->d_ptr.data();
}


WmiRequest::WmiRequest(WmiRequestPrivate *d)
    : QObject(const_cast<WmiConnection*>(d->connection())),
      d_ptr(d) {

    d->setHandler(this);
}

WmiRequest::~WmiRequest() { }

WmiError WmiRequest::error() const {
    Q_D(const WmiRequest);
    return WmiError(d->m_lastError);
}

bool WmiRequest::isFinished() const {
    Q_D(const WmiRequest);
    return d->isFinished();
}

bool WmiRequest::waitForFinished(int msecs) {
    Q_D(WmiRequest);
    return d->waitForFinished(msecs);
}

bool WmiRequest::cancel() {
    Q_D(WmiRequest);
    return d->cancel();
}
