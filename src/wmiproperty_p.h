#ifndef WMIPROPERTY_P_H
#define WMIPROPERTY_P_H

#include "wmiproperty.h"
#include "wmielement_p.h"

class WmiPropertyPrivate : public WmiElementPrivate {
public:
    WmiPropertyPrivate(const WmiObject &object = WmiObject(), const QString &name = QString());
    WmiPropertyPrivate(const WmiPropertyPrivate &other);
    ~WmiPropertyPrivate();

    static WmiProperty create(const WmiObject &object = WmiObject(), const QString &name = QString());

    QString m_origin;
};

#endif // WMIPROPERTY_P_H
