#include "comtools.h"

BString::BString(const QString &string) {
    m_str = SysAllocStringLen(0, string.length());
    string.toWCharArray(m_str);
}

BString::~BString() {
    SysFreeString(m_str);
}

QString BString::bstrToQString(BSTR string) {
    return QString((QChar*)string, ::SysStringLen(string));
}

BString::operator BSTR() {
    return m_str;
}
