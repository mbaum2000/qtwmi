#include "wmielement.h"
#include "wmielement_p.h"
#include "wmiqualifier.h"
#include "wmiqualifier_p.h"
#include "wmiflavor.h"
#include "wmimethod.h"
#include "wmiobject.h"
#include "wmiobject_p.h"

#include <QMetaEnum>
#include <QDebug>

#include <wbemidl.h>

WmiElementPrivate::WmiElementPrivate(const WmiObject &object, const QString &name)
    : m_object(object),
      m_name(name),
      m_cimType(WmiElement::Empty),
      m_flavor(0),
      m_lastError(WmiError::NoError) { }

WmiElementPrivate::WmiElementPrivate(const WmiElementPrivate &other)
    : m_object(other.m_object),
      m_name(other.m_name),
      m_qualifiers(other.m_qualifiers),
      m_cimType(other.m_cimType),
      m_flavor(other.m_flavor),
      m_lastError(other.m_lastError) { }

WmiElementPrivate::~WmiElementPrivate() { }


WmiElement::WmiElement()
    : d_ptr(new WmiElementPrivate) { }

WmiElement::WmiElement(const WmiElement &other)
    : d_ptr(other.d_ptr) { }

WmiElement::WmiElement(WmiElementPrivate *d)
    : d_ptr(d) { }

WmiElement::~WmiElement() { }

const WmiElement &WmiElement::operator=(const WmiElement &other) {
    d_ptr = other.d_ptr;
    return *this;
}

bool WmiElement::operator==(const WmiElement &other) const {
    return (d_func()->m_object == other.d_func()->m_object &&
            d_func()->m_name == other.d_func()->m_name);
}

bool WmiElement::operator!=(const WmiElement &other) const {
    return !(*this == other);
}

bool WmiElement::isValid() const {
    Q_D(const WmiElement);
    return d->m_object.isValid() && !d->m_name.isEmpty();
}

bool WmiElement::isNull() const {
    return !isValid();
}

WmiObject WmiElement::object() const {
    Q_D(const WmiElement);
    return d->m_object;
}

QString WmiElement::name() const {
    Q_D(const WmiElement);
    return d->m_name;
}

WmiElement::CimType WmiElement::cimType() const {
    Q_D(const WmiElement);
    return d->m_cimType;
}

QString WmiElement::cimTypeName() const {
    Q_D(const WmiElement);
    int index = staticMetaObject.indexOfEnumerator("CimType");
    if(d->m_cimType == WmiElement::Empty) {
        return QString();
    } else if(d->m_cimType & CIM_FLAG_ARRAY) {
        return QString("%1[]").arg(staticMetaObject.enumerator(index).valueToKey(d->m_cimType ^ CIM_FLAG_ARRAY));
    } else {
        return staticMetaObject.enumerator(index).valueToKey(d->m_cimType);
    }
}

QList<WmiQualifier> WmiElement::qualifiers() const {
    Q_D(const WmiElement);
    return d->m_qualifiers.values();
}

WmiQualifier WmiElement::qualifier(const QString &name) const {
    Q_D(const WmiElement);
    return d->m_qualifiers.value(name.toLower());
}

WmiError WmiElement::error() const {
    Q_D(const WmiElement);
    return WmiError(d->m_lastError);
}
