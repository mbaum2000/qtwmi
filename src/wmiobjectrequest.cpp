#include "wmiobjectrequest.h"
#include "wmiobjectrequest_p.h"
#include "wmiconnection.h"
#include "wmiconnection_p.h"
#include "comtools.h"

#include <QEventLoop>
#include <QTimer>
#include <QDebug>

WmiObjectRequestPrivate::WmiObjectRequestPrivate(WmiObjectRequest *q, const WmiConnection *connection)
    : WmiRequestPrivate(q, connection) { }

WmiObjectRequestPrivate::~WmiObjectRequestPrivate() { }

WmiObjectRequest *WmiObjectRequestPrivate::create(const WmiConnection *connection) {
    return new WmiObjectRequest(connection);
}


WmiObjectRequest::WmiObjectRequest(const WmiConnection *connection)
    : WmiRequest(new WmiObjectRequestPrivate(this, connection)) {
}

WmiObjectRequest::~WmiObjectRequest() { }

QList<WmiObject> WmiObjectRequest::objects() const {
    Q_D(const WmiObjectRequest);
    return d->objects();
}
