#ifndef WMIMETHODCALL_H
#define WMIMETHODCALL_H

#include "exports.h"
#include "wmirequest.h"

class WmiMethodCallPrivate;
class WmiMethod;
class WmiError;
class WmiConnection;

class QTWMI_EXPORT WmiMethodCall : public WmiRequest {
    Q_OBJECT
    Q_DECLARE_PRIVATE(WmiMethodCall)
public:
    virtual ~WmiMethodCall();

    WmiMethod method() const;

    QVariant returnValue() const;
    QVariantMap inputParameterValues() const;
    QVariantMap outputParameterValues() const;

protected:
    explicit WmiMethodCall(const WmiMethod &method, const QVariantMap &inputParameters, const WmiConnection *connection);
};

#endif // WMIMETHODCALL_H
