#include "wmimethod.h"
#include "wmimethod_p.h"
#include "wmimethodcall.h"
#include "wmimethodcall_p.h"
#include "wmiobject.h"
#include "wmiobject_p.h"
#include "wmiconnection.h"
#include "wmiconnection_p.h"
#include "wmiproperty.h"
#include "wmiparameter_p.h"
#include "wmiqualifier.h"
#include "wmiqualifier_p.h"
#include "comtools.h"

#include <wbemidl.h>

#include <qaxtypes.h>
#include <QDebug>

WmiMethodPrivate::WmiMethodPrivate(const WmiObject &object, const QString &name)
    : WmiElementPrivate(object, name) {

    ScopedComObject<IWbemClassObject> inParams;
    ScopedComObject<IWbemClassObject> outParams;

    HRESULT hr;

    // Check Connection
    if(!object.connection()) {
        m_lastError = WmiError::NotConnected;
        return;
    }

    // Get COM Object for class, not instance
    WmiObject classDef = object.classObject();
    if(classDef.error() != WmiError::NoError){
        m_lastError = classDef.error().error();
        return;
    }

    // Get Method Parameters
    IWbemClassObject *comObject = WmiObjectData::comObjectForObject(classDef);
    if(!comObject) {
        m_lastError = WmiError::InvalidClass;
        return;
    }
    hr = comObject->GetMethod(BString(name), 0, inParams, outParams);
    if(FAILED(hr)) {
        m_lastError = WmiError::errorFromHresult(hr);
        return;
    }
    if(inParams) {
        m_inParams = WmiObjectData::create(QLatin1String("__PARAMETERS"), inParams, object.connection());
    }
    if(outParams) {
        m_outParams = WmiObjectData::create(QLatin1String("__PARAMETERS"), outParams, object.connection());
    }

    // Get Return Type
    m_cimType = m_outParams.propertyInfo(QLatin1String("ReturnValue")).cimType();

    // Get Method Qualifiers
    ScopedComObject<IWbemQualifierSet> qualifiers;
    hr = comObject->GetMethodQualifierSet(BString(m_name), qualifiers);
    if(!FAILED(hr)) {
        m_qualifiers = WmiQualifierData::qualifierSetToMap(qualifiers);
    } else {
        m_lastError = WmiError::errorFromHresult(hr);
    }

    // Get Method Origin
    BSTR methodOrigin;
    hr = comObject->GetMethodOrigin(BString(m_name), &methodOrigin);
    if(!FAILED(hr)) {
        m_origin = BString::bstrToQString(methodOrigin);
        SysFreeString(methodOrigin);
    } else {
        m_lastError = WmiError::errorFromHresult(hr);
    }
}

WmiMethodPrivate::WmiMethodPrivate(const WmiMethodPrivate &other)
    : WmiElementPrivate(other) { }

WmiMethodPrivate::~WmiMethodPrivate() { }

WmiMethod WmiMethodPrivate::create(const WmiObject &object, const QString &name) {
    return WmiMethod(new WmiMethodPrivate(object, name));
}


WmiMethod::WmiMethod()
    : WmiElement(new WmiMethodPrivate) { }

WmiMethod::WmiMethod(const WmiMethod &other)
    : WmiElement(other) { }

WmiMethod::WmiMethod(WmiMethodPrivate *d)
    : WmiElement(d) { }

WmiMethod::~WmiMethod() { }

QString WmiMethod::origin() const {
    Q_D(const WmiMethod);
    return d->m_origin;
}

QStringList WmiMethod::inputParameters() const {
    Q_D(const WmiMethod);
    return d->m_inParams.properties();
}

QStringList WmiMethod::outputParameters() const {
    Q_D(const WmiMethod);
    QStringList params = d->m_outParams.properties();
    params.removeAll(QLatin1String("ReturnValue"));
    return params;
}

WmiParameter WmiMethod::parameterInfo(const QString &parameterName) const {
    Q_D(const WmiMethod);
    WmiObject object;
    bool isInput = false;
    bool isOutput = false;

    if(d->m_outParams.properties().contains(parameterName)) {
        object = d->m_outParams;
        isOutput = true;
    }

    if(d->m_inParams.properties().contains(parameterName)) {
        object = d->m_inParams;
        isInput = true;
    }

    if(object.isValid()) {
        return WmiParameterPrivate::create(object, *this, parameterName, isInput, isOutput);
    }  else {
        return WmiParameter();
    }
}

WmiMethodCall* WmiMethod::exec(const QVariantList &inputParametersList, const QVariantMap &inputParametersMap) {
    Q_D(WmiMethod);    
    if(isNull()) {
        d->m_lastError = WmiError::InvalidMethod;
        return 0;
    }

    const WmiConnection *connection = d->m_object.connection();
    IWbemServices *service = WmiConnectionPrivate::serviceForConnection(connection);
    if(!connection || !connection->isValid() || !service) {
        d->m_lastError = WmiError::NotConnected;
        return 0;
    }

    d->m_lastError = WmiError::NoError;
    HRESULT hr;

    QVariantMap inputParameterValues;
    WmiObject inParams;

    if(d->m_inParams.isValid()) {
        inParams = d->m_inParams.spawnInstance();
        if(inParams.isNull()) {
            d->m_lastError = inParams.error().error();
            return 0;
        }

        QStringList inputParameterNames = inputParameters();
        for(int i = 0; i < inputParametersList.length(); i++) {
            if(i >= inputParameterNames.length()) {
                qWarning() << "Failed to set positional parameter" << i << "for method" << name() << "on object" << object().objectPath() << ": too many parameters";
                d->m_lastError = WmiError::InvalidMethodParameters;
                break;
            }
            inputParameterValues.insert(inputParameterNames.value(i), inputParametersList.value(i));
        }
        for(const QString &key : inputParametersMap.keys()) {
            inputParameterValues.insert(key, inputParametersMap.value(key));
        }

        for(const QString &arg : inputParameterValues.keys()) {
            inParams.setProperty(arg, inputParameterValues.value(arg));
            if(inParams.error() != WmiError::NoError) {
                qWarning() << "Failed to set parameter" << arg << "for method" << name() << "on object" << object().objectPath() << ":" << inParams.error().error();
                d->m_lastError = inParams.error().error();
            }
        }
    }

    WmiMethodCall *request = WmiMethodCallPrivate::create(*this, inputParameterValues, connection);
    WmiObjectSink *sink = WmiRequestPrivate::objectSinkFromRequest(request);
    hr = service->ExecMethodAsync(BString(object().objectPath()),
                                  BString(d->m_name),
                                  0,
                                  NULL,
                                  WmiObjectData::comObjectForObject(inParams),
                                  sink);

    if(FAILED(hr)) {
        request->deleteLater();
        d->m_lastError = WmiError::errorFromHresult(hr);
        return 0;
    }
    return request;
}

WmiMethodCall *WmiMethod::exec(const QVariantMap &inputParameters) {
    return exec(QVariantList(), inputParameters);
}

QVariant WmiMethod::execWait(const QVariantList &inputParametersList, const QVariantMap &inputParametersMap, QVariantMap &outputParameters) {
    WmiMethodCall *call = exec(inputParametersList, inputParametersMap);
    if(!call) {
        return QVariant();
    }
    call->waitForFinished();
    QVariant value = call->returnValue();
    outputParameters = call->outputParameterValues();
    call->deleteLater();
    return value;
}

QVariant WmiMethod::execWait(const QVariantList &inputParametersList, const QVariantMap &inputParametersMap) {
    QVariantMap dummy;
    return execWait(inputParametersList, inputParametersMap, dummy);
}

QVariant WmiMethod::execWait(const QVariantList &inputParameters, QVariantMap &outputParameters) {
    return execWait(inputParameters, QVariantMap(), outputParameters);
}

QVariant WmiMethod::execWait(const QVariantMap &inputParameters, QVariantMap &outputParameters) {
    return execWait(QVariantList(), inputParameters, outputParameters);
}

QVariant WmiMethod::execWait(const QVariantMap &inputParameters) {
    QVariantMap dummy;
    return execWait(QVariantList(), inputParameters, dummy);
}

QDebug operator<<(QDebug dbg, const WmiMethod &method) {
    QDebugStateSaver saver(dbg);
    dbg.nospace() << "WmiMethod(";
    if(method.isValid()) {
        dbg.nospace() << '['
                      << method.object().objectPath()
                      << "]."
                      << method.name();
    }
    dbg.nospace() << ')';
    return dbg.space();
}
