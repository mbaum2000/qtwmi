#include "wmiparameter.h"
#include "wmiparameter_p.h"
#include "wmimethod.h"
#include "wmiobject.h"
#include "wmiobject_p.h"
#include "wmiqualifier_p.h"
#include "comtools.h"

#include <QDebug>

#include <wbemidl.h>

WmiParameterPrivate::WmiParameterPrivate(const WmiObject &object, const WmiMethod &method, const QString &name, bool isInput, bool isOutput)
    : WmiElementPrivate(object, name),
      m_method(method),
      m_isInput(isInput),
      m_isOutput(isOutput) {

    // Check COM Object
    HRESULT hr;
    IWbemClassObject *comObject = WmiObjectData::comObjectForObject(m_object);
    if(!comObject) {
        m_lastError = WmiError::NotConnected;
        return;
    }

    // Get Parameter Info
    VARIANT v;
    CIMTYPE t;
    LONG f;
    hr = comObject->Get(BString(m_name), 0, &v, &t, &f);
    if(!FAILED(hr)) {
        m_cimType = static_cast<WmiElement::CimType>(t);
        m_flavor = f;
    } else {
        m_lastError = WmiError::errorFromHresult(hr);
    }

    // Get Parameter Qualifiers
    ScopedComObject<IWbemQualifierSet> qualifiers;
    hr = comObject->GetPropertyQualifierSet(BString(m_name), qualifiers);
    if(!FAILED(hr)) {
        m_qualifiers = WmiQualifierData::qualifierSetToMap(qualifiers);
    } else {
        m_lastError = WmiError::errorFromHresult(hr);
    }
}

WmiParameterPrivate::WmiParameterPrivate(const WmiParameterPrivate &other)
    : WmiElementPrivate(other),
      m_method(other.m_method),
      m_isInput(other.m_isInput),
      m_isOutput(other.m_isOutput) { }

WmiParameterPrivate::~WmiParameterPrivate() { }

WmiParameter WmiParameterPrivate::create(const WmiObject &object, const WmiMethod &method, const QString &name, bool isInput, bool isOutput) {
    return WmiParameter(new WmiParameterPrivate(object, method, name, isInput, isOutput));
}


WmiParameter::WmiParameter()
    : WmiElement(new WmiParameterPrivate) { }

WmiParameter::WmiParameter(const WmiParameter &other)
    : WmiElement(other) { }

WmiParameter::WmiParameter(WmiParameterPrivate *d)
    : WmiElement(d) { }

WmiParameter::~WmiParameter() { }

bool WmiParameter::operator==(const WmiParameter &other) const {
    return (d_func()->m_method == other.d_func()->m_method &&
            d_func()->m_name == other.d_func()->m_name);
}

bool WmiParameter::isValid() const {
    Q_D(const WmiParameter);
    return d->m_method.isValid() && !d->m_name.isEmpty();
}

WmiObject WmiParameter::object() const {
    Q_D(const WmiParameter);
    return d->m_method.object();
}

WmiMethod WmiParameter::method() const {
    Q_D(const WmiParameter);
    return d->m_method;
}

QString WmiParameter::origin() const {
    Q_D(const WmiParameter);
    return d->m_method.origin();
}

bool WmiParameter::isInput() const {
    Q_D(const WmiParameter);
    return d->m_isInput;
}

bool WmiParameter::isOutput() const {
    Q_D(const WmiParameter);
    return d->m_isOutput;
}

QDebug operator<<(QDebug dbg, const WmiParameter &param) {
    QDebugStateSaver saver(dbg);
    dbg.nospace() << "WmiParameter(";
    if(param.isValid()) {
        dbg.noquote() << '['
                      << param.method().object().objectPath()
                      << "]."
                      << param.method().name()
                      << '('
                      << param.name()
                      << ')';
    }
    dbg.nospace() << ')';
    return dbg.space();
}
