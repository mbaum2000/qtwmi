#ifndef WMIOBJECTREQUEST_H
#define WMIOBJECTREQUEST_H

#include <QObject>
#include <QList>

#include "wmirequest.h"
#include "wmiobject.h"

class WmiObjectRequestPrivate;
class WmiError;
class WmiConnection;

class QTWMI_EXPORT WmiObjectRequest : public WmiRequest {
    Q_OBJECT
    Q_DECLARE_PRIVATE(WmiObjectRequest)
public:
    virtual ~WmiObjectRequest();

    QList<WmiObject> objects() const;

protected:
    explicit WmiObjectRequest(const WmiConnection *connection);
};

#endif // WMIOBJECTREQUEST_H
