QT += core axcontainer
QT -= gui

CONFIG += c++11

TARGET = qtwmi
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = lib
DEFINES += QTWMI_LIBRARY

SOURCES += \
    comtools.cpp \
    comconnection.cpp \
    wmiconnection.cpp \
    wmierror.cpp \
    wmiflavor.cpp \
    wmimethod.cpp \
    wmimethodcall.cpp \
    wmiobject.cpp \
    wmiobjectrequest.cpp \
    wmiobjectsink.cpp \
    wmiproperty.cpp \
    wmiqualifier.cpp \
    wmiparameter.cpp \
    wmielement.cpp \
    wmirequest.cpp

QMAKE_CXXFLAGS_RELEASE -= -Zc:strictStrings
QMAKE_CFLAGS_RELEASE -= -Zc:strictStrings
QMAKE_CFLAGS -= -Zc:strictStrings
QMAKE_CXXFLAGS -= -Zc:strictStrings

LIBS += -ladvapi32 -luser32 -lole32 -lwbemuuid

HEADERS += \
    exports.h \
    comtools.h \
    comconnection.h \
    wmiconnection.h \
    wmiconnection_p.h \
    wmierror.h \
    wmiflavor.h \
    wmiflavor_p.h \
    wmimethod.h \
    wmimethod_p.h \
    wmimethodcall.h \
    wmimethodcall_p.h \
    wmiobject.h \
    wmiobject_p.h \
    wmiobjectrequest.h \
    wmiobjectrequest_p.h \
    wmiobjectsink.h  \
    wmiproperty.h \
    wmiproperty_p.h \
    wmiqualifier.h \
    wmiqualifier_p.h \
    wmiparameter.h \
    wmiparameter_p.h \
    wmielement.h \
    wmielement_p.h \
    wmirequest.h \
    wmirequest_p.h

DISTFILES += \
    libqtwmi.pri
