#ifndef WMIPARAMETER_P_H
#define WMIPARAMETER_P_H

#include "wmiparameter.h"
#include "wmielement_p.h"
#include "wmimethod.h"

class WmiParameterPrivate : public WmiElementPrivate {
public:
    WmiParameterPrivate(const WmiObject &object = WmiObject(), const WmiMethod &method = WmiMethod(), const QString &name = QString(), bool isInput = false, bool isOutput = false);
    WmiParameterPrivate(const WmiParameterPrivate &other);
    ~WmiParameterPrivate();

    static WmiParameter create(const WmiObject &object = WmiObject(), const WmiMethod &method = WmiMethod(), const QString &name = QString(), bool isInput = false, bool isOutput = false);

    WmiMethod m_method;
    bool m_isInput;
    bool m_isOutput;
};

#endif // WMIPARAMETER_P_H
