#ifndef WMIREQUEST_P_H
#define WMIREQUEST_P_H

#include "wmirequest.h"
#include "wmiobjectsink.h"

class WmiRequestPrivate : public WmiObjectSink {
    Q_DECLARE_PUBLIC(WmiRequest)
public:
    explicit WmiRequestPrivate(WmiRequest *q, const WmiConnection *connection);
    virtual ~WmiRequestPrivate();

    static WmiObjectSink *objectSinkFromRequest(WmiRequest *request);

    WmiRequest *q_ptr;
};


#endif // WMIREQUEST_P_H
