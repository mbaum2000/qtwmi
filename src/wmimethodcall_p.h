#ifndef WMIMETHODCALL_P_H
#define WMIMETHODCALL_P_H

#include "wmirequest_p.h"
//#include "wmimethodcall.h"
#include "wmimethod.h"

class WmiMethodCall;

class WmiMethodCallPrivate : public WmiRequestPrivate {
    Q_DECLARE_PUBLIC(WmiMethodCall)
public:
    explicit WmiMethodCallPrivate(WmiMethodCall *q, const WmiMethod &method, const QVariantMap &inputParameters, const WmiConnection *connection);
    virtual ~WmiMethodCallPrivate();

    static WmiMethodCall *create(const WmiMethod &method, const QVariantMap &inputParameters, const WmiConnection *connection);

    WmiMethod m_method;
    QVariantMap m_inputParams;
};

#endif // WMIMETHODCALL_P_H
