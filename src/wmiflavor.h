#ifndef WMIFLAVOR_H
#define WMIFLAVOR_H

#include "exports.h"

#include <QVariant>
#include <QSharedDataPointer>

class WmiFlavorData;

class QTWMI_EXPORT WmiFlavor {
public:
    WmiFlavor(bool amended = false, bool toInstance = true, bool toSubClass = true, bool disableOverride = false);
    WmiFlavor(const WmiFlavor &other);
    ~WmiFlavor();

    const WmiFlavor &operator=(const WmiFlavor &other);
    bool operator==(const WmiFlavor &other) const;
    bool operator!=(const WmiFlavor &other) const;

    bool amended() const;
    void setAmended(bool amended);

    bool toInstance() const;
    void setToInstance(bool toInstance);

    bool toSubClass() const;
    void setToSubClass(bool toSubClass);

    bool disableOverride() const;
    void setDisableOverride(bool disableOverride);

    inline operator QVariant() const { return QVariant::fromValue<WmiFlavor>(*this); }

private:
    WmiFlavor(long wmiFlavor);

    friend class WmiFlavorData;
    friend class WmiFlavorTest;

    QSharedDataPointer<WmiFlavorData> d;
};

QDebug QTWMI_EXPORT operator<<(QDebug dbg, const WmiFlavor &flavor);

Q_DECLARE_METATYPE(WmiFlavor)

#endif // WMIFLAVOR_H
