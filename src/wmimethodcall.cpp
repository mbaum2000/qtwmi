#include "wmimethodcall.h"
#include "wmimethodcall_p.h"
#include "wmiconnection_p.h"
#include "comtools.h"

#include <QEventLoop>
#include <QTimer>
#include <QDebug>

WmiMethodCallPrivate::WmiMethodCallPrivate(WmiMethodCall *q, const WmiMethod &method, const QVariantMap &inputParameters, const WmiConnection *connection)
    : WmiRequestPrivate(q, connection),
      m_method(method),
      m_inputParams(inputParameters) { }

WmiMethodCallPrivate::~WmiMethodCallPrivate() { }

WmiMethodCall *WmiMethodCallPrivate::create(const WmiMethod &method, const QVariantMap &inputParameters, const WmiConnection *connection) {
    return new WmiMethodCall(method, inputParameters, connection);
}


WmiMethodCall::WmiMethodCall(const WmiMethod &method, const QVariantMap &inputParameters, const WmiConnection *connection)
    : WmiRequest(new WmiMethodCallPrivate(this, method, inputParameters, connection)) { }

WmiMethodCall::~WmiMethodCall() { }

WmiMethod WmiMethodCall::method() const {
    Q_D(const WmiMethodCall);
    return d->m_method;
}

// TODO: Test returnValue() and outputParameterValues() with a method that will time-out.
QVariant WmiMethodCall::returnValue() const {
    Q_D(const WmiMethodCall);
    if(!d->isFinished() && (d->m_method.cimType() != WmiProperty::Empty)) {
        if(!d->waitForFinished()) {
            d->m_lastError = WmiError::TimeOut;
            return QVariant();
        }
    }
    if(d->objects().isEmpty()) {
        return QVariant();
    }

    WmiObject result = d->objects().value(0);
    return result.property(QLatin1String("ReturnValue"));
}

QVariantMap WmiMethodCall::inputParameterValues() const {
    Q_D(const WmiMethodCall);
    return d->m_inputParams;
}

QVariantMap WmiMethodCall::outputParameterValues() const {
    Q_D(const WmiMethodCall);
    if(!d->isFinished() && !d->m_method.outputParameters().isEmpty()) {
        if(!d->waitForFinished()) {
            d->m_lastError = WmiError::TimeOut;
            return QVariantMap();
        }
    }
    if(d->objects().isEmpty()) {
        return QVariantMap();
    }

    QVariantMap out;
    WmiObject result = d->objects().value(0);
    for(const QString &p : result.properties()) {
        if(p != QLatin1String("ReturnValue")) {
            out.insert(p, result.property(p));
        }
    }
    return out;
}
