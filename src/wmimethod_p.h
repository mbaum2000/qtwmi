#ifndef WMIMETHOD_P_H
#define WMIMETHOD_P_H

#include "wmierror.h"
#include "wmielement_p.h"
#include "wmiobject.h"

class WmiMethodPrivate : public WmiElementPrivate {
public:
    WmiMethodPrivate(const WmiObject &object = WmiObject(), const QString &name = QString());
    WmiMethodPrivate(const WmiMethodPrivate &other);
    ~WmiMethodPrivate();

    static WmiMethod create(const WmiObject &object = WmiObject(), const QString &name = QString());

    WmiObject m_inParams;
    WmiObject m_outParams;
    QString m_origin;
};

#endif // WMIMETHOD_P_H
