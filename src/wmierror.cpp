#include "wmierror.h"
#include "wmiobject.h"

#include <QMetaEnum>
#include <QDebug>

#include <wbemidl.h>

WmiError::WmiError(WmiError::Error error)
    : m_error(error) { }

WmiError::WmiError(const WmiError &other)
    : m_error(other.m_error) { }

const WmiError &WmiError::operator=(const WmiError &other) {
    m_error = other.m_error;
    return *this;
}

bool WmiError::operator==(const WmiError &other) const {
    return m_error == other.m_error;
}

bool WmiError::operator!=(const WmiError &other) const {
    return m_error != other.m_error;
}

WmiError::Error WmiError::error() const {
    return m_error;
}

QString WmiError::errorString() const {
    int index = staticMetaObject.indexOfEnumerator("Error");
    return staticMetaObject.enumerator(index).valueToKey(m_error);
}

QString WmiError::errorDescription() const {
    switch(m_error) {
    case NoError:
        return tr("No Error.");

    case NotConnected:
        return tr("Not connected to WMI namespace.");

    case TimeOut:
        return tr("The operation timed out.");

    case ServerUnavailable:
        return tr("The RPC server is unavailable.");

    case NotFound:
        return tr("Object cannot be found.");

    case AccessDenied:
        return tr("Current user does not have permission to perform the action.");

    case ProviderFailure:
        return tr("Provider has failed at some time other than during initialization.");

    case TypeMismatch:
        return tr("Type mismatch occurred.");

    case OutOfMemory:
        return tr("Not enough memory for the operation.");

    case InvalidParameter:
        return tr("One of the parameters to the call is not correct.");

    case InvalidNamespace:
        return tr("Namespace specified cannot be found.");

    case InvalidObject:
        return tr("Specified instance is not valid.");

    case InvalidClass:
        return tr("Specified class is not valid.");

    case TransportFailure:
        return tr("Networking error that prevents normal operation has occurred.");

    case InvalidQuery:
        return tr("Query was not syntactically valid.");

    case AlreadyExists:
        return tr("In a put operation, the wbemChangeFlagCreateOnly flag was specified, but the instance already exists.");

    case IncompleteClass:
        return tr("Current object is not a valid class definition.");

    case InvalidSyntax:
        return tr("Query is syntactically not valid.");

    case ProviderNotCapable:
        return tr("Provider cannot perform the requested operation.");

    case IllegalNull:
        return tr("Value of Nothing/NULL was specified for a property that must have a value, such as one that is marked by a Key, Indexed, or Not_Null qualifier.");

    case InvalidPropertyType:
        return tr("CIM type specified for a property is not valid.");

    case InvalidMethod:
        return tr("Requested method is not available.");

    case InvalidMethodParameters:
        return tr("Parameters provided for the method are not valid.");

    case ShuttingDown:
        return tr("User has requested an operation while WMI is in the process of shutting down.");

    case InvalidObjectPath:
        return tr("Specified object path was not valid.");

    case MethodNotImplemented:
        return tr("Attempt was made to execute a method not marked with [implemented] in any relevant class.");

    case MethodDisabled:
        return tr("Attempt was made to execute a method marked with [disabled].");

    case LocalCredentials:
        return tr("User specified a username/password/authority on a local connection. The user must use a blank username/password and rely on default security.");

    case UnknownError:
    default:
        return tr("An unknown/unspecified error has occured.");
    }
}

WmiError::Error WmiError::errorFromHresult(long hr) {
    if(!FAILED(hr)) {
        return NoError;
    }
    switch(hr) {
    case RPC_S_SERVER_UNAVAILABLE:
    case 0x800706ba: // SERVER_UNAVAILABLE
        return ServerUnavailable;

    case WBEM_E_NOT_FOUND:
        return NotFound;

    case WBEM_E_ACCESS_DENIED:
    case E_ACCESSDENIED:
        return AccessDenied;

    case WBEM_E_PROVIDER_FAILURE:
        return ProviderFailure;

    case WBEM_E_TYPE_MISMATCH:
        return TypeMismatch;

    case WBEM_E_OUT_OF_MEMORY:
    case E_OUTOFMEMORY:
        return OutOfMemory;

    case WBEM_E_INVALID_PARAMETER:
    case E_INVALIDARG:
        return InvalidParameter;

    case WBEM_E_INVALID_NAMESPACE:
        return InvalidNamespace;

    case WBEM_E_INVALID_OBJECT:
        return InvalidObject;

    case WBEM_E_INVALID_CLASS:
        return InvalidClass;

    case WBEM_E_TRANSPORT_FAILURE:
        return TransportFailure;

    case WBEM_E_INVALID_QUERY:
        return InvalidQuery;

    case WBEM_E_ALREADY_EXISTS:
        return AlreadyExists;

    case WBEM_E_INCOMPLETE_CLASS:
        return IncompleteClass;

    case WBEM_E_INVALID_SYNTAX:
        return InvalidSyntax;

    case WBEM_E_PROVIDER_NOT_CAPABLE:
        return ProviderNotCapable;

    case WBEM_E_ILLEGAL_NULL:
        return IllegalNull;

    case WBEM_E_INVALID_PROPERTY_TYPE:
        return InvalidPropertyType;

    case WBEM_E_INVALID_METHOD:
        return InvalidMethod;

    case WBEM_E_INVALID_METHOD_PARAMETERS:
        return InvalidMethodParameters;

    case WBEM_E_SHUTTING_DOWN:
        return ShuttingDown;

    case WBEM_E_INVALID_OBJECT_PATH:
        return InvalidObjectPath;

    case WBEM_E_METHOD_NOT_IMPLEMENTED:
        return MethodNotImplemented;

    case WBEM_E_METHOD_DISABLED:
        return MethodDisabled;

    case WBEM_E_LOCAL_CREDENTIALS:
        return LocalCredentials;

    case WBEM_E_FAILED:
    case E_FAIL:
    case E_UNEXPECTED:
    case E_POINTER:
    default:
        return UnknownError;
    }
}

QDebug operator<<(QDebug dbg, const WmiError &error) {
    QDebugStateSaver saver(dbg);
    dbg.nospace() << "WmiError(";
    dbg.noquote() << error.errorString();
    dbg.nospace() << ')';
    return dbg.space();
}
