#include "wmiobject.h"
#include "wmiobject_p.h"
#include "wmimethod.h"
#include "wmimethod_p.h"
#include "wmiproperty.h"
#include "wmiproperty_p.h"
#include "wmiconnection.h"
#include "wmiconnection_p.h"
#include "wmiqualifier_p.h"
#include "comtools.h"

#include <qaxtypes.h>
#include <QScopedPointer>
#include <QSharedPointer>
#include <QDebug>

#include <wbemidl.h>

static int registerWmiObject() {
    QMetaType::registerDebugStreamOperator<WmiObject>();
    QMetaType::registerDebugStreamOperator<QList<WmiObject>>();
    return 0;
}

Q_CONSTRUCTOR_FUNCTION(registerWmiObject)


WmiObjectData::WmiObjectData(const WmiConnection *connection, IWbemClassObject *comObject, const QString &objectPath)
    : m_connection(connection),
      m_comObject(comObject),
      m_objectPath(objectPath),
      m_isInstance(false),
      m_lastError(WmiError::NoError) {

    if(m_comObject) {
        m_comObject->AddRef();
    }
    if(m_connection) {
        getComObject();
        if(m_comObject) {
            m_className = getProperty("__CLASS", m_comObject).toString();
            m_isInstance = (getProperty("__GENUS", m_comObject).toInt() == WBEM_GENUS_INSTANCE);
        }
    }
}

WmiObjectData::WmiObjectData(const WmiObjectData &other)
    : QSharedData(other),
      m_connection(other.m_connection),
      m_comObject(other.m_comObject),
      m_className(other.m_className),
      m_objectPath(other.m_objectPath),
      m_isInstance(other.m_isInstance),
      m_lastError(other.m_lastError) { }

WmiObjectData::~WmiObjectData() {
    releaseComObject();
}

void WmiObjectData::getComObject() {
    m_lastError = WmiError::NoError;
    if(m_comObject) {
        return;
    }

    IWbemServices *service = WmiConnectionPrivate::serviceForConnection(m_connection);
    if(!m_connection || !service) {
        m_lastError = WmiError::NotConnected;
        return;
    }

    if(m_objectPath.isEmpty()) {
        m_lastError = WmiError::InvalidObjectPath;
        return;
    }

    HRESULT hr = service->GetObject(BString(m_objectPath), 0, NULL, &(m_comObject), NULL);
    if(FAILED(hr)) {
        m_lastError = WmiError::errorFromHresult(hr);
        return;
    }
}

void WmiObjectData::releaseComObject() {
    if(m_comObject) {
        m_comObject->Release();
        m_comObject = NULL;
    }
}

QVariant WmiObjectData::getProperty(const QString &prop, IWbemClassObject *comObject) const {
    if(!comObject) {
        m_lastError = WmiError::InvalidObjectPath;
        return QVariant();
    }
    if(!m_connection || !m_connection->isValid()) {
        m_lastError = WmiError::NotConnected;
        return QVariant();
    }
    m_lastError = WmiError::NoError;

    HRESULT hr;

    VARIANT v;
    hr = comObject->Get(BString(prop), 0, &v, NULL, NULL);
    if(FAILED(hr)) {
        m_lastError = WmiError::errorFromHresult(hr);
        return QVariant();
    }

    if(v.vt == VT_UNKNOWN) {
        ScopedComObject<IWbemClassObject> obj;
        hr = v.pdispVal->QueryInterface(IID_IWbemClassObject, reinterpret_cast<void**>(obj.dataPointer()));
        VariantClear(&v);
        if(FAILED(hr)) {
            m_lastError = WmiError::errorFromHresult(hr);
            return QVariant();
        }
        return QVariant::fromValue<WmiObject>(WmiObject(QString(), obj, m_connection));
    } else if(v.vt == (VT_ARRAY | VT_UNKNOWN)) {
        IUnknown **val;
        SafeArrayAccessData(v.parray, reinterpret_cast<void**>(&val));

        long lowerBound, upperBound;
        SafeArrayGetLBound(v.parray, 1, &lowerBound);
        SafeArrayGetUBound(v.parray, 1, &upperBound);

        long num_elements = upperBound - lowerBound + 1;

        QList<WmiObject> list;
        for(long i = 0; i < num_elements; i++) {
            ScopedComObject<IWbemClassObject> obj;
            hr = val[i]->QueryInterface(IID_IWbemClassObject, reinterpret_cast<void**>(obj.dataPointer()));
            if(FAILED(hr)) {
                m_lastError = WmiError::errorFromHresult(hr);
                continue;
            }
            list.append(WmiObject(QString(), obj, m_connection));
        }
        SafeArrayUnaccessData(v.parray);
        VariantClear(&v);
        return QVariant::fromValue<QList<WmiObject>>(list);
    } else {
        QVariant variant = VARIANTToQVariant(v, QByteArray());
        VariantClear(&v);
        return variant;
    }
}

bool WmiObjectData::setProperty(const QString &prop, const QVariant &value, IWbemClassObject *comObject) const {
    if(!comObject || !m_connection || !m_connection->isValid()) {
        m_lastError = WmiError::NotConnected;
        return false;
    }
    m_lastError = WmiError::NoError;

    HRESULT hr;

    VARIANT v;
    QVariantToVARIANT(value, v);
    hr = comObject->Put(BString(prop), 0, &v, 0);
    VariantClear(&v);
    if(FAILED(hr)) {
        m_lastError = WmiError::errorFromHresult(hr);
        return false;
    }
    return true;
}

QStringList WmiObjectData::getProperties(IWbemClassObject *comObject) const {
    if(!m_connection || !m_connection->isValid()) {
        m_lastError = WmiError::NotConnected;
        return QStringList();
    } else if(!comObject) {
        m_lastError = WmiError::InvalidObject;
        return QStringList();
    }
    m_lastError = WmiError::NoError;

    QStringList list;
    HRESULT hr;
    hr = comObject->BeginEnumeration(0);

    while(WBEM_S_NO_ERROR == hr) {
        BSTR methodName;
        hr = comObject->Next(0, &methodName, NULL, NULL, NULL);

        if(WBEM_S_NO_ERROR == hr) {
            list << BString::bstrToQString(methodName);
            SysFreeString(methodName);
        }
    }

    comObject->EndEnumeration();
    return list;
}

WmiObject WmiObjectData::create(const QString &objectPath, IWbemClassObject *comObject, const WmiConnection *connection) {
    return WmiObject(objectPath, comObject, connection);
}

IWbemClassObject *WmiObjectData::comObjectForObject(const WmiObject &object) {
    return object.d->m_comObject;
}

bool WmiObjectData::compare(IWbemClassObject *object1, IWbemClassObject *object2) {
    if(object1 == object2) {
        return true;
    } else if(object1 == NULL || object2 == NULL) {
        return false;
    } else {
        return (object1->CompareTo(0, object2) == WBEM_S_SAME);
    }
}


WmiObject::WmiObject(const QString &objectPath, IWbemClassObject *comObject, const WmiConnection *connection)
    : d(new WmiObjectData(connection, comObject, objectPath)) { }

WmiObject::WmiObject()
    : d(new WmiObjectData) { }

WmiObject::WmiObject(const WmiObject &other)
    : d(other.d) { }

WmiObject::~WmiObject() { }

const WmiObject &WmiObject::operator=(const WmiObject &other) {
    d = other.d;
    return *this;
}

bool WmiObject::operator==(const WmiObject &other) const {
    return WmiObjectData::compare(d->m_comObject, other.d->m_comObject);
}

bool WmiObject::operator!=(const WmiObject &other) const {
    return !(*this == other);
}

bool WmiObject::isValid() const {
    return ((d->m_comObject != NULL)
            && !(d->m_connection.isNull())
            && !(d->m_className.isEmpty()));
}

bool WmiObject::isNull() const {
    return !isValid();
}

WmiError WmiObject::error() const {
    return WmiError(d->m_lastError);
}

QVariant WmiObject::property(const QString &name) const {
    return d->getProperty(name, d->m_comObject);
}

WmiProperty WmiObject::propertyInfo(const QString &name) const {
    if(allProperties().contains(name)) {
        return WmiPropertyPrivate::create(*this, name);
    } else {
        return WmiProperty();
    }
}

bool WmiObject::setProperty(const QString &name, const QVariant &value) {
    return d->setProperty(name, value, d->m_comObject);
}

QStringList WmiObject::properties() const {
    return allProperties().mid(10); // TODO: I do not like the use of magic numbers here.  Fix this.
}

QStringList WmiObject::allProperties() const {
    return d->getProperties(d->m_comObject);
}

QString WmiObject::className() const {
    return d->m_className;
}

QString WmiObject::objectPath() const {
    return d->m_objectPath;
}

const WmiConnection *WmiObject::connection() const {
    return d->m_connection;
}

bool WmiObject::isInstance() const {
    return d->m_isInstance;
}

QList<WmiQualifier> WmiObject::qualifiers() const {
    ScopedComObject<IWbemQualifierSet> qualifiers;
    QList<WmiQualifier> qualifierList;
    if(!d->m_comObject) {
        return qualifierList;
    }

    HRESULT hr;
    hr = d->m_comObject->GetQualifierSet(qualifiers);
    if(!FAILED(hr)) {
        // TODO: I don't like this conversion from map to list.  It is wasteful.  Fix this.
        qualifierList = WmiQualifierData::qualifierSetToMap(qualifiers).values();
    } else {
        d->m_lastError = WmiError::errorFromHresult(hr);
    }
    return qualifierList;
}

WmiQualifier WmiObject::qualifier(const QString &name) const {
    QList<WmiQualifier> qualifierList = qualifiers();
    for(const WmiQualifier &qualifier : qualifierList) {
        if(QString::compare(qualifier.name(), name, Qt::CaseInsensitive) == 0) {
            return qualifier;
        }
    }
    d->m_lastError = WmiError::NotFound;
    return WmiQualifier();
}

QStringList WmiObject::methods() const {
    const WmiConnection *conn = connection();
    IWbemServices *service = WmiConnectionPrivate::serviceForConnection(conn);
    if(!conn || !service) {
        d->m_lastError = WmiError::NotConnected;
        return QStringList();
    }
    d->m_lastError = WmiError::NoError;

    QStringList list;
    HRESULT hr;
    IWbemClassObject *cls = NULL;

    if(isInstance()) {
        hr = service->GetObject(BString(d->m_className), 0, NULL, &cls, NULL);
        if(FAILED(hr)) {
            d->m_lastError = WmiError::errorFromHresult(hr);
            return list;
        }
    } else {
        cls = d->m_comObject;
    }

    if(!cls) {
        d->m_lastError = WmiError::InvalidObject;
        return list;
    }

    hr = cls->BeginMethodEnumeration(0);

    while(WBEM_S_NO_ERROR == hr) {
        BSTR methodName;
        hr = cls->NextMethod(0, &methodName, NULL, NULL);

        if(WBEM_S_NO_ERROR == hr) {
            list << BString::bstrToQString(methodName);
            SysFreeString(methodName);
        }
    }

    cls->EndMethodEnumeration();
    if(cls != d->m_comObject) {
        cls->Release();
    }
    return list;
}

WmiMethod WmiObject::method(const QString &name) const {
    if(methods().contains(name)) {
        return WmiMethodPrivate::create(*this, name);
    } else {
        return WmiMethod();
    }
}

QVariant WmiObject::execMethod(const QString &name, const QVariantList &inputParametersList, const QVariantMap &inputParametersMap, QVariantMap &outputParameters) {
    return method(name).execWait(inputParametersList, inputParametersMap, outputParameters);
}

QVariant WmiObject::execMethod(const QString &name, const QVariantList &inputParametersList, const QVariantMap &inputParametersMap) {
    QVariantMap dummy;
    return execMethod(name, inputParametersList, inputParametersMap, dummy);
}

QVariant WmiObject::execMethod(const QString &name, const QVariantList &inputParameters, QVariantMap &outputParameters) {
    return execMethod(name, inputParameters, QVariantMap(), outputParameters);
}

QVariant WmiObject::execMethod(const QString &name, const QVariantMap &inputParameters, QVariantMap &outputParameters) {
    return execMethod(name, QVariantList(), inputParameters, outputParameters);
}

QVariant WmiObject::execMethod(const QString &name, const QVariantMap &inputParameters) {
    QVariantMap dummy;
    return execMethod(name, QVariantList(), inputParameters, dummy);
}

QString WmiObject::mof(bool noFlavors) const {
    BSTR mof;
    HRESULT hr = d->m_comObject->GetObjectText(noFlavors ? WBEM_FLAG_NO_FLAVORS : 0, &mof);
    if(FAILED(hr)) {
        d->m_lastError = WmiError::errorFromHresult(hr);
        return QString();
    }
    QString mofText = BString::bstrToQString(mof);
    SysFreeString(mof);
    return mofText;
}

WmiObject WmiObject::spawnInstance() const {
    d->m_lastError = WmiError::NoError;
    if(isNull()) {
        d->m_lastError = WmiError::InvalidObject;
        return WmiObject();
    } else if(isInstance()) {
        return classObject().spawnInstance();
    } else {
        HRESULT hr;
        ScopedComObject<IWbemClassObject> classInstance;

        hr = d->m_comObject->SpawnInstance(0, classInstance);
        if(FAILED(hr)) {
            d->m_lastError = WmiError::errorFromHresult(hr);
            return WmiObject();
        }

        return WmiObject(QString(), classInstance, connection());
    }
}

WmiObject WmiObject::classObject() const {
    d->m_lastError = WmiError::NoError;

    if(isNull()) {
        d->m_lastError = WmiError::InvalidObject;
        return WmiObject();
    } else if(isInstance()) {
        HRESULT hr;
        ScopedComObject<IWbemClassObject> classDef;
        IWbemServices *service = WmiConnectionPrivate::serviceForConnection(connection());
        if(!connection() || !service) {
            d->m_lastError = WmiError::NotConnected;
            return WmiObject();
        }

        hr = service->GetObject(BString(className()), 0, NULL, classDef, NULL);
        if(FAILED(hr)) {
            d->m_lastError = WmiError::errorFromHresult(hr);
            return WmiObject();
        }

        return WmiObject(className(), classDef, connection());
    } else {
        return *this;
    }
}

void WmiObject::refresh() {
    d->releaseComObject();
    d->getComObject();
}

bool WmiObject::apply() {
    const WmiConnection *conn = connection();
    IWbemServices *service = WmiConnectionPrivate::serviceForConnection(connection());
    if(!conn || !service) {
        d->m_lastError = WmiError::NotConnected;
        return false;
    }
    d->m_lastError = WmiError::NoError;

    HRESULT hr;

    hr = service->PutInstance(d->m_comObject, WBEM_FLAG_CREATE_OR_UPDATE, NULL, NULL);
    if(FAILED(hr)) {
        d->m_lastError = WmiError::errorFromHresult(hr);
        return false;
    }
    if(d->m_objectPath.isEmpty()) {
        d->m_objectPath = property("__RELPATH").toString();
        if(!d->m_objectPath.isEmpty()) {
            refresh();
            QString fullPath = property("__PATH").toString();
            if(!fullPath.isEmpty()) {
                d->m_objectPath = fullPath;
            }
        }
    }
    return true;
}

QDebug operator<<(QDebug dbg, const WmiObject &obj) {
    QDebugStateSaver saver(dbg);
    dbg.nospace() << "WmiObject(";
    if(obj.isValid()) {
        if(obj.isInstance()) {
            dbg.nospace() << "Instance, ";
        } else {
            dbg.nospace() << "Class, ";
        }
        dbg.nospace() << obj.objectPath();
    } else {
        dbg.nospace() << "Null";
    }
    dbg.nospace() << ')';
    return dbg.space();
}
