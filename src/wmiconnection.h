#ifndef WMICONNECTION_H
#define WMICONNECTION_H

#include "exports.h"
#include "wmiobject.h"

#include <QObject>
#include <QString>
#include <QVariant>
#include <QList>
#include <QScopedPointer>

class WmiError;
class WmiConnectionPrivate;
class WmiObjectRequest;

class QTWMI_EXPORT WmiConnection : public QObject {
    Q_OBJECT
    Q_DECLARE_PRIVATE(WmiConnection)
public:
    explicit WmiConnection(const QString &wmiNamespace, QObject *parent = 0);
    virtual ~WmiConnection();

    QString wmiNamespace() const;

    WmiObjectRequest* execQuery(const QString &query) const;
    WmiObject getObject(const QString &className) const;

    WmiError error() const;

    bool isValid() const;

private:
    QScopedPointer<WmiConnectionPrivate> d_ptr;
};

QDebug QTWMI_EXPORT operator<<(QDebug dbg, const WmiConnection *obj);

#endif // WMICONNECTION_H
