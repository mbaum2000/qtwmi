#include "comconnection.h"
#include "comtools.h"

#include <QDebug>

#define _WIN32_DCOM
#include <windows.h>

ComConnection::ComConnection()
    : m_isConnected(false) {

    HRESULT hr;

    hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
    if(hr == RPC_E_CHANGED_MODE) {
        qDebug() << "COM library already initialized on this thread:" << hrPrintable(hr);
        return;
    }
    if(FAILED(hr) && (hr != S_FALSE)) {
        qWarning() << "Failed to initialize COM library:" << hrPrintable(hr);
        return;
    }

    hr = CoInitializeSecurity(
        NULL,
        -1,
        NULL,
        NULL,
        RPC_C_AUTHN_LEVEL_DEFAULT,
        RPC_C_IMP_LEVEL_IMPERSONATE,
        NULL,
        EOAC_NONE,
        NULL);
    if(FAILED(hr) && (hr != RPC_E_TOO_LATE)) {
        qWarning() << "Failed to initialize COM security:" << hrPrintable(hr);
        CoUninitialize();
        return;
    }
    m_isConnected = true;
}

ComConnection::~ComConnection() {
    disconnect();
}

bool ComConnection::isConnected() const {
    return m_isConnected;
}

void ComConnection::disconnect() {
    if(m_isConnected) {
        CoUninitialize();
    }
}

bool ComConnection::isComInitialized() {
    HRESULT hr;
    void *dummy = NULL;
    hr = CoCreateInstance(IID_IUnknown, 0, CLSCTX_INPROC_SERVER, IID_IUnknown, &dummy);
    return (hr != CO_E_NOTINITIALIZED);
}

