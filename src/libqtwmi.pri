INCLUDEPATH += $${top_srcdir}/src

CONFIG(debug, debug|release) {
    LIBS += -L$${top_builddir}/src/debug
}
CONFIG(release, debug|release) {
    LIBS += -L$${top_builddir}/src/release
}

LIBS += -lqtwmi
