#ifndef WMIMETHOD_H
#define WMIMETHOD_H

#include "exports.h"
#include "wmielement.h"
#include "wmiparameter.h"
#include "wmiproperty.h"

#include <QVariant>
#include <QExplicitlySharedDataPointer>

class WmiMethodPrivate;
class WmiMethodCall;
class WmiObject;
class WmiError;

class QTWMI_EXPORT WmiMethod : public WmiElement {
    Q_GADGET
    Q_DECLARE_PRIVATE(WmiMethod)
public:
    WmiMethod();
    WmiMethod(const WmiMethod &other);
    ~WmiMethod();

    QString origin() const;

    QStringList inputParameters() const;
    QStringList outputParameters() const;
    WmiParameter parameterInfo(const QString &parameterName) const;

    WmiMethodCall* exec(const QVariantList &inputParametersList = QVariantList(), const QVariantMap &inputParametersMap = QVariantMap());
    WmiMethodCall* exec(const QVariantMap &inputParameters);

    QVariant execWait(const QVariantList &inputParametersList, const QVariantMap &inputParametersMap, QVariantMap &outputParameters);
    QVariant execWait(const QVariantList &inputParametersList = QVariantList(), const QVariantMap &inputParametersMap = QVariantMap());
    QVariant execWait(const QVariantList &inputParameters, QVariantMap &outputParameters);
    QVariant execWait(const QVariantMap &inputParameters, QVariantMap &outputParameters);
    QVariant execWait(const QVariantMap &inputParameters);

protected:
    WmiMethod(WmiMethodPrivate *d);

private:
    friend class WmiElementPrivate;
};

QDebug QTWMI_EXPORT operator<<(QDebug dbg, const WmiMethod &method);

#endif // WMIMETHOD_H
