#include "wmiobjectsink.h"
#include "wmiconnection_p.h"
#include "wmirequest.h"
#include "comtools.h"

#include <qaxtypes.h>
#include <QEventLoop>
#include <QTimer>
#include <QDebug>

WmiObjectSink::WmiObjectSink(QObject *handler, const WmiConnection *connection)
    : IWbemObjectSink(),
      m_handler(handler),
      m_connection(connection),
      m_ref(0),
      m_finished(false),
      m_lastError(WmiError::NoError) { }

WmiObjectSink::~WmiObjectSink() {
    cancel();
}

HRESULT WmiObjectSink::QueryInterface(const IID &riid, void **ppvObject) {
    if(riid == IID_IUnknown || riid == IID_IWbemObjectSink) {
        *ppvObject = (IWbemObjectSink *) this;
        AddRef();
        return WBEM_S_NO_ERROR;
    } else {
        return E_NOINTERFACE;
    }
}

ULONG WmiObjectSink::AddRef() {
    return InterlockedIncrement(&m_ref);
}

ULONG WmiObjectSink::Release() {
    return InterlockedDecrement(&m_ref);
}

HRESULT WmiObjectSink::Indicate(LONG lObjectCount, IWbemClassObject **apObjArray) {
    for(long i = 0; i < lObjectCount; i++) {
        IWbemClassObject *obj = apObjArray[i];
        if(!obj) {
            m_lastError = WmiError::UnknownError;
            continue;
        }

        VARIANT v;
        obj->Get(BSTR(L"__PATH"), 0, &v, NULL, NULL);
        QString pathName = VARIANTToQVariant(v, QByteArray()).toString();

        m_objects.append(WmiObjectData::create(pathName, obj, m_connection));

        VariantClear(&v);
    }

    return WBEM_S_NO_ERROR;
}

HRESULT WmiObjectSink::SetStatus(LONG lFlags, HRESULT hResult, BSTR strParam, IWbemClassObject *pObjParam) {
    Q_UNUSED(hResult)
    Q_UNUSED(strParam)
    Q_UNUSED(pObjParam)
    // TODO: Handle hResult, and if error, set error(), and possibly emit an error signal.
    switch(lFlags) {
    case WBEM_STATUS_COMPLETE:
        m_finished = true;
        QMetaObject::invokeMethod(m_handler.data(), "finished", Qt::QueuedConnection);
        break;
    default:
        break;
    }
    return WBEM_S_NO_ERROR;
}

void WmiObjectSink::setHandler(QObject *handler) {
    m_handler = handler;
}

QObject *WmiObjectSink::handler() const {
    return m_handler.data();
}

const WmiConnection *WmiObjectSink::connection() const {
    return m_connection.data();
}

QList<WmiObject> WmiObjectSink::objects() const {
    return m_objects;
}

bool WmiObjectSink::isFinished() const {
    return m_finished;
}

bool WmiObjectSink::waitForFinished(int msecs) const {
    if(m_finished) {
        return false;
    }
    QEventLoop loop;
    QTimer timeout;
    QObject::connect(&timeout, SIGNAL(timeout()), &loop, SLOT(quit()));
    QObject::connect(m_handler.data(), SIGNAL(finished()), &loop, SLOT(quit()));
    if(msecs != -1) {
        timeout.start(msecs);
    }
    loop.exec();
    return m_finished;
}

bool WmiObjectSink::cancel() {
    m_lastError = WmiError::NoError;
    if(m_finished) {
        return false;
    }
    IWbemServices *service = WmiConnectionPrivate::serviceForConnection(m_connection);
    if(!m_connection || !service) {
        m_lastError = WmiError::NotConnected;
        return false;
    }
    HRESULT hr = service->CancelAsyncCall(this);
    if(FAILED(hr)) {
        m_lastError = WmiError::errorFromHresult(hr);
        return false;
    }
    return true;
}
