#ifndef EXPORTS_H
#define EXPORTS_H

#if defined(QTWMI_LIBRARY)
#  define QTWMI_EXPORT Q_DECL_EXPORT
#else
#  define QTWMI_EXPORT Q_DECL_IMPORT
#endif

#endif // EXPORTS_H

