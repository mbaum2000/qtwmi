#ifndef COMTOOOLS_H
#define COMTOOOLS_H

#include <QString>

#include <windows.h>

#define hrPrintable(hr) qPrintable(QString::number((quint32)hr, 16).prepend("0x"))
#define hrError(hr) qWarning() << "ERROR" << hrPrintable(hr) << ":"
#define hrInfo(hr) qDebug() << "INFO" << hrPrintable(hr) << ":"

template <typename T>
class ScopedComObject {
public:
    ScopedComObject(T *obj = NULL) : m_obj(obj) { }
    ~ScopedComObject() { if(m_obj) { m_obj->Release(); } }

    T &operator*() { return m_obj; }
    T *operator->() { return m_obj; }
    operator T*() { return m_obj; }
    operator T**() { return &m_obj; }
    operator bool() { return m_obj != NULL; }

    T* data() { return m_obj; }
    T** dataPointer() { return &m_obj; }

private:
    T *m_obj;
};

struct ScopedPointerComReleaser {
    static inline void cleanup(IUnknown *obj) {
        if(obj) {
            obj->Release();
        }
    }
};

class BString {
public:
    BString(const QString &string);
    ~BString();

    operator BSTR();

    static QString bstrToQString(BSTR string);

private:
    BSTR m_str;
};

#endif // COMTOOOLS_H
