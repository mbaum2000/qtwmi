#include "wmiproperty.h"
#include "wmiproperty_p.h"
#include "wmiobject.h"
#include "wmiobject_p.h"
#include "wmiqualifier_p.h"
#include "comtools.h"

#include <QDebug>

#include <wbemidl.h>

WmiPropertyPrivate::WmiPropertyPrivate(const WmiObject &object, const QString &name)
    : WmiElementPrivate(object, name) {

    // Check COM Object
    HRESULT hr;
    IWbemClassObject *comObject = WmiObjectData::comObjectForObject(m_object);
    if(!comObject) {
        m_lastError = WmiError::NotConnected;
        return;
    }

    // Get Property Info
    VARIANT v;
    CIMTYPE t;
    LONG f;
    hr = comObject->Get(BString(m_name), 0, &v, &t, &f);
    if(!FAILED(hr)) {
        m_cimType = static_cast<WmiElement::CimType>(t);
        m_flavor = f;
    } else {
        m_lastError = WmiError::errorFromHresult(hr);
    }

    // Get Property Qualifiers
    ScopedComObject<IWbemQualifierSet> qualifiers;
    hr = comObject->GetPropertyQualifierSet(BString(m_name), qualifiers);
    if(!FAILED(hr)) {
        m_qualifiers = WmiQualifierData::qualifierSetToMap(qualifiers);
    } else {
        m_lastError = WmiError::errorFromHresult(hr);
    }

    // Get Property Origin
    BSTR propertyOrigin;
    hr = comObject->GetPropertyOrigin(BString(m_name), &propertyOrigin);
    if(!FAILED(hr)) {
        m_origin = BString::bstrToQString(propertyOrigin);
        SysFreeString(propertyOrigin);
    } else {
        m_lastError = WmiError::errorFromHresult(hr);
    }
}

WmiPropertyPrivate::WmiPropertyPrivate(const WmiPropertyPrivate &other)
    : WmiElementPrivate(other),
      m_origin(other.m_origin) { }

WmiPropertyPrivate::~WmiPropertyPrivate() { }

WmiProperty WmiPropertyPrivate::create(const WmiObject &object, const QString &name) {
    return WmiProperty(new WmiPropertyPrivate(object, name));
}


WmiProperty::WmiProperty()
    : WmiElement(new WmiPropertyPrivate) { }

WmiProperty::WmiProperty(const WmiProperty &other)
    : WmiElement(other) { }

WmiProperty::WmiProperty(WmiPropertyPrivate *d)
    : WmiElement(d) { }

WmiProperty::~WmiProperty() { }

QString WmiProperty::origin() const {
    Q_D(const WmiProperty);
    return d->m_origin;
}

bool WmiProperty::isSystem() const {
    Q_D(const WmiProperty);
    return (d->m_flavor & WBEM_FLAVOR_ORIGIN_SYSTEM);
}

bool WmiProperty::isPropogated() const {
    Q_D(const WmiProperty);
    return (d->m_flavor & WBEM_FLAVOR_ORIGIN_PROPAGATED);
}

QDebug operator<<(QDebug dbg, const WmiProperty &prop) {
    QDebugStateSaver saver(dbg);
    dbg.nospace() << "WmiProperty(";
    if(prop.isValid()) {
        dbg.noquote() << '['
                      << prop.object().objectPath()
                      << "]."
                      << prop.name();
    }
    dbg.nospace() << ')';
    return dbg.space();
}
