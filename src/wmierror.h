#ifndef WMIERROR_H
#define WMIERROR_H

#include "exports.h"

#include <QString>
#include <QCoreApplication>

//TODO: What would be better? error() with it's mutable member, or optional return value on each error-able function?
class QTWMI_EXPORT WmiError {
    Q_GADGET
    Q_DECLARE_TR_FUNCTIONS(WmiError)
public:
    enum Error {
        NoError = 0,
        NotConnected = 1,
        TimeOut = 2,
        ServerUnavailable = 0x6ba,
        UnknownError = 0x1001,
        NotFound = 0x1002,
        AccessDenied = 0x1003,
        ProviderFailure = 0x1004,
        TypeMismatch = 0x1005,
        OutOfMemory = 0x1006,
        InvalidParameter = 0x1008,
        InvalidNamespace = 0x100e,
        InvalidObject = 0x100f,
        InvalidClass = 0x1010,
        TransportFailure = 0x1015,
        InvalidQuery = 0x1017,
        AlreadyExists = 0x1019,
        IncompleteClass = 0x1020,
        InvalidSyntax = 0x1021,
        ProviderNotCapable = 0x1024,
        IllegalNull = 0x1028,
        InvalidPropertyType = 0x102a,
        InvalidMethod = 0x102e,
        InvalidMethodParameters = 0x102f,
        ShuttingDown = 0x1033,
        InvalidObjectPath = 0x103a,
        MethodNotImplemented = 0x1055,
        MethodDisabled = 0x1056,
        LocalCredentials = 0x1064
    };
    Q_ENUM(Error)

    WmiError(Error error = NoError);
    WmiError(const WmiError &other);

    const WmiError &operator=(const WmiError &other);
    bool operator==(const WmiError &other) const;
    bool operator!=(const WmiError &other) const;

    Error error() const;
    QString errorString() const;
    QString errorDescription() const;
    static Error errorFromHresult(long hr);

private:
    Error m_error;
};

QDebug QTWMI_EXPORT operator<<(QDebug dbg, const WmiError &error);

#endif // WMIERROR_H
