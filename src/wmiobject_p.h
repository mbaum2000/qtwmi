#ifndef WMIOBJECT_P_H
#define WMIOBJECT_P_H

#include "wmierror.h"

#include <QSharedData>
#include <QPointer>

class WmiConnection;
struct IWbemClassObject;

class WmiObjectData : public QSharedData {
public:
    WmiObjectData(const WmiConnection *connection = Q_NULLPTR, IWbemClassObject *comObject = NULL, const QString &objectPath = QString());
    WmiObjectData(const WmiObjectData &other);
    ~WmiObjectData();

    void getComObject();
    void releaseComObject();

    QVariant getProperty(const QString &prop, IWbemClassObject *comObject) const;
    bool setProperty(const QString &prop, const QVariant &value, IWbemClassObject *comObject) const;
    QStringList getProperties(IWbemClassObject *comObject) const;

    static WmiObject create(const QString &objectPath, IWbemClassObject *comObject, const WmiConnection *connection);
    static IWbemClassObject *comObjectForObject(const WmiObject &object);
    static bool compare(IWbemClassObject *object1, IWbemClassObject *object2);

    QPointer<const WmiConnection> m_connection;
    IWbemClassObject *m_comObject;
    QString m_className;
    QString m_objectPath;
    bool m_isInstance;
    mutable WmiError::Error m_lastError;
};

#endif // WMIOBJECT_P_H

