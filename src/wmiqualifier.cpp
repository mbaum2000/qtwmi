#include "wmiqualifier.h"
#include "wmiqualifier_p.h"
#include "wmiflavor_p.h"
#include "comtools.h"

#include <qaxtypes.h>
#include <QDebug>

#include <wbemidl.h>

static int registerWmiQualifier() {
    QMetaType::registerDebugStreamOperator<WmiQualifier>();
    return 0;
}

Q_CONSTRUCTOR_FUNCTION(registerWmiQualifier)

WmiQualifierData::WmiQualifierData(const QString &name, const QVariant &value, const WmiFlavor &flavor)
    : m_name(name),
      m_value(value),
      m_flavor(flavor) { }

WmiQualifierData::WmiQualifierData(const WmiQualifierData &other)
    : QSharedData(other),
      m_name(other.m_name),
      m_value(other.m_value),
      m_flavor(other.m_flavor) { }

WmiQualifierData::~WmiQualifierData() { }

QMap<QString, WmiQualifier> WmiQualifierData::qualifierSetToMap(IWbemQualifierSet *qualifiers) {
    HRESULT hr;
    QMap<QString, WmiQualifier> map;

    hr = qualifiers->BeginEnumeration(0);
    while(WBEM_S_NO_ERROR == hr) {
        BSTR name;
        VARIANT data;
        LONG flavor;
        hr = qualifiers->Next(0, &name, &data, &flavor);
        if(WBEM_S_NO_ERROR == hr) {
            QString qualifierName = BString::bstrToQString(name);
            WmiQualifier qualifier(qualifierName, VARIANTToQVariant(data, QByteArray()), WmiFlavorData::create(flavor));
            map.insert(qualifierName.toLower(), qualifier);
            SysFreeString(name);
            VariantClear(&data);
        }
    }
    qualifiers->EndEnumeration();

    return map;
}


WmiQualifier::WmiQualifier(const QString &name, const QVariant &value, const WmiFlavor &flavor)
    : d(new WmiQualifierData(name, value, flavor)) { }

WmiQualifier::WmiQualifier(const WmiQualifier &other)
    : d(other.d) { }

WmiQualifier::~WmiQualifier() { }

const WmiQualifier &WmiQualifier::operator=(const WmiQualifier &other) {
    d = other.d;
    return *this;
}

bool WmiQualifier::operator==(const WmiQualifier &other) const {
    return ((QString::compare(d->m_name, other.d->m_name, Qt::CaseInsensitive) == 0)
            && (d->m_value == other.d->m_value)
            && (d->m_flavor == other.d->m_flavor));
}

bool WmiQualifier::operator!=(const WmiQualifier &other) const {
    return !(*this == other);
}

QString WmiQualifier::name() const {
    return d->m_name;
}

void WmiQualifier::setName(const QString &name) {
    if(d->m_name != name) {
        d->m_name = name;
    }
}

QVariant WmiQualifier::value() const {
    return d->m_value;
}

void WmiQualifier::setValue(const QVariant &value) {
    if(d->m_value != value) {
        d->m_value = value;
    }
}

WmiFlavor WmiQualifier::flavor() const {
    return d->m_flavor;
}

void WmiQualifier::setFlavor(const WmiFlavor &flavor) {
    if(d->m_flavor != flavor) {
        d->m_flavor = flavor;
    }
}
QDebug operator<<(QDebug dbg, const WmiQualifier &qualifier) {
    QDebugStateSaver saver(dbg);
    dbg.nospace() << "WmiQualifier(";
    dbg.noquote() << qualifier.name();

    QVariant value = qualifier.value();
    dbg << '(';
    if(value.type() == QVariant::String) {
        dbg.quote() << value.toString();
    } else {
        dbg.noquote() << value.toString();
    }
    dbg.noquote() << ')';

    dbg.space() << ':';
    dbg.nospace() << WmiFlavorData::toString(qualifier.flavor()) << ')';
    return dbg.space();
}
