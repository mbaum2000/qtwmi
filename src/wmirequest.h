#ifndef WMIREQUEST_H
#define WMIREQUEST_H

#include "exports.h"

#include <QObject>

class WmiError;
class WmiConnection;
class WmiRequestPrivate;

class QTWMI_EXPORT WmiRequest : public QObject {
    Q_OBJECT
    Q_DECLARE_PRIVATE(WmiRequest)
public:
    virtual ~WmiRequest();

    WmiError error() const;

    bool isFinished() const;

    bool waitForFinished(int msecs = 30000);

signals:
    void finished();

public slots:
    bool cancel();

protected:
    explicit WmiRequest(WmiRequestPrivate *d);

    QScopedPointer<WmiRequestPrivate> d_ptr;
};

#endif // WMIREQUEST_H
