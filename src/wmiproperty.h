#ifndef WMIPROPERTY_H
#define WMIPROPERTY_H

#include "exports.h"
#include "wmielement.h"

#include <QVariant>

class WmiPropertyPrivate;
class WmiObject;

class QTWMI_EXPORT WmiProperty : public WmiElement {
    Q_GADGET
    Q_DECLARE_PRIVATE(WmiProperty)
public:
    WmiProperty();
    WmiProperty(const WmiProperty &other);
    ~WmiProperty();

    QString origin() const;

    bool isSystem() const;
    bool isPropogated() const;

protected:
    WmiProperty(WmiPropertyPrivate *d);

private:
    friend class WmiPropertyPrivate;
};

QDebug QTWMI_EXPORT operator<<(QDebug dbg, const WmiProperty &prop);

#endif // WMIPROPERTY_H
