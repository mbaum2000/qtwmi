#ifndef WMIOBJECTREQUEST_P_H
#define WMIOBJECTREQUEST_P_H

#include "wmirequest_p.h"

class WmiObjectRequest;

class WmiObjectRequestPrivate : public WmiRequestPrivate {
    Q_DECLARE_PUBLIC(WmiObjectRequest)
public:
    explicit WmiObjectRequestPrivate(WmiObjectRequest *q, const WmiConnection *connection);
    virtual ~WmiObjectRequestPrivate();

    static WmiObjectRequest *create(const WmiConnection *connection);
};

#endif // WMIOBJECTREQUEST_P_H
