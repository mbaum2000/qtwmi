#ifndef COMCONNECTION_H
#define COMCONNECTION_H

#include <QtCore/qglobal.h>

#include "exports.h"

class QTWMI_EXPORT ComConnection {
public:
    ComConnection();
    virtual ~ComConnection();

    bool isConnected() const;
    void disconnect();

    static bool isComInitialized();

private:
    bool m_isConnected;
};

#endif // COMCONNECTION_H
